package com.w3p.im.iib.mon.mq.files;

import static com.w3p.im.iib.mon.mq.files.constants.IConstants.FILES_TO_QUEUE;
import static com.w3p.im.iib.mon.mq.files.constants.IConstants.QUEUE_TO_FILES;
import static com.w3p.im.iib.mon.mq.files.constants.IConstants.QUEUE_TO_QUEUE;

import java.util.Scanner;

import javax.jms.Destination;
import javax.jms.JMSContext;
import javax.jms.JMSException;
import javax.jms.JMSRuntimeException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.ibm.msg.client.jms.JmsConnectionFactory;
import com.ibm.msg.client.jms.JmsFactoryFactory;
import com.ibm.msg.client.wmq.WMQConstants;
import com.w3p.im.iib.mon.mq.files.data.ConnectionData;
import com.w3p.im.iib.mon.mq.files.data.Request;
import com.w3p.im.iib.mon.mq.files.data.Response;
import com.w3p.im.iib.mon.mq.files.exceptions.MessagingException;
import com.w3p.im.iib.mon.mq.files.exceptions.PropertiesException;
import com.w3p.im.iib.mon.mq.files.exceptions.ValidationException;
import com.w3p.im.iib.mon.mq.files.jms.JmsFactoryFactoryWrapper;
import com.w3p.im.iib.mon.mq.files.processors.CreateRequest;
import com.w3p.im.iib.mon.mq.files.processors.FilesToQueueProcessor;
import com.w3p.im.iib.mon.mq.files.processors.QueueToFilesProcessor;
import com.w3p.im.iib.mon.mq.files.writers.FilesWriter;
import com.w3p.im.iib.mon.mq.files.writers.MessagesWriter;

public class MQFilesUtility {
	private static final Logger log = LoggerFactory.getLogger(MQFilesUtility.class);
	
	
	public MQFilesUtility() {		
	}
	
	public void run(String[] args) {
		
		Response resp = new Response();
		
		try {
			// Merge the args and properties file to create the RequestData
			Request req = createRequest(args);
			
			switch (req.getReqType()) {
			case FILES_TO_QUEUE:
				FilesToQueueProcessor ftqp = createFilesToQueueProcessor();
				try (MessagesWriter messagesWriter = new MessagesWriter()) { // Auto-closable
					resp = ftqp.process(req, messagesWriter);
				} 
				break;
			case QUEUE_TO_FILES:
				QueueToFilesProcessor qtfp = createQueueToFilesProcessor();
				try (FilesWriter filesWriter = new FilesWriter()) {
					resp = qtfp.process(req, filesWriter);
				}
				break;
			case QUEUE_TO_QUEUE:
//TODO TBD		QueueToQueueProcessor qtpp = new QueueToQudeueProcessor();
//				FilesWriter filesWriter = new FilesWriter();
//				resp = qtqpw.process(req, filesWriter);				
			default:
				String reason = String.format("Invalid type of request: '%s'. Please correct.", req.getReqType());
				reportAndTerminate(resp, "Invalid request", new Exception(reason));
			}
		} catch (ValidationException e) {			
			reportAndTerminate(resp, "Validation", e);
		}  catch (MessagingException e) {
			reportAndTerminate(resp, "MQ/JMS", e);
		}  catch (PropertiesException e) {
			reportAndTerminate(resp, "Properties", e);
		} catch (Exception e) {
			// A catch-all for any unhandled exceptions
			reportAndTerminate(resp, "Unhandled exception", e);
		}
	}

	/**
	 * Allows the 'doYouWantToContinue()' method to be overriden durinmg jUnit testing  
	 * @return
	 */
	protected FilesToQueueProcessor createFilesToQueueProcessor() {
		return new FilesToQueueProcessor();
	}

	/**
	 * Allows the 'doYouWantToContinue()' method to be overriden durinmg jUnit testing
	 * @return
	 */
	protected QueueToFilesProcessor createQueueToFilesProcessor() {
		return new QueueToFilesProcessor();
	}

	protected Request createRequest(String[] args) throws ValidationException, PropertiesException {
		return new CreateRequest().create(args);
	}

	protected JmsConnectionFactory createJmsConnectionFactory(JmsFactoryFactoryWrapper jffw) throws MessagingException {
		JmsConnectionFactory connFac =  null;
		try {
			JmsFactoryFactory ff = jffw.getFactory(WMQConstants.WMQ_PROVIDER);
			connFac = ff.createConnectionFactory();
		} catch (JMSException e) {
			String reason = String.format("Error occurred when creating the JMS Connection Factory. Cause: '%s'. See log.", e.getMessage());
			throw new MessagingException(reason, e);
		}
		
		return connFac;
	}

	protected void configureConnFactory(JmsConnectionFactory connFac, ConnectionData conn) throws MessagingException {
		try {
			connFac.setStringProperty(WMQConstants.WMQ_HOST_NAME, conn.getHost());
			connFac.setIntProperty(WMQConstants.WMQ_PORT, conn.getPort());
			connFac.setStringProperty(WMQConstants.WMQ_CHANNEL, conn.getServerChan());
			connFac.setIntProperty(WMQConstants.WMQ_CONNECTION_MODE, WMQConstants.WMQ_CM_CLIENT);
			connFac.setStringProperty(WMQConstants.WMQ_QUEUE_MANAGER, conn.getQmgr());
			connFac.setStringProperty(WMQConstants.WMQ_APPLICATIONNAME, "MQ Files Utility App");
			connFac.setBooleanProperty(WMQConstants.USER_AUTHENTICATION_MQCSP, false);
			if (conn.getUserid() != null) {
				connFac.setBooleanProperty(WMQConstants.USER_AUTHENTICATION_MQCSP, true);
				connFac.setStringProperty(WMQConstants.USERID, conn.getUserid());
				connFac.setStringProperty(WMQConstants.PASSWORD, conn.getPassword());
			}
		} catch (JMSException e) {
			String reason = String.format("Error occurred when configuring the JMS Connection Factory. Cause: '%s'. See log.", e.getMessage());
			throw new MessagingException(reason, e);
		}
	}

	protected JMSContext createJMSContext(JmsConnectionFactory connFac) throws MessagingException{
		JMSContext context = null;
		try {
			context = connFac.createContext(JMSContext.AUTO_ACKNOWLEDGE);
		} catch (JMSRuntimeException e) {
			String reason = String.format("Error occurred when creating the JMS Context. Cause: '%s'. See log.", e.getMessage());
			throw new MessagingException(reason, e);
		}
		return context;
	}

	protected Destination createDestination(JMSContext context, ConnectionData conn) {
		Destination dest = null;
		try {
			dest = context.createQueue(String.format("queue:///%s", conn.getQueue()));	
		} catch (JMSRuntimeException e) {
			String reason = String.format("Error occurred when creating the JMS Destination '%s'. Cause: '%s'. See log.", conn.getQueue(), e.getMessage());
			throw new MessagingException(reason, e);
		}
		return dest;	
	}

	protected boolean doYouWantToContinue() {
		boolean proceed = false;
		Scanner scanner = new Scanner(System.in);
		System.out.print("Please check the details above for your copy request.\nDo you want to continue? [Y or N]");
		String reply = scanner.nextLine();
		boolean answered = false;
		while (!answered) {
			if (reply != null) {
				if (reply.equalsIgnoreCase("Y") || reply.isEmpty()) {
					proceed = true;
					answered = true;
				} else if (reply.equalsIgnoreCase("N")) {						
					answered = true;
				} else {
					System.out.print("Please reply with 'Y' or 'N'");
					reply = scanner.nextLine();
				}
			} else {
				System.out.print("Please reply with 'Y' or 'N'");
				reply = scanner.next();
			}					
		}
		scanner.close();
		return proceed;
	}

	protected void reportAndTerminate(final Response resp, String errType, Exception e) {
		String reason = e.getMessage();
		String cause = null; 
		if (e.getCause() !=  null) {
			cause = e.getCause().toString();
		}
		resp.setMessage("Error", "*** the following errors occurred ***");
		resp.setMessage(errType, reason);
		if (cause != null) {
			resp.setMessage("Cause", cause);
		}
		if (e instanceof ValidationException) {
			((ValidationException) e).getErrorMessages().forEach((k,v) -> {
				resp.setMessage(k, v);
			} );
		}
		log.error(resp.getAllMessages().get());
		System.err.println(resp.getAllMessages());
		// Maven Surefire plugin doesn't support System.exit();, so throw an unchecked exception
		throw new RuntimeException("Error - force non-zero return code");
	}
}
