package com.w3p.im.iib.mon.mq.files;

public class MQFilesUtilityApp {
	
	
	public static void main(String[] args) {		
		// Create the utility
		MQFilesUtility mfu = new MQFilesUtility();
		// Run it
		run(mfu, args);
	}

	protected static void run(MQFilesUtility mfu, String[] args) {
		mfu.run(args);		
	}

}
