package com.w3p.im.iib.mon.mq.files.constants;

public interface IConstants {
	
	// Types of Request
	public static final String FILES_TO_QUEUE = "FilesToQueue";
	public static final String QUEUE_TO_FILES = "QueueToFiles";
	public static final String QUEUE_TO_QUEUE = "QueueToQueue";
	
	// Action values
	public static final String ACTION = "a";
	public static final String COPY = "copy";
	public static final String MOVE = "move";
	
	// Properties files arg name
	public static final String PROPS_FILE = "pf";
	public static final String DFLT_MQ_FILES_PROPERTIES = "mqFilesUtility.properties"; // default properties file name
	
	// Input properties/args values
	public static final String I_PASSWORD = "ipw";
	public static final String I_USERID = "iu";
	public static final String I_SERVER_CHANNEL = "isc";
	public static final String I_PORT = "ip";
	public static final String I_HOST = "ih";
	public static final String I_QUEUE = "iq";
	public static final String I_QUEUE_MANAGER = "iqm";
	public static final String I_FOLDER = "if";
	public static final String I_FILE_NAME_PATTERN = "ifnp";
	public static final String MSG_SELECTOR = "ms";
	public static final String READBUFFER_SIZE = "readBufSize"; // files-reading performance - size of buffer for each 'chunk' of file
	
	// Output properties/args values
	public static final String O_PASSWORD = "opw";
	public static final String O_USERID = "ou";
	public static final String O_SERVER_CHANNEL = "osc";
	public static final String O_PORT = "op";
	public static final String O_HOST = "oh";
	public static final String O_QUEUE = "oq";
	public static final String O_QUEUE_MANAGER = "oqm";
	public static final String O_FOLDER = "of";
	public static final String O_FILE_NAME_PATTERN = "ofnp";
	public static final String MSGPROP = "msgprop";
	public static final String PUT_WAIT_INTERVAL = "pwi";
	public static final String GEN_CORREL_ID = "gcid";	
	
	// General
	public static final String CONN_DATA = "connData";
		
}
