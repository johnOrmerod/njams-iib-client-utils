package com.w3p.im.iib.mon.mq.files.data;

import static com.w3p.im.iib.mon.mq.files.constants.IConstants.I_HOST;
import static com.w3p.im.iib.mon.mq.files.constants.IConstants.I_PORT;
import static com.w3p.im.iib.mon.mq.files.constants.IConstants.I_QUEUE;
import static com.w3p.im.iib.mon.mq.files.constants.IConstants.I_QUEUE_MANAGER;
import static com.w3p.im.iib.mon.mq.files.constants.IConstants.I_SERVER_CHANNEL;

import java.util.Map;
import java.util.Optional;

public class ConnectionDataIn extends ConnectionData {

	public ConnectionDataIn() {
	}
	
	/**
	 * Only run this after ConnectionData has been set.
	 * @return Optional 
	 */
	public Optional<Map<String, String>> isConnectionDataComplete() {		
		if (requiredVars.isEmpty()) {
			requiredVars.put(I_QUEUE_MANAGER, qmgr);
			requiredVars.put(I_QUEUE, queue);
			requiredVars.put(I_HOST, host);
			requiredVars.put(I_PORT, port);
			requiredVars.put(I_SERVER_CHANNEL, serverChan);
		}
		
		return super.isConnectionDataComplete();
	}
}
