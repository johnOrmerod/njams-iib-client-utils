package com.w3p.im.iib.mon.mq.files.data;

import static com.w3p.im.iib.mon.mq.files.constants.IConstants.O_HOST;
import static com.w3p.im.iib.mon.mq.files.constants.IConstants.O_PORT;
import static com.w3p.im.iib.mon.mq.files.constants.IConstants.O_QUEUE;
import static com.w3p.im.iib.mon.mq.files.constants.IConstants.O_QUEUE_MANAGER;
import static com.w3p.im.iib.mon.mq.files.constants.IConstants.O_SERVER_CHANNEL;

import java.util.Map;
import java.util.Optional;

public class ConnectionDataOut extends ConnectionData {

	public ConnectionDataOut() {
	}
	
	/**
	 * Only run this after ConnectionData has been set.
	 * @return Optional 
	 */
	public Optional<Map<String, String>> isConnectionDataComplete() {		
		if (requiredVars.isEmpty()) {
			requiredVars.put(O_QUEUE_MANAGER, qmgr);
			requiredVars.put(O_QUEUE, queue);
			requiredVars.put(O_HOST, host);
			requiredVars.put(O_PORT, port);
			requiredVars.put(O_SERVER_CHANNEL, serverChan);
		}
		
		return super.isConnectionDataComplete();
	}

}
