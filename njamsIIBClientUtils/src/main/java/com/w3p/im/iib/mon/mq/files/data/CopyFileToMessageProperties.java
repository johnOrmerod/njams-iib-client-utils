package com.w3p.im.iib.mon.mq.files.data;

import java.nio.file.Path;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

public class CopyFileToMessageProperties extends CopyProperties {

	protected boolean createCorrelId;
	protected byte[] correlId;
	protected Map<String, String> msgProperties = new HashMap<>();

	public CopyFileToMessageProperties(Path filePath) {
		super(filePath);
		// TODO Auto-generated constructor stub
	}

	public boolean isCreateCorrelId() {
		return createCorrelId;
	}

	public void setCreateCorrelId(boolean createCorrelId) {
		this.createCorrelId = createCorrelId;
	}

	public byte[] getCorrelId() {
		return correlId;
	}

	public void setCorrelId(byte[] corerelId) {
		this.correlId = corerelId;
	}

	public Map<String, String> getMsgProperties() {
		return msgProperties;
	}

	public void setMsgProperties(Map<String, String> msgProps) {
		this.msgProperties = msgProps;
	}

	@Override
	public String toString() {
		return "CopyFileToMessageProperties [createCorrelId=" + createCorrelId + ", correlId="
				+ Arrays.toString(correlId) + ", msgProperties=" + msgProperties + "]";
	}

}
