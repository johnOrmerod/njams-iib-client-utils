package com.w3p.im.iib.mon.mq.files.data;

import java.io.IOException;
import java.io.InputStream;
import java.util.Map.Entry;

import com.w3p.im.iib.mon.mq.files.constants.IConstants;
import com.w3p.im.iib.mon.mq.files.exceptions.PropertiesException;

import java.util.Properties;

public class MQFilesProperties {
	

	public MQFilesProperties() {
	}
	
	public Properties getProperties() throws PropertiesException {
		return getProperties(IConstants.DFLT_MQ_FILES_PROPERTIES); }
	
	/**
	 * This method allows jUnit tests to overide the Properties file name
	 * @param mqFilesProperties
	 * @return
	 * @throws PropertiesException
	 */
	public Properties getProperties(String mqFilesProperties) throws PropertiesException {
		Properties props = new Properties();
		
		try (InputStream is = MQFilesProperties.class.getResourceAsStream(mqFilesProperties)) {
			props.load(is);
			trimProperties(props);
//FIXME			displayAndLog(String.format("Props file '%s' was found via 'resources'", MQ_FILES_PROPERTIES));
		} catch (IOException e) {
			String reason = "An error occurred when reading the properties file '/resources/mqLoader.properties'. see log.";
//FIXME			log.error(reason, e);
			throw new PropertiesException(reason, e);
		} catch (Exception e) {
			String reason = "An error occurred when reading the properties file '/resources/mqLoader.properties'. see log.";
//FIXME			log.error(reason, e);
			throw new PropertiesException(reason, e);
		}

		return props;
	}
	
	protected void trimProperties(Properties props) {
		for (Entry<Object, Object> entry : props.entrySet()) {
			entry.setValue(entry.getValue().toString().trim());
		}
	}
	

}
