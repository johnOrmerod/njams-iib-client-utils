package com.w3p.im.iib.mon.mq.files.data;

import com.w3p.im.iib.mon.mq.files.constants.IConstants;

public class Request {
	
	protected String reqType; // 'FilesToQueue' etc
	protected String action;
	protected RequestDataIn inData;
	protected RequestDataOut outData;
	
	public Request() {
		setDefaultValues();
	}
	
	private void setDefaultValues() {
		action = IConstants.COPY;		
	}

	public String getReqType() {
		return reqType;
	}

	public void setReqType(String reqType) {
		this.reqType = reqType;
	}

	public String getAction() {
		return action;
	}

	public void setAction(String action) {
		this.action = action;
	}

	public RequestDataIn getInData() {
		return inData;
	}

	public void setInData(RequestDataIn inData) {
		this.inData = inData;
	}

	public RequestDataOut getOutData() {
		return outData;
	}

	public void setOutData(RequestDataOut outData) {
		this.outData = outData;
	}
}
