package com.w3p.im.iib.mon.mq.files.data;

import static com.w3p.im.iib.mon.mq.files.constants.IConstants.CONN_DATA;
import static com.w3p.im.iib.mon.mq.files.constants.IConstants.I_FOLDER;
import static com.w3p.im.iib.mon.mq.files.constants.IConstants.READBUFFER_SIZE;

import java.util.HashMap;
import java.util.Map;
import java.util.Optional;

public class RequestDataIn {
	
	// Inputs - files
	private String folder;
	private int readBufSize;
	private String msgSelector;
	
	// File name pattern - for filtering input files and naming output files.
	private String fileNamePattern;
	
	// Connection data
	private ConnectionDataIn connData;

	private final Map<String, Object> requiredVarsFilesIn;  // These variables must not be null
	private final Map<String, Object> requiredVarsQueueIn;	
			
		
	public RequestDataIn() {
		connData = new ConnectionDataIn();
		requiredVarsFilesIn = new HashMap<>();
		requiredVarsQueueIn = new HashMap<>();
		setDefaultValues();
	}
	

	private void setDefaultValues() {
		setReadBufSize(4096);		
	}


	public Optional<Map<String, String>> isRequestDataCompleteFilesIn() {
		final Map<String, String> invalidValues = new HashMap<>();
		requiredVarsFilesIn.forEach((k, v) -> {
			// (At the time of coding!) It's safe to assume that a required variable is either a String or an int 
			if (v == null || (v.getClass().isAssignableFrom(String.class) && ((String)v).isEmpty())) {				
				invalidValues.put(k, "is missing - it must be specified");
			} else if (v.getClass().isAssignableFrom(Integer.class) && ((Integer)v) <= 0) {
				invalidValues.put(k, "must be specified and be greater than zero");
			}
		});
		
		return invalidValues.isEmpty() ? Optional.empty() : Optional.of(invalidValues);
	}


	public Optional<Map<String, String>> isRequestDataCompleteQueueIn() {
		final Map<String, String> invalidValues = new HashMap<>();
		requiredVarsQueueIn.forEach((k, v) -> {
			// (At the time of coding!) It's safe to assume that a required variable is either a String or an int 
			if (v == null || (v.getClass().isAssignableFrom(String.class) && ((String)v).isEmpty())) {				
				invalidValues.put(k, "is missing - it must be specified");
			} else if (v.getClass().isAssignableFrom(Integer.class) && ((Integer)v) <= 0) {
				invalidValues.put(k, "must be specified and be greater than zero");
			}
		});		
		
		if (connData.isConnectionDataComplete().isPresent()) {
			invalidValues.putAll(connData.isConnectionDataComplete().get());
		}

		return invalidValues.isEmpty() ? Optional.empty() : Optional.of(invalidValues);
	}


	public String getFolder() {
		return folder;
	}

	
	public RequestDataIn setFolder(String folder) {
		this.folder = folder;
		requiredVarsFilesIn.put(I_FOLDER, folder);
		return this;
	}


	public Optional<String> getMsgSelector() {
		return msgSelector == null ? Optional.ofNullable(msgSelector) :  
			msgSelector.isEmpty() ? Optional.empty() : Optional.of(msgSelector);
	}

	
	public RequestDataIn setMsgSelector(String msgSelector) {
		this.msgSelector = msgSelector;
		return this;
	}

	
	public int getReadBufSize() {
		return readBufSize;
	}

	
	public RequestDataIn setReadBufSize(int readBufSize) {
		this.readBufSize = readBufSize;
		requiredVarsFilesIn.put(READBUFFER_SIZE, readBufSize);
		return this;
	}
	
	public Optional<String> getFileNamePattern() {
		return fileNamePattern == null ? Optional.ofNullable(fileNamePattern) : 
				fileNamePattern.isEmpty() ? Optional.empty() : Optional.of(fileNamePattern);
	}
	
	public RequestDataIn setFileNamePattern(String fileNamePattern) {
		this.fileNamePattern = fileNamePattern;				
		return this;
	}
	
	public ConnectionData getConnData() {
		return connData; // != null ? connData : connDataIn;
	}

	public RequestDataIn setQMgr(String qMgr) {
		connData.setQmgr(qMgr);
		requiredVarsQueueIn.put(CONN_DATA, connData);
		return this;
	}

	public RequestDataIn setQueue(String queue) {
		connData.setQueue(queue);
		return this;
	}

	public RequestDataIn setHost(String host) {
		connData.setHost(host);
		return this;
	}

	public RequestDataIn setPort(int port) {
		connData.setPort(port);
		return this;
	}

	public RequestDataIn setServerChan(String serverChan) {
		connData.setServerChan(serverChan);
		return this;
	}

	public RequestDataIn setUserid(String userid) {
		connData.setUserid(userid);
		return this;
	}

	public RequestDataIn setPassword(String password) {
		connData.setPassword(password);
		return this;
	}
}
