package com.w3p.im.iib.mon.mq.files.data;

import static com.w3p.im.iib.mon.mq.files.constants.IConstants.CONN_DATA;
import static com.w3p.im.iib.mon.mq.files.constants.IConstants.O_FILE_NAME_PATTERN;
import static com.w3p.im.iib.mon.mq.files.constants.IConstants.O_FOLDER;
import static com.w3p.im.iib.mon.mq.files.constants.IConstants.PUT_WAIT_INTERVAL;

import java.util.HashMap;
import java.util.Map;
import java.util.Optional;
	
public class RequestDataOut {
	
	// Inputs - files
	private String folder;
	// Outputs - queue
	private int timeToWaitSec;	
	private boolean genCorrelId;
	private final Map<String, String> msgProperties; // Message Properties

	// File name pattern - for filtering input files and naming output files.
	private String fileNamePattern;
	
	// Connection data
	private ConnectionDataOut connData;
	
	private final Map<String, Object> requiredVarsFilesOut;  // These variables must not be null
	private final Map<String, Object> requiredVarsQueueOut;	
		
		
	public RequestDataOut() {
		connData = new ConnectionDataOut();
		msgProperties = new HashMap<>(); 
		requiredVarsFilesOut = new HashMap<>();
		requiredVarsQueueOut = new HashMap<>();
	}

	public String getFolder() {
		return folder;
	}

	public RequestDataOut setFolder(String folder) {
		this.folder = folder;
		requiredVarsFilesOut.put(O_FOLDER, folder);
		return this;
	}

	public Optional<Map<String, String>> isRequestDataCompleteFilesOut() {		
		final Map<String, String> invalidValues = new HashMap<>();
		requiredVarsFilesOut.forEach((k, v) -> {
			// (At the time of coding!) It's safe to assume that a required variable is Stringt 
			if (v == null || (v.getClass().isAssignableFrom(String.class) && ((String)v).isEmpty())) {				
				invalidValues.put(k, "is missing - it must be specified");
			} else if (v.getClass().isAssignableFrom(Integer.class) && ((Integer)v) <= 0) {
				invalidValues.put(k, "must be specified and be greater than zero");
			}
		});

		return invalidValues.isEmpty() ? Optional.empty() : Optional.of(invalidValues);
	}

	public Optional<Map<String, String>> isRequestDataCompleteQueueOut() {	
		final Map<String, String> invalidValues = new HashMap<>();
		requiredVarsQueueOut.forEach((k, v) -> {
			// (At the time of coding!) It's safe to assume that a required variable is String 
			if (v == null || (v.getClass().isAssignableFrom(String.class) && ((String)v).isEmpty())) {				
				invalidValues.put(k, "is missing - it must be specified");
			} else if (v.getClass().isAssignableFrom(Integer.class) && ((Integer)v) <= 0) {
				invalidValues.put(k, "must be specified and be greater than zero");
			}
		});
		
		if (connData.isConnectionDataComplete().isPresent()) {
			invalidValues.putAll(connData.isConnectionDataComplete().get());
		}

		return invalidValues.isEmpty() ? Optional.empty() : Optional.of(invalidValues);
	}

	public int getTimeToWaitSec() {
		return timeToWaitSec;
	}

	public RequestDataOut setTimeToWaitSec(int timeToWaitInSeconds) {
		this.timeToWaitSec = timeToWaitInSeconds;
		requiredVarsQueueOut.put(PUT_WAIT_INTERVAL, timeToWaitSec);
		return this;
	}
	
	public boolean isGenCorrelId() {
		return genCorrelId;
	}
	
	public RequestDataOut setGenCorrelId(boolean genCorrelId) {
		this.genCorrelId = genCorrelId;		
		return this;
	}
	
	public Map<String, String> getMsgProperties() {
		return msgProperties;
	}
	
	public String getFileNamePattern() {
		return fileNamePattern == null ? "" : fileNamePattern;
	}
	
	public RequestDataOut setFileNamePattern(String fileNamePattern) {
		this.fileNamePattern = fileNamePattern;
		requiredVarsFilesOut.put(O_FILE_NAME_PATTERN, fileNamePattern); // Output filename format
		return this;
	}
	
	public ConnectionData getConnData() {
		return connData; // != null ? connData : connDataIn;
	}

	public RequestDataOut setQMgr(String qMgr) {
		connData.setQmgr(qMgr); // != null ? qMgr : null);
		requiredVarsQueueOut.put(CONN_DATA, connData);
		return this;
	}

	public RequestDataOut setQueue(String queue) {
		connData.setQueue(queue);
		return this;
	}

	public RequestDataOut setHost(String host) {
		connData.setHost(host);
		return this;
	}

	public RequestDataOut setPort(int port) {
		connData.setPort(port);
		return this;
	}

	public RequestDataOut setServerChan(String serverChan) {
		connData.setServerChan(serverChan);
		return this;
	}

	public RequestDataOut setUserid(String userid) {
		connData.setUserid(userid);
		return this;
	}

	public RequestDataOut setPassword(String password) {
		connData.setPassword(password);
		return this;
	}
}
