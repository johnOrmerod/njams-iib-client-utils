package com.w3p.im.iib.mon.mq.files.data;

import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Optional;
import java.util.Set;

public class Response {
	
	private Map<String, String> messages;
	private boolean success = true; // Errors will set this to false
	private int itemsToBeCopied;
	private int itemsCopied;
	private int itemsDeleted;
	private int itemsInError;;
	
	public Response() {
	}
	
	/**
	 * Intended only for JUnit
	 * @return
	 */
	protected Map<String, String> getMessages() {
		if (messages == null) {
			messages = new LinkedHashMap<>();
		}
		return messages;
	}

	/**
	 * Defaults to error
	 * @param key
	 * @param message
	 */
	public void setMessage(String key, String message) {
		setMessage(key, message, false);
	}
	public void setMessage(String key, String message, boolean success) {
		getMessages().put(key, message);
		setSuccess(success);
	}

	public boolean isSuccess() {
		return success;
	}
	private void setSuccess(boolean success) {
		this.success = success;
	}
	public Optional<String> getAllMessages() {
		StringBuilder sb = new StringBuilder();
		Set<String> keys = getMessages().keySet();
		for (String key : keys) {
			sb.append(key).append(": ").append(messages.get(key)).append("\r\n");		
		}
		return sb.length() == 0 ? Optional.empty() : Optional.of(sb.toString());
	}
	public int getItemsToBeCopied() {
		return itemsToBeCopied;
	}

	public void setItemsToBeCopied(int filesToBeCopied) {
		this.itemsToBeCopied = filesToBeCopied;
	}

	public int getItemsCopied() {
		return itemsCopied;
	}

	public void setItemsCopied(int filesCopied) {
		this.itemsCopied = filesCopied;
	}

	public int getItemsDeleted() {
		return itemsDeleted;
	}

	public void setItemsDeleted(int filesDeleted) {
		this.itemsDeleted = filesDeleted;
	}

	public int getItemsInError() {
		return itemsInError;
	}

	public void setItemsInError(int filesInError) {
		this.itemsInError = filesInError;
	}

	@Override
	public String toString() {
		return String.format("Messages = %s", messages);
	}
}
