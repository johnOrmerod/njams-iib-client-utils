package com.w3p.im.iib.mon.mq.files.exceptions;

public class InvalidArgumentException extends RuntimeException {

	private static final long serialVersionUID = 1L;
	private String msg;

	public InvalidArgumentException() {
		super();
	}

	public InvalidArgumentException(String arg, Throwable cause) {
		super(cause);
		setMsg(arg);

	}

	/**
	 * @param arg
	 */
	public InvalidArgumentException(String arg) {
		setMsg(arg);
	}

	/**
	 * Returns the value of the local 'msg' 
	 */
	@Override
	public String getMessage() {
		return msg;
	}
	
	private void setMsg(String arg) {
		msg = String.format("Invalid parameter '%s' - missing a '-'", arg); 
	}

}
