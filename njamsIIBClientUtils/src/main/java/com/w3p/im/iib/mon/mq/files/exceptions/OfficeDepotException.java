package com.w3p.im.iib.mon.mq.files.exceptions;

public class OfficeDepotException extends RuntimeException {

	private static final long serialVersionUID = 1L;

	public OfficeDepotException() {
		super();
	}

	public OfficeDepotException(String message) {
		super(message);
	}

	public OfficeDepotException(Throwable cause) {
		super(cause);
	}

	public OfficeDepotException(String message, Throwable cause) {
		super(message, cause);
	}
}
