package com.w3p.im.iib.mon.mq.files.exceptions;

public class PropertiesException extends OfficeDepotException {

	private static final long serialVersionUID = 1L;
	private String msg;

	public PropertiesException() {
		super();
	}

	public PropertiesException(String msg, Throwable cause) {
		super(cause);
		setMsg(msg);
	}
	
	public PropertiesException(String msg) {
		setMsg(msg);
	}

	@Override
	public String getMessage() {
		return msg;
	}
	
	private void setMsg(String msg) {
		this.msg = msg; 
	}

}
