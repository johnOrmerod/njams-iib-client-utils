package com.w3p.im.iib.mon.mq.files.exceptions;

import java.util.HashMap;
import java.util.Map;

public class ValidationException extends OfficeDepotException {

	private static final long serialVersionUID = 1L;
	private String msg;
	private final Map<String, String> errorMsgs = new HashMap<>();

	public ValidationException() {
		super();
	}
	
	public ValidationException(String msg) {
		this.msg = msg;
	}

	public ValidationException(String msg, Throwable cause) {
		super(cause);
		this.msg = msg;
	}

	
	public ValidationException(String msg, Map<String, String> errorMsgs) {
		this(msg);
		this.errorMsgs.putAll(errorMsgs);
	}

	public ValidationException(String msg, Map<String, String>  errorMsgs, Throwable cause) {
		this(msg, cause);
		this.errorMsgs.putAll(errorMsgs);		
	}


	@Override
	public String getMessage() {
		return msg;
	}
	
	public Map<String, String> getErrorMessages() {
		return errorMsgs;
	}
	
//	private void setMsg(String msg) {
//		this.msg = String.format("Parameter '%s' is missing, run terminated.", msg); 
//	}

//	private void replaceMsg(String msg) {
//		this.msg = msg; 
//	}
}
