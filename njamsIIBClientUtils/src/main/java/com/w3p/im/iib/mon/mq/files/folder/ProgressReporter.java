package com.w3p.im.iib.mon.mq.files.folder;

import java.nio.file.Path;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.w3p.im.iib.mon.mq.files.data.RequestDataOut;
import com.w3p.im.iib.mon.mq.files.data.Response;

public class ProgressReporter {
	private static final Logger log = LoggerFactory.getLogger(ProgressReporter.class);
	
	private final Response resp;
	private final DateTimeFormatter dtf = DateTimeFormatter.ofPattern("HH:mm:ss");// For 'running' time-stamp
	private final int runningMsgInterval;
	private int msgCount = 1;

	public ProgressReporter(RequestDataOut reqDataOut, Response resp) {
		this.resp = resp;
		runningMsgInterval = calculateMsgInterval(reqDataOut); // Write out a 'running' approx every 10 seconds
	}

	private int calculateMsgInterval(RequestDataOut reqDataOut) {
		return Math.max(1, Math.floorDiv(10, reqDataOut.getTimeToWaitSec()));
	}
	
	public void reportStartMsg() {
		displayAndLog("Started copying files... ");
	}
	
	public void displayProgress(Path pathFileName) {
		if (msgCount == runningMsgInterval) {
			LocalDateTime now = LocalDateTime.now();
			String runningMsg = String.format("running... %s Current file is '%s'. File count is '%s'.", 
					dtf.format(now), pathFileName.toFile().getName(), resp.getItemsCopied());
			displayAndLog(runningMsg);
			msgCount = 0; // reset
		}
		msgCount++;		
	}
	
	private void displayAndLog(String s) {
		System.out.println(s);
		log.info(s);
	}

}
