package com.w3p.im.iib.mon.mq.files.jms;

import javax.jms.Destination;
import javax.jms.JMSContext;
import javax.jms.JMSException;
import javax.jms.JMSRuntimeException;

import com.ibm.msg.client.jms.JmsConnectionFactory;
import com.ibm.msg.client.jms.JmsFactoryFactory;
import com.ibm.msg.client.wmq.WMQConstants;
import com.w3p.im.iib.mon.mq.files.data.ConnectionData;
import com.w3p.im.iib.mon.mq.files.exceptions.MessagingException;

public abstract class JmsConnector {
	
	protected JMSContext context; // = session + connection
	protected Destination dest;
	

	public JmsConnector() {
	}
	
	public void init(ConnectionData connData) throws MessagingException {
		// Use of 'wrapper' allows us to provide a mock for JMSFactoryFactory.getInstance()
		JmsFactoryFactoryWrapper jffw = new JmsFactoryFactoryWrapper();
		init(connData, jffw);
	}
	
	public void init(ConnectionData connData, JmsFactoryFactoryWrapper jffw) throws MessagingException {
		try {		
			JmsConnectionFactory connFac = createJmsConnectionFactory(jffw);
			configureConnFactory(connFac, connData);
			context = createJMSContext(connFac);
			dest = createDestination(context, connData);
		} catch (JMSException e) {
			String reason = String.format("Error occurred when creating the JMS Connection Factory. See log.", e.getMessage());
			throw new MessagingException(reason, e);
		} catch (JMSRuntimeException e) {
			String reason = String.format("Error occurred when creating the JMS Destination '%s'. See log.", connData.getQueue());
			throw new MessagingException(reason, e);
		} 
	}

	protected JmsConnectionFactory createJmsConnectionFactory(JmsFactoryFactoryWrapper jffw) throws JMSException {
		JmsConnectionFactory connFac =  null;
		JmsFactoryFactory jff = jffw.getFactory(WMQConstants.WMQ_PROVIDER);
		connFac = jff.createConnectionFactory();

		return connFac;
	}

	protected void configureConnFactory(JmsConnectionFactory connFac, ConnectionData conn) throws MessagingException {
		try {
			connFac.setStringProperty(WMQConstants.WMQ_HOST_NAME, conn.getHost());
			connFac.setIntProperty(WMQConstants.WMQ_PORT, conn.getPort());
			connFac.setStringProperty(WMQConstants.WMQ_CHANNEL, conn.getServerChan());
			connFac.setIntProperty(WMQConstants.WMQ_CONNECTION_MODE, WMQConstants.WMQ_CM_CLIENT);
			connFac.setStringProperty(WMQConstants.WMQ_QUEUE_MANAGER, conn.getQmgr());
			connFac.setStringProperty(WMQConstants.WMQ_APPLICATIONNAME, "MQ Files Utility App");
			connFac.setBooleanProperty(WMQConstants.USER_AUTHENTICATION_MQCSP, false);
			if (conn.getUserid() != null) {
				connFac.setBooleanProperty(WMQConstants.USER_AUTHENTICATION_MQCSP, true);
				connFac.setStringProperty(WMQConstants.USERID, conn.getUserid());
				connFac.setStringProperty(WMQConstants.PASSWORD, conn.getPassword());
			}
		} catch (JMSException e) {
			String reason = String.format("Error occurred when configuring the JMS Connection Factory. See log.", e.getMessage());
			throw new MessagingException(reason, e);
		}
	}

	protected JMSContext createJMSContext(JmsConnectionFactory connFac) throws MessagingException{
		JMSContext context = null;
		try {
			context = connFac.createContext(JMSContext.AUTO_ACKNOWLEDGE);
		} catch (JMSRuntimeException e) {
			String reason = String.format("Error occurred when creating the JMS Context. See log.", e.getMessage());
			throw new MessagingException(reason, e);
		}
		return context;
	}
	
	protected Destination createDestination(JMSContext context, ConnectionData connData) throws JMSRuntimeException {
//		try {
			dest = context.createQueue(String.format("queue:///%s", connData.getQueue()));	
//		} catch (JMSRuntimeException e) {
//			String reason = String.format("Error occurred when creating the JMS Destination '%s'. See log.", connData.getQueue());
//			throw new MessagingException(reason, e);
//		}
		return dest;	
	}

	/**
	 * This is only for jUnit testing
	 * @return
	 */
	public Destination getDest() {
		return dest;
	}
	

}
