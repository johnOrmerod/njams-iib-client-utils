package com.w3p.im.iib.mon.mq.files.processors;

import java.util.Scanner;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public abstract class AbstractProcessor {
	static protected final Logger log = LoggerFactory.getLogger(AbstractProcessor.class);

	
	protected boolean doYouWantToContinue() {
		boolean proceed = false;
		Scanner scanner = new Scanner(System.in);
		System.out.print("Please check the details above for your copy request.\nDo you want to continue? [Y or N]");
		String reply = scanner.nextLine();
		boolean answered = false;
		while (!answered) {
			if (reply != null) {
				if (reply.equalsIgnoreCase("Y") || reply.isEmpty()) {
					proceed = true;
					answered = true;
				} else if (reply.equalsIgnoreCase("N")) {						
					answered = true;
				} else {
					System.out.print("Please reply with 'Y' or 'N'");
					reply = scanner.nextLine();
				}
			} else {
				System.out.print("Please reply with 'Y' or 'N'");
				reply = scanner.next();
			}					
		}
		scanner.close();
		return proceed;
	}
}
