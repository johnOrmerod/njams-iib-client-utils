package com.w3p.im.iib.mon.mq.files.processors;

import static com.w3p.im.iib.mon.mq.files.constants.IConstants.ACTION;
import static com.w3p.im.iib.mon.mq.files.constants.IConstants.COPY;
import static com.w3p.im.iib.mon.mq.files.constants.IConstants.DFLT_MQ_FILES_PROPERTIES;
import static com.w3p.im.iib.mon.mq.files.constants.IConstants.GEN_CORREL_ID;
import static com.w3p.im.iib.mon.mq.files.constants.IConstants.I_FILE_NAME_PATTERN;
import static com.w3p.im.iib.mon.mq.files.constants.IConstants.I_FOLDER;
import static com.w3p.im.iib.mon.mq.files.constants.IConstants.I_HOST;
import static com.w3p.im.iib.mon.mq.files.constants.IConstants.I_PASSWORD;
import static com.w3p.im.iib.mon.mq.files.constants.IConstants.I_PORT;
import static com.w3p.im.iib.mon.mq.files.constants.IConstants.I_QUEUE;
import static com.w3p.im.iib.mon.mq.files.constants.IConstants.I_QUEUE_MANAGER;
import static com.w3p.im.iib.mon.mq.files.constants.IConstants.I_SERVER_CHANNEL;
import static com.w3p.im.iib.mon.mq.files.constants.IConstants.I_USERID;
import static com.w3p.im.iib.mon.mq.files.constants.IConstants.MSGPROP;
import static com.w3p.im.iib.mon.mq.files.constants.IConstants.MSG_SELECTOR;
import static com.w3p.im.iib.mon.mq.files.constants.IConstants.O_FILE_NAME_PATTERN;
import static com.w3p.im.iib.mon.mq.files.constants.IConstants.O_FOLDER;
import static com.w3p.im.iib.mon.mq.files.constants.IConstants.O_HOST;
import static com.w3p.im.iib.mon.mq.files.constants.IConstants.O_PASSWORD;
import static com.w3p.im.iib.mon.mq.files.constants.IConstants.O_PORT;
import static com.w3p.im.iib.mon.mq.files.constants.IConstants.O_QUEUE;
import static com.w3p.im.iib.mon.mq.files.constants.IConstants.O_QUEUE_MANAGER;
import static com.w3p.im.iib.mon.mq.files.constants.IConstants.O_SERVER_CHANNEL;
import static com.w3p.im.iib.mon.mq.files.constants.IConstants.O_USERID;
import static com.w3p.im.iib.mon.mq.files.constants.IConstants.PROPS_FILE;
import static com.w3p.im.iib.mon.mq.files.constants.IConstants.PUT_WAIT_INTERVAL;
import static com.w3p.im.iib.mon.mq.files.constants.IConstants.READBUFFER_SIZE;

import java.io.FileNotFoundException;
import java.util.Arrays;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Properties;

import com.w3p.im.iib.mon.mq.files.data.MQFilesProperties;
import com.w3p.im.iib.mon.mq.files.data.Request;
import com.w3p.im.iib.mon.mq.files.data.RequestDataIn;
import com.w3p.im.iib.mon.mq.files.data.RequestDataOut;
import com.w3p.im.iib.mon.mq.files.exceptions.PropertiesException;
import com.w3p.im.iib.mon.mq.files.exceptions.ValidationException;
import com.w3p.im.iib.mon.mq.files.validation.Validator;


public class CreateRequest {
	
	
	public Request create(String[] args) throws ValidationException {
		Request req = new Request();
		
		try {
			Validator validator = new Validator();
			// Validate Args
			validator.validateArgs(args);
			// Validate Properties
			String propsFile = findPropertiesFileToBeUsed(args);
			Properties props = getProperties(propsFile);
			validator.validateProperties(props, propsFile);
			// Merge CLI args and Properties into a Request.
			Map<String, String> params = createParamsFromArgsAndProperties(args, props);
			
			RequestDataIn reqDataIn = createRequestDatatInFromParams(params);		
			RequestDataOut reqDataOut = createRequestDataOutFromParams(params);
			req.setAction(params.get(ACTION) == null ? COPY : params.get(ACTION)); // Default 'action' to 'copy'
			req.setInData(reqDataIn);
			req.setOutData(reqDataOut);
			// Validate completeness of Request
			validator.validateRequest(req);
		} catch (FileNotFoundException e) {
			// For Properties file
			throw new PropertiesException("Properties file not found", e);
		}
		return req;
	}
	
	protected String findPropertiesFileToBeUsed(String[] args) throws FileNotFoundException {
		// Is there a specific .properties file defines in the CLI args?
		List<String> argsList = Arrays.asList(args);
		int fpIndex  = argsList.indexOf("-"+PROPS_FILE);
		String propsFile = fpIndex > -1 ? propsFile = argsList.get(fpIndex+1) : DFLT_MQ_FILES_PROPERTIES; 
		return propsFile;
	}

	protected Properties getProperties(String propsFile) throws FileNotFoundException {
		MQFilesProperties mfp = new MQFilesProperties(); 
		Properties props = null;
		props = mfp.getProperties(String.format("/%s", propsFile));
		if (props == null) {
			throw new FileNotFoundException(String.format("Properties file '%s' not found in 'resources' folder. Please correct and retry", propsFile));
		}
		return props;
}

	protected Map<String, String> createParamsFromArgsAndProperties(String[] args, Properties props) {
		final Map<String, String> params = new HashMap<>();
			
		// First populate the default runtime params with any values from Properties file
		Enumeration<?> names = props.propertyNames();
		while (names.hasMoreElements()) {
			String prop = (String) names.nextElement();
			params.put(prop, props.getProperty(prop));			
		}
		// Now add args and overwrite any defaults from Properties
		for (int i = 0; i < args.length; i++) {
			String arg = args[i];
			if (arg.charAt(0) == '-') {
				params.put(arg.substring(1), args[i+1]);
				i++;
			} else {
				// FIXME throw(new InvalidArgumentException(arg));
			}
		}

		return params;
	}

	protected RequestDataIn createRequestDatatInFromParams(Map<String, String> params) {
		RequestDataIn reqDataIn = new RequestDataIn();

		reqDataIn.setFolder(params.get(I_FOLDER))
		.setQMgr(params.get(I_QUEUE_MANAGER))
		.setQueue(params.get(I_QUEUE))
		.setHost(params.get(I_HOST))
		.setPort(params.get(I_PORT) != null ? Integer.parseInt(params.get(I_PORT)) : 0)
		.setServerChan(params.get(I_SERVER_CHANNEL))
		.setUserid(params.get(I_USERID))
		.setPassword(params.get(I_PASSWORD))
		.setMsgSelector(params.get(MSG_SELECTOR))
		.setFileNamePattern(params.get(I_FILE_NAME_PATTERN))
		.setReadBufSize(params.get(READBUFFER_SIZE) == null ? 0 : Integer.parseInt(params.get(READBUFFER_SIZE)));

		return reqDataIn;
	}
	
	protected RequestDataOut createRequestDataOutFromParams(Map<String, String> params) {
		RequestDataOut reqDataOut = new RequestDataOut();

		reqDataOut.setFolder(params.get(O_FOLDER))
		.setQMgr(params.get(O_QUEUE_MANAGER))
		.setQueue(params.get(O_QUEUE))
		.setHost(params.get(O_HOST))				
		.setPort(params.get(O_PORT) == null ? 0 :Integer.parseInt(params.get(O_PORT)))
		.setServerChan(params.get(O_SERVER_CHANNEL))
		.setUserid(params.get(O_USERID))
		.setPassword(params.get(O_PASSWORD))
		.setFileNamePattern(params.get(O_FILE_NAME_PATTERN))
		.setTimeToWaitSec(params.get(PUT_WAIT_INTERVAL) == null ? 0 : Integer.parseInt(params.get(PUT_WAIT_INTERVAL)))
		.setGenCorrelId(Boolean.parseBoolean(params.get(GEN_CORREL_ID)));

		// Add message properties
		params.forEach((k,v) -> {
			if (k.startsWith(MSGPROP)) {
				reqDataOut.getMsgProperties().put(k.substring(MSGPROP.length()+1), v);
			}
		});

		return reqDataOut;
	}
	
}
