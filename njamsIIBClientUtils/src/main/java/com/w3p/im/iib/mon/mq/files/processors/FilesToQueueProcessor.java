package com.w3p.im.iib.mon.mq.files.processors;

import com.w3p.im.iib.mon.mq.files.data.Request;
import com.w3p.im.iib.mon.mq.files.data.Response;
import com.w3p.im.iib.mon.mq.files.readers.FilesReader;
import com.w3p.im.iib.mon.mq.files.reporting.FilesToQueueReporter;
import com.w3p.im.iib.mon.mq.files.reporting.Reporter;
import com.w3p.im.iib.mon.mq.files.writers.IWriter;

public class FilesToQueueProcessor extends AbstractProcessor {


	public FilesToQueueProcessor() {
	}
	
	public Response process(Request req, IWriter messagesWriter) {
		log.debug("process(Request req, IWriter messagesWriter) - entry");
		Response resp = new Response();
		Reporter rep = new FilesToQueueReporter();
		// Display request to user
		rep.reportRequest(req);
		// Ask user to check before launching
		if (doYouWantToContinue()) {
			FilesReader filesReader = new FilesReader();
			filesReader.readAll(req, resp, messagesWriter); // Read all messages and write to a queue
			rep.reportResults(resp); 
			if (!resp.isSuccess()) {
				log.error("Failure - throwing a RuntimeException to force an abnormal termination");
				// Maven Surefire plugin doesn't support System.exit();, so throw an unchecked exception
				throw new RuntimeException("Error - force non-zero return code");
			}
		}
		log.debug("process(Request req, IWriter messagesWriter) - exit");
		return resp;
	}
}
