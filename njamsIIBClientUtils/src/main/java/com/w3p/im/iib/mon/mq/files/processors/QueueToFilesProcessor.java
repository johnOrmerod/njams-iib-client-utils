package com.w3p.im.iib.mon.mq.files.processors;

import java.io.IOException;

import com.w3p.im.iib.mon.mq.files.data.Request;
import com.w3p.im.iib.mon.mq.files.data.Response;
import com.w3p.im.iib.mon.mq.files.readers.MessagesReader;
import com.w3p.im.iib.mon.mq.files.reporting.QueueToFilesReporter;
import com.w3p.im.iib.mon.mq.files.reporting.Reporter;
import com.w3p.im.iib.mon.mq.files.writers.IWriter;

public class QueueToFilesProcessor extends AbstractProcessor {	
	

	public QueueToFilesProcessor() {
	}
	
	public Response process(Request req, IWriter filesWriter) throws IOException {
		log.debug("process(Request req, IWriter filesWriter) - entry");
		Response resp = new Response();
		Reporter rep = new QueueToFilesReporter();
		// Display request to user
		rep.reportRequest(req);
		// Ask user to check before launching
		if (doYouWantToContinue()) {
			try(MessagesReader messagesReader = new MessagesReader()) {
				messagesReader.readAll(req, resp, filesWriter);
				rep.reportResults(resp);
				if (!resp.isSuccess()) {
					log.error("Failure - throwing a RuntimeException to force an abnormal termination");
					// Maven Surefire plugin doesn't support System.exit();, so throw an unchecked exception
					throw new RuntimeException("Error - force non-zero return code");
				}
			}
		}
		log.debug("process(Request req, IWriter filesWriter) - exit");
		return resp;
	}
}
