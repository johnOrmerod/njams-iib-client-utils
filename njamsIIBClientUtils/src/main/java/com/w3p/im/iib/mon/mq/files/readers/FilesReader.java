package com.w3p.im.iib.mon.mq.files.readers;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.nio.ByteBuffer;
import java.nio.channels.SeekableByteChannel;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.StandardOpenOption;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.w3p.im.iib.mon.mq.files.constants.IConstants;
import com.w3p.im.iib.mon.mq.files.data.Request;
import com.w3p.im.iib.mon.mq.files.data.RequestDataIn;
import com.w3p.im.iib.mon.mq.files.data.RequestDataOut;
import com.w3p.im.iib.mon.mq.files.data.Response;
import com.w3p.im.iib.mon.mq.files.folder.ProgressReporter;
import com.w3p.im.iib.mon.mq.files.writers.IWriter;

public class FilesReader implements IReader {
	private static final Logger log = LoggerFactory.getLogger(FilesReader.class);
	
	protected Request req;
	protected IWriter writer;
	protected FilesToBeCopied filesToBeCopied;


	public FilesReader() {
	}

	protected void init(Request req, Response resp, IWriter writer) {
		this.req = req;
		this.writer = writer;
		writer.init(req.getOutData(), resp);	
	}
	
	@Override
	public void readAll(Request req, Response resp, IWriter writer) {
		init(req, resp, writer);;
		if (!resp.isSuccess()) {
			return;
		}
		RequestDataIn reqDataIn = req.getInData();
		RequestDataOut reqDataOut = req.getOutData();
		filesToBeCopied = new FindFilesToBeCopied().find(req.getInData(), resp);

		ProgressReporter progressReporter = new ProgressReporter(reqDataOut, resp);
		int readBufferSize = reqDataIn.getReadBufSize();

		for (Path filePath : filesToBeCopied.getFiles()) {
			// Read the file into a String
			String fileContent = getFileContentsAsString(filePath, readBufferSize, resp);
			// Put the file contents to a JMS Text message
			String fileName = filePath.toFile().getName(); 
			writer.write(fileName, fileContent, resp);
			if (resp.isSuccess()) {
				if (req.getAction().equals(IConstants.MOVE)) { // Delete input file if requested
					deleteAfterRead(resp, filePath, fileName);
				}
				progressReporter.displayProgress(filePath);				
				waitBeforeWritingNextMessage(reqDataOut.getTimeToWaitSec()); // Wait for n seconds before putting the next message

			}
		}
	}

	protected String getFileContentsAsString(Path filePath, int readBufferSize, Response resp) {
		
		// ByteBuffer: see https://www.javacodegeeks.com/2012/12/the-java-bytebuffer-a-crash-course.html
		ByteArrayOutputStream baos = null;
		ByteBuffer buf = null;
		String fileContent = null;
		try (SeekableByteChannel sbc = Files.newByteChannel(filePath, StandardOpenOption.READ)) {
			buf = ByteBuffer.allocate(readBufferSize);
			baos = new ByteArrayOutputStream();
			while (sbc.read(buf) > 0) {
				buf.flip();
				int bufRemain = buf.remaining();
				if (bufRemain == readBufferSize) {
					baos.write(buf.array());
				} else {
					byte[] bytes = new byte[bufRemain];
					buf.duplicate().get(bytes);
					baos.write(bytes);
				}
				buf.clear();
			}
	
			fileContent = baos.toString(StandardCharsets.UTF_8.toString());			
	
		} catch (UnsupportedEncodingException e) {
			String reason = String.format("Error converting contents of inputfile '%s' from byte-array, "
					+ "before writing to the MQ queue.  See log.", filePath.getFileName().toString());			
			resp.setMessage("Reading file", reason);
			log.error(reason, e);
		} catch (IOException e) {
			String reason = String.format("Error reading contents of inputfile '%s' before writing to the MQ queue. Reason is: '%s'. See log.",
					filePath.getFileName().toString(), e.toString());
			log.error(reason, e);
			resp.setMessage("Reading file", reason);
		} catch (Exception e) {
			String reason = String.format("Unhandled error reading contents of inputfile '%s' before writing to the MQ queue. Reason is: '%s'. See log.",
					filePath.getFileName().toString(), e.toString());
			log.error(reason, e);
			resp.setMessage("Reading file", reason);
		}
		
		return fileContent;
	}

	private void waitBeforeWritingNextMessage(int timeToWaitSec) {
		try {
			log.debug("Sleeping for {} seconds", timeToWaitSec);
			Thread.sleep(timeToWaitSec*1000);
		} catch (InterruptedException e) {
			log.error("Exception while waiting between writes to MQ", e);
		}
	}

	private void deleteAfterRead(Response resp, Path filePath, String fileName) {
		try {				
			Files.delete(filePath);
			resp.setItemsDeleted(resp.getItemsDeleted() + 1);
		} catch (IOException e) {
			String reason = String.format("Error deleting the inputfile '%s' from input folder. Reason: '%s'. See log.", fileName);
			resp.setMessage("IO error", reason);
			log.error(reason, e);
		}
	}
}
