package com.w3p.im.iib.mon.mq.files.readers;

import java.nio.file.Path;
import java.util.ArrayList;
import java.util.List;

public class FilesToBeCopied {
	
	private final List<Path> filesToBeCopied;

	public FilesToBeCopied() {
		filesToBeCopied = new ArrayList<>();
	}

	public void addFile(Path fileName) {
		filesToBeCopied.add(fileName);
	}

	public List<Path> getFiles() {
		return filesToBeCopied;
	}
}
