package com.w3p.im.iib.mon.mq.files.readers;

import java.io.IOException;
import java.nio.file.DirectoryStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.w3p.im.iib.mon.mq.files.data.RequestDataIn;
import com.w3p.im.iib.mon.mq.files.data.Response;

public class FindFilesToBeCopied {	
	private static final Logger log = LoggerFactory.getLogger(FindFilesToBeCopied.class);

	public FindFilesToBeCopied() {
	}

	public FilesToBeCopied find(RequestDataIn reqDataIn, Response resp) {
		
		FilesToBeCopied filesToBeCopied = new FilesToBeCopied();

		Path inputFolder = verifyFilesCanBeCopied(reqDataIn, resp);
		if (!resp.isSuccess()) {
			return filesToBeCopied;
		}

		// Read files from the folder and store in a List of file properties
		Path currentFile = inputFolder; // For exception reporting - it needs to be init with a valid value
		// 	If no file name pattern is provided, then get all files.
		String fileNamePattern = reqDataIn.getFileNamePattern().isPresent() ? reqDataIn.getFileNamePattern().get() : "*.*";
		// See: https://examples.javacodegeeks.com/core-java/nio/java-nio-iterate-files-directory/			
		try (DirectoryStream<Path> stream = Files.newDirectoryStream(inputFolder, fileNamePattern)) {
			for (Path file : stream) {
				if (!Files.isDirectory(file) ) { // exclude directories
					currentFile = file;				
					filesToBeCopied.addFile(file);
				}
			}
			resp.setItemsToBeCopied(filesToBeCopied.getFiles().size());
		} catch (IOException e) {
			String reason = currentFile == null? 
					String.format("Error starting to read files rom the input folder: '%s'. See log.", reqDataIn.getFolder()) :
						String.format("Error reading file '%s' from the input folder: '%s'. See log.", reqDataIn.getFolder(), currentFile.toFile().getName(), e.toString());	
			log.error(reason, e);
			resp.setMessage(currentFile.toFile().getName(), reason);
		}

		return filesToBeCopied;
	}

	private Path verifyFilesCanBeCopied(RequestDataIn reqDataIn, Response resp) {
		Path inputFolder = Paths.get(reqDataIn.getFolder());	

		// Check that the input folder exists.
		if (!Files.exists(inputFolder)) {
			String reason = String.format("Input folder '%s' does not exist.", inputFolder.toString());
			log.error(reason);
			resp.setMessage("Input folder does not exist", reason);
		}
		if (!resp.isSuccess()) {
			return inputFolder;
		}

		// Check that input folder is a folder
		if (!Files.isDirectory(inputFolder) ) {
			String reason = String.format("Input folder '%s' is not a directory.", inputFolder.toString());
			log.error(reason);
			resp.setMessage("Input folder is not a directory", reason);
		}
		if (!resp.isSuccess()) {
			return inputFolder;
		}

		// Check for an empty folder
		if (inputFolder.toFile().listFiles().length == 0) {
			String reason = String.format("Input folder '%s' is empty. No files copied.", reqDataIn.getFolder());
			log.error(reason);
			resp.setMessage("Empty folder", reason);
		}
		return inputFolder;
	}
}
