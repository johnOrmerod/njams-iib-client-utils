package com.w3p.im.iib.mon.mq.files.readers;

import com.w3p.im.iib.mon.mq.files.data.Request;
import com.w3p.im.iib.mon.mq.files.data.Response;
import com.w3p.im.iib.mon.mq.files.writers.IWriter;

public interface IReader {

	void readAll(Request req, Response resp, IWriter writer);

//	void readAll(Response resp) throws Exception;

}