package com.w3p.im.iib.mon.mq.files.readers;

import java.io.Closeable;
import java.io.IOException;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Enumeration;

import javax.jms.BytesMessage;
import javax.jms.JMSConsumer;
import javax.jms.JMSException;
import javax.jms.JMSRuntimeException;
import javax.jms.Message;
import javax.jms.Queue;
import javax.jms.QueueBrowser;
import javax.jms.QueueReceiver;
import javax.jms.TextMessage;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.ibm.msg.client.jms.DetailedInvalidSelectorRuntimeException;
import com.w3p.im.iib.mon.mq.files.constants.IConstants;
import com.w3p.im.iib.mon.mq.files.data.Request;
import com.w3p.im.iib.mon.mq.files.data.Response;
import com.w3p.im.iib.mon.mq.files.exceptions.MessagingException;
import com.w3p.im.iib.mon.mq.files.jms.JmsConnector;
import com.w3p.im.iib.mon.mq.files.jms.JmsFactoryFactoryWrapper;
import com.w3p.im.iib.mon.mq.files.writers.IWriter;

public class MessagesReader extends JmsConnector implements IReader, Closeable {
	private static final Logger log = LoggerFactory.getLogger(MessagesReader.class);
	
	protected Request req;


	public MessagesReader() {
	}

	public void init(Request req, Response resp, IWriter writer) {
		try {
			super.init(req.getInData().getConnData());
			writer.init(req.getOutData(), resp);
		} catch (MessagingException e) {
			resp.setMessage("Messages reader", e.getMessage());
		}
	}

	// For jUnit testing
	public void init(Request req, Response resp, IWriter writer, JmsFactoryFactoryWrapper jffw) {
		try {
			super.init(req.getInData().getConnData(), jffw);
			writer.init(req.getOutData(), resp);
		} catch (MessagingException e) {
			resp.setMessage("Messages reader", e.getMessage());
		}
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public void readAll(Request req, Response resp, IWriter writer) {   //browseMessagesFromQueue
		init(req, resp, writer);
		String msgSelector = req.getInData().getMsgSelector().isPresent() ? req.getInData().getMsgSelector().get() : null;
		Enumeration<Message> messages = null;
		Queue queue = (Queue) dest;
		
		// Get queue depth
		int qDepth;
		try {
			qDepth = getQueueDepth(queue, msgSelector);
			resp.setItemsToBeCopied(qDepth);
		} catch (JMSException e) {
			String reason = String.format("Error calculating queue depth by browsing all messages. Reason: '%s'.", e.toString());
			log.error(reason, e);
			resp.setMessage("Messages reader", reason);
		}		
		
		if (req.getAction().equals(IConstants.COPY)) {
			try (QueueBrowser qBrowser = context.createBrowser(queue, msgSelector)) {
				// Process all mesages
				messages =  qBrowser.getEnumeration();
				while (messages.hasMoreElements()) {					
					Message message = messages.nextElement();
					String msgText = extractMessageText(resp, message);
					if (msgText != null) {
						// findUserProperties(message); // Might be needed for Queue-to-Queue copying
						String fileName = createFileName(req); // File-name pattern has one variable for a timestamp, e.g: "name1_name2-%s.xml"
						writer.write(fileName, msgText, resp);
					}
				}
			} catch (JMSException e) {
				String reason = String.format("Error creating a text version of the current JMS message - msg count is %d'. Reason: '%s'.", resp.getItemsCopied(), e.toString());
				log.error(reason, e);
				resp.setMessage("Messages reader", reason);
				resp.setItemsInError(resp.getItemsInError() + 1);
			} catch (DetailedInvalidSelectorRuntimeException e) {
				String reason = String.format("Error in the message selector's format - terminating run. Reason: '%s'.", e.toString());
				// log.error(reason, e);
				resp.setMessage("Messages reader", reason);
				resp.setItemsInError(resp.getItemsInError() + 1);	
				throw new MessagingException(reason, e);
			} catch (JMSRuntimeException e) {
				String reason = String.format("Error creating a text version of the current JMS message - msg count is %d'. Reason: '%s'.", resp.getItemsCopied(), e.toString());
				log.error(reason, e);
				resp.setMessage("Message writer", reason);
				resp.setItemsInError(resp.getItemsInError() + 1);
			} catch (Exception e) {
				// Catch any un-handled exception/
				String reason = String.format("Unhandled error creating a text version of the current JMS message - msg count is %d'. Reason: '%s'.", resp.getItemsCopied(), e.toString());
				log.error(reason, e);
				resp.setMessage("Messages writer", reason);
				resp.setItemsInError(resp.getItemsInError() + 1);
			} finally {
				if (context != null) {context.close();} // Assume consumer has been auto-closed
			}
		} else if (req.getAction().equals(IConstants.MOVE)) {
			try (JMSConsumer consumer = context.createConsumer(dest, msgSelector)) {
				// Process all messages
				Message message  = consumer.receive(1000L);				
				while (message != null) {
					String msgText = extractMessageText(resp, message);
					if (msgText != null) {
						resp.setItemsCopied(resp.getItemsCopied() + 1);
						resp.setItemsDeleted(resp.getItemsDeleted() + 1);
						// findUserProperties(message); // Might be needed for Queue-to-Queue copying					
						String fileName = createFileName(req); // File-name pattern has one variable for a timestamp, e.g: "name1_name2-%s.xml"
						writer.write(fileName, msgText, resp);
					}
					message  = consumer.receive(1000L);
				}
			} catch (JMSException e) {
				String reason = String.format("Error creating a text version of the current JMS message - msg count is %d'. Reason: '%s'.", resp.getItemsCopied(), e.toString());
				log.error(reason, e);
				resp.setMessage("Messages reader", reason);
				resp.setItemsInError(resp.getItemsInError() + 1);
			} catch (DetailedInvalidSelectorRuntimeException e) {
				String reason = String.format("Error in the message selector's format - terminating run. Reason: '%s'.", e.toString());
				// log.error(reason, e);
				resp.setMessage("Messages reader", reason);
				resp.setItemsInError(resp.getItemsInError() + 1);	
				throw new MessagingException(reason, e);
			} catch (JMSRuntimeException e) {
				String reason = String.format("Error creating a text version of the current JMS message - msg count is %d'. Reason: '%s'.", resp.getItemsCopied(), e.toString());
				log.error(reason, e);
				resp.setMessage("Message writer", reason);
				resp.setItemsInError(resp.getItemsInError() + 1);
			} catch (Exception e) {
				// Catch any un-handled exception/
				String reason = String.format("Unhandled error creating a text version of the current JMS message - msg count is %d'. Reason: '%s'.", resp.getItemsCopied(), e.toString());
				log.error(reason, e);
				resp.setMessage("Messages writer", reason);
				resp.setItemsInError(resp.getItemsInError() + 1);
			} finally {
				if (context != null) {context.close();} // Assume consumer has been auto-closed
			}
		}
	}
	
	private int getQueueDepth(Queue queue, String msgSelector) throws JMSException {
		int depth=0;
		try (QueueBrowser qBrowser = context.createBrowser(queue, msgSelector))  {
			for (@SuppressWarnings("unchecked")
			Enumeration<Message> msg = qBrowser.getEnumeration(); msg.hasMoreElements(); )	{
				depth++;
				msg.nextElement();
			}			
		}				

		return depth;
	}

	private String extractMessageText(Response resp, Message message) throws JMSException {
		String msgText = null;
		if (message instanceof TextMessage) {
			msgText = message.getBody(String.class);	
		} else if (message instanceof BytesMessage) {
			msgText = new String(message.getBody(byte[].class));	
		} else {
			String reason = String.format("Unsupported JMS msg type '%s' - msg count is %d.", message.getClass().getName(), resp.getItemsCopied());
			resp.setMessage("Messages reader", reason);
			resp.setItemsInError(resp.getItemsInError() + 1);
		}
		return msgText;
	}

	private String createFileName(Request req) {
		DateTimeFormatter dtf = DateTimeFormatter.ofPattern("yyyyMMddhhmmssSSS");
		String now = LocalDateTime.now().format(dtf);
		String fileName = String.format(req.getOutData().getFileNamePattern(), now);
		return fileName;
	}
	
	protected void findUserProperties(Message message) throws JMSException {
		@SuppressWarnings("unchecked")
		Enumeration<String> userProps = message.getPropertyNames();
		while (userProps.hasMoreElements()) {
			String propName = userProps.nextElement();
			if (!propName.startsWith("JMS")) {
				Object o = message.getObjectProperty(propName);
				System.out.println("Property is name: "+propName+", value is: "+o.toString());
			}
			
		}
	}

	@Override
	public void close() throws IOException {
		if (context != null) {super.context.close();}
		
	}
}
