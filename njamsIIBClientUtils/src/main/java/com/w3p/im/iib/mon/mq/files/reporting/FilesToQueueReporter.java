package com.w3p.im.iib.mon.mq.files.reporting;

import com.w3p.im.iib.mon.mq.files.data.Request;
import com.w3p.im.iib.mon.mq.files.data.RequestDataIn;
import com.w3p.im.iib.mon.mq.files.data.RequestDataOut;
import com.w3p.im.iib.mon.mq.files.data.Response;

public class FilesToQueueReporter extends Reporter {

	public FilesToQueueReporter() {
		super();
	}
	
	public void reportRequest(Request req) {
		RequestDataIn inData = req.getInData();
		RequestDataOut outData = req.getOutData();
		
		displayAndLog("Source Parameters used", "");
		displayAndLog("   Action             : %s", req.getAction());
		displayAndLog("   Read-buffer size   : %d (controls the file reading efficiency)", inData.getReadBufSize());				
		displayAndLog("   Input folder       : %s", inData.getFolder());
		displayAndLog("   File-name pattern  : %s", inData.getFileNamePattern().isPresent() ? inData.getFileNamePattern().get() : "*");
		displayAndLog("Target Parameters used", "");
		displayAndLog("   Queue manager      : %s", outData.getConnData().getQmgr());		
		displayAndLog("   Output queue       : %s", outData.getConnData().getQueue());
		displayAndLog("   Host               : %s", outData.getConnData().getHost());
		displayAndLog("   Port               : %d", outData.getConnData().getPort());
		displayAndLog("   SvrConn Chan       : %s", outData.getConnData().getServerChan());
		if (outData.getConnData().getUserid() != null) {
			displayAndLog("   Userid             : %s", outData.getConnData().getUserid());
		}
		displayAndLog("   Put wait-interval  : %d (seconds)", outData.getTimeToWaitSec());
		displayAndLog("   Generate CorrrelId?: %b", outData.isGenCorrelId());
		
		if (!outData.getMsgProperties().isEmpty()) {
			displayAndLog("   Message Properties : The following properties will be added to each message:", "");
			outData.getMsgProperties().forEach((prop, value) -> {
				displayAndLog("     %s = %s", prop, value);
			});
		}
		
		displayAndLog("", "");
	}
	
	public void reportResults(Response resp) {
		displayAndLog("Results of this run", "");
		displayAndLog("   No of files in folder   : %s", resp.getItemsToBeCopied());
		displayAndLog("   No of files deleted     : %s", resp.getItemsDeleted());
		displayAndLog("   No of files with errors : %s", resp.getItemsInError());
		displayAndLog("   No of files copied to MQ: %s", resp.getItemsCopied());
		displayAndLog(resp.isSuccess() ? "   Success" : "   Failure", "");
		if (!resp.isSuccess()) {			
			System.out.println(resp.getAllMessages());
			log.info(resp.getAllMessages().get());
		}
	}

}
