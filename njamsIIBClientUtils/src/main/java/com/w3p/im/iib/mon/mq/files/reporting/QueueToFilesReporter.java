package com.w3p.im.iib.mon.mq.files.reporting;

import com.w3p.im.iib.mon.mq.files.data.Request;
import com.w3p.im.iib.mon.mq.files.data.RequestDataIn;
import com.w3p.im.iib.mon.mq.files.data.RequestDataOut;
import com.w3p.im.iib.mon.mq.files.data.Response;

public class QueueToFilesReporter extends Reporter {

	public QueueToFilesReporter() {
		super();
	}
	
	public void reportRequest(Request req) {
		RequestDataIn inData = req.getInData();
		RequestDataOut outData = req.getOutData();
		
		displayAndLog("Source Parameters used", "");
		displayAndLog("   Action             : %s", req.getAction());
		displayAndLog("   Msg selector       : %s", inData.getMsgSelector().isPresent() ? inData.getMsgSelector().get() : "");
		displayAndLog("   Queue manager      : %s", inData.getConnData().getQmgr());		
		displayAndLog("   Output queue       : %s", inData.getConnData().getQueue());
		displayAndLog("   Host               : %s", inData.getConnData().getHost());
		displayAndLog("   Port               : %d", inData.getConnData().getPort());
		displayAndLog("   SvrConn Chan       : %s", inData.getConnData().getServerChan());
		if (inData.getConnData().getUserid() != null) {
			displayAndLog("   Userid             : %s", inData.getConnData().getUserid());
		}
		
		displayAndLog("Target Parameters used", "");
		displayAndLog("   Output folder       : %s", outData.getFolder());
		displayAndLog("   File-name pattern   : %s", outData.getFileNamePattern());
		
		displayAndLog("", "");
	}
	
	public void reportResults(Response resp) {
		displayAndLog("Results of this run", "");
		displayAndLog("   No of msgs in queue     : %s", resp.getItemsToBeCopied());
		displayAndLog("   No of msgs copied       : %s", resp.getItemsCopied());
		displayAndLog("   No of msgs cleared      : %s", resp.getItemsDeleted());
		displayAndLog("   No of msgs with errors  : %s", resp.getItemsInError());		
		displayAndLog(resp.isSuccess() ? "   Success" : "   Failure", "");
		if (!resp.isSuccess()) {			
			System.out.println(resp.getAllMessages());
			log.info(resp.getAllMessages().get());
		}
	}

}
