package com.w3p.im.iib.mon.mq.files.reporting;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.w3p.im.iib.mon.mq.files.data.Request;
import com.w3p.im.iib.mon.mq.files.data.Response;


public abstract class Reporter {
	static final Logger log = LoggerFactory.getLogger(Reporter.class);

	public Reporter() {
		reportStarting();
	}
	
	
	public abstract void reportRequest(Request req);
	
	public abstract void reportResults(Response resp);
	
	protected void displayAndLog(String s, Object... o) {
		if (o[0] != null) {
			String line = String.format(s, o);
			System.out.println(line);
			log.info(line);
		}
	}

	public void reportStarting() {
		displayAndLog(" ", "");
		displayAndLog("****** MQ-Files Utility starting ******", "");
		displayAndLog(" ", "");
	}
}
