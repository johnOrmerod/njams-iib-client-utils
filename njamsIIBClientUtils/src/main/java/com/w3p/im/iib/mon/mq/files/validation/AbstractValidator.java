package com.w3p.im.iib.mon.mq.files.validation;

import static com.w3p.im.iib.mon.mq.files.constants.IConstants.ACTION;
import static com.w3p.im.iib.mon.mq.files.constants.IConstants.COPY;
import static com.w3p.im.iib.mon.mq.files.constants.IConstants.GEN_CORREL_ID;
import static com.w3p.im.iib.mon.mq.files.constants.IConstants.I_FILE_NAME_PATTERN;
import static com.w3p.im.iib.mon.mq.files.constants.IConstants.I_FOLDER;
import static com.w3p.im.iib.mon.mq.files.constants.IConstants.I_HOST;
import static com.w3p.im.iib.mon.mq.files.constants.IConstants.I_PASSWORD;
import static com.w3p.im.iib.mon.mq.files.constants.IConstants.I_PORT;
import static com.w3p.im.iib.mon.mq.files.constants.IConstants.I_QUEUE;
import static com.w3p.im.iib.mon.mq.files.constants.IConstants.I_QUEUE_MANAGER;
import static com.w3p.im.iib.mon.mq.files.constants.IConstants.I_SERVER_CHANNEL;
import static com.w3p.im.iib.mon.mq.files.constants.IConstants.I_USERID;
import static com.w3p.im.iib.mon.mq.files.constants.IConstants.MOVE;
import static com.w3p.im.iib.mon.mq.files.constants.IConstants.MSGPROP;
import static com.w3p.im.iib.mon.mq.files.constants.IConstants.MSG_SELECTOR;
import static com.w3p.im.iib.mon.mq.files.constants.IConstants.O_FILE_NAME_PATTERN;
import static com.w3p.im.iib.mon.mq.files.constants.IConstants.O_FOLDER;
import static com.w3p.im.iib.mon.mq.files.constants.IConstants.O_HOST;
import static com.w3p.im.iib.mon.mq.files.constants.IConstants.O_PASSWORD;
import static com.w3p.im.iib.mon.mq.files.constants.IConstants.O_PORT;
import static com.w3p.im.iib.mon.mq.files.constants.IConstants.O_QUEUE;
import static com.w3p.im.iib.mon.mq.files.constants.IConstants.O_QUEUE_MANAGER;
import static com.w3p.im.iib.mon.mq.files.constants.IConstants.O_SERVER_CHANNEL;
import static com.w3p.im.iib.mon.mq.files.constants.IConstants.O_USERID;
import static com.w3p.im.iib.mon.mq.files.constants.IConstants.PROPS_FILE;
import static com.w3p.im.iib.mon.mq.files.constants.IConstants.PUT_WAIT_INTERVAL;
import static com.w3p.im.iib.mon.mq.files.constants.IConstants.READBUFFER_SIZE;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.function.Function;
import java.util.function.Predicate;

public abstract class AbstractValidator {
	
	// Values for property keys and 'key' CLI args (preceded with a '-')
	protected static List<String> validKeyValues = Arrays.asList(ACTION, PROPS_FILE,
			I_FOLDER, I_FILE_NAME_PATTERN, READBUFFER_SIZE,	I_QUEUE_MANAGER, I_QUEUE, I_HOST, I_PORT, I_SERVER_CHANNEL, I_USERID, I_PASSWORD, MSG_SELECTOR, 
			O_FOLDER, O_FILE_NAME_PATTERN, O_QUEUE_MANAGER, O_QUEUE, O_HOST, O_PORT, O_SERVER_CHANNEL, O_USERID, O_PASSWORD, PUT_WAIT_INTERVAL, GEN_CORREL_ID, MSGPROP);
	
	
	protected final static Map<String, Class<?>>  keyValueClasses = new HashMap<>();
	{
		keyValueClasses.put(ACTION, String.class);
		keyValueClasses.put(GEN_CORREL_ID, Boolean.class);
		keyValueClasses.put(I_PORT, Integer.class);
		keyValueClasses.put(O_PORT, Integer.class);
		keyValueClasses.put(READBUFFER_SIZE, Integer.class);
		keyValueClasses.put(PUT_WAIT_INTERVAL, Integer.class);
	}
	
	protected final static List<String> booleanValues = Arrays.asList("true", "false");
	
	protected final static List<String> actionValues = Arrays.asList(COPY, MOVE);
	
	public AbstractValidator() {
	}
	
	
	protected Function<String, String> trimFirstChar = s -> s.substring(1);
	
	protected Predicate<String> startsWithMinus = s -> s.substring(0,1).equals("-");
		
	protected Predicate<Integer> isEven = n -> n%2 == 0;
	
	protected Predicate<Map<String, String>> doesValueMatchClass = m -> {
		final boolean[] isValid = new boolean[]{true};
		m.keySet().forEach(k -> {			
			String v = m.get(k);
			Class<?> clazz = keyValueClasses.get(k);
			if (clazz != null ) {
				if (Integer.class.isAssignableFrom(clazz)) {
					try {
						Integer.parseInt(v);	
					} catch (Exception e) {
						isValid[0] = false;
					}					
				} else if (Boolean.class.isAssignableFrom(clazz)) {
					if (!booleanValues.contains(v)) {
						isValid[0] = false;
					}	
				} else if (k.equals(ACTION)) {
					if (!actionValues.contains(v)) {
						isValid[0] = false;
					}	
				}			
			}
		});
		
		return isValid[0];
		};	
		
		protected Function<List<String>, List<String>> validateDataTypeOfValues = keyValueList -> {
			List<String> errorMsgs = new ArrayList<>();
			// Put each arg k+v in a single-entry Map
			Map<String, String> m = new HashMap<>();
			keyValueList.forEach(k -> {			
				int iKey = keyValueList.indexOf(k);
				if (isEven.test(iKey)) {
					String v = keyValueList.get(iKey+1);
						m.put(k, v);
						if (!doesValueMatchClass.test(m)) {
							errorMsgs.add(String.format("Value '%s' is not valid for '%s'. Please correct.", v, k));
						}
						m.clear();
				}
			});

			return errorMsgs;
		};


		protected Function<List<String>, Optional<List<String>>> validateKeyValues = keyList -> {
				final List<String> errorMsgs = new ArrayList<>();
		
				keyList.forEach(k -> {
					if (!validKeyValues.contains(k)) {				
						errorMsgs.add(String.format("Key '%s' is not a valid value. Please correct.", k));				
					}
				});		
		
				return errorMsgs.isEmpty() ? Optional.empty() : Optional.of(errorMsgs);
			};

}
