package com.w3p.im.iib.mon.mq.files.validation;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.function.Function;
import java.util.function.Predicate;

public class ArgsValidator extends AbstractValidator implements IArgsValidator {
	/*
	 * Guidance on using Java 8 Functiona and related topics, was found as below.
	 * 
	 * General:
	 *    https://www.oracle.com/technetwork/articles/java/architect-lambdas-part1-2080972.html
	 *    https://www.oracle.com/technetwork/articles/java/architect-lambdas-part2-2081439.html
	 * 
	 * Optional: 
	 *    https://www.oracle.com/technetwork/articles/java/java8-optional-2175753.html
	 *    https://www.baeldung.com/java-optional
	 *    https://dzone.com/articles/optional-ispresent-is-bad-for-you
	 * 
	 * Streams:
	 *       https://www.baeldung.com/java-8-streams
	 *   Example to init a List with a value of false
	 *   // See: https://stackoverflow.com/questions/36885371/lambda-expression-to-initialize-array
	 *   Supplier<Boolean> supp = () -> new Boolean("false");
	 *	 final List<Boolean> results = Stream.generate(supp).limit(args.length).collect(Collectors.toList());
     *
	 *       
	 * Function, Lambda:
	 *    https://www.baeldung.com/java-8-functional-interfaces
	 *    https://www.baeldung.com/java-8-lambda-expressions-tips
	 *    https://dzone.com/articles/functional-programming-java-8
	 *    
	 * Predicates:
	 *    https://www.baeldung.com/java-predicate-chain
	 *        
	 * Method Reference (::)
	 *    https://www.baeldung.com/java-8-double-colon-operator
	 *    
	 * Validation:
	 *  Strategy Pattern (influenced this design)
	 *    https://stackoverflow.com/questions/38184481/business-logic-validation-patterns-advices
	 *  Others:
	 *    https://dzone.com/articles/avoiding-many-if-blocks     
	 */

	public ArgsValidator() { 
	}
	
	
	protected Predicate<Map<String, String>> doesValueMatchClass = m -> {
		Map<String, String> mm = new HashMap<>(); // Avoids ConcurrentModificationException on 'm'
		m.keySet().forEach(k -> {			
			String v = m.get(k);			
			mm.put(trimFirstChar.apply(k), v);
		});
		return super.doesValueMatchClass.test(mm);
	};

	protected Function<String[], Optional<List<String>>> checkThereAreEvenNumberOfArgs = args -> {
		// args come in pairs: "-key" "value" = hence there must be an even number of them
		final List<String> errorMsgs =  new ArrayList<>();
		int argsLen = args.length;
		if (!isEven.test(argsLen)) {
			errorMsgs.add("There is an arg missing - should be an even number of key-value pairs.");
		}
			
		return errorMsgs.isEmpty() ? Optional.empty() : Optional.of(errorMsgs);
	};
	
	protected Function<String[], Optional<List<String>>> verifyAllArgsAreKeyValuePairs = args -> {
		// args come in pairs: "-key" "value" = hence even args start with "-" and odd ones do not
		final List<String> errorMsgs =  new ArrayList<>();

		List<String> argList = Arrays.asList(args);		
		argList.forEach(arg -> {
			int iKey = argList.indexOf(arg);
			if (isEven.test(iKey)) {
				if (!startsWithMinus.test(arg)) {
					errorMsgs.add(String.format("Invalid key-value pair found: '%s, %s'. Arg '%s' must start with a '-'.", arg, argList.get(iKey+1), arg));
				}
			}  else if (startsWithMinus.test(arg)) {				
				errorMsgs.add(String.format("Invalid key-value pair found: '%s, %s'. Arg '%s' must not start with a '-'.", argList.get(iKey-1), arg, arg));
			}
		});

		return errorMsgs.isEmpty() ? Optional.empty() : Optional.of(errorMsgs);
	};

	protected Function<String[], Optional<List<String>>> validateKeyValuesOfArgs = args -> {
		List<String> argList = Arrays.asList(args);
		 // Extract key items and remove the '-' sign
		List<String> propertiesList = new ArrayList<>();
		argList.forEach(arg -> {
			int iKey = argList.indexOf(arg);
			if (isEven.test(iKey)) {
				propertiesList.add(trimFirstChar.apply(arg));
			}
		});

		return super.validateKeyValues.apply(propertiesList);
	};
	
	protected Function<String[], Optional<List<String>>> validateDataTypeOfArgValues = args -> {
		List<String> argList = Arrays.asList(args);
		List<String> propertiesList = new ArrayList<>(); // '-' signs will have been removed
		argList.forEach(arg -> {
			propertiesList.add(startsWithMinus.test(arg) ? trimFirstChar.apply(arg) : arg);
		});
		
		List<String> errorMsgs = super.validateDataTypeOfValues.apply(propertiesList);
		
		return errorMsgs.isEmpty() ? Optional.empty() : Optional.of(errorMsgs);
	};
	
	@Override
	public Optional<List<String>> validate(String[] args) {
		List<String> errorMsgsList = new ArrayList<>();
		List<Function<String[], Optional<List<String>>>> validators = Arrays.asList(
				checkThereAreEvenNumberOfArgs,
				verifyAllArgsAreKeyValuePairs, 
				validateKeyValuesOfArgs,
				validateDataTypeOfArgValues);
		
		validators.forEach(v -> {
			// If there is an error message present, stop validation
			if (errorMsgsList.isEmpty()) {
				Optional<List<String>> msgs = v.apply(args);
				if (msgs.isPresent()) {
					msgs.get().forEach(m -> {
						errorMsgsList.add(m);
					});
				}
			}
		});
		
		return errorMsgsList.isEmpty() ? Optional.empty() : Optional.of(errorMsgsList);				
	}

}
