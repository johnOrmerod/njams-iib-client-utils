package com.w3p.im.iib.mon.mq.files.validation;

import java.util.List;
import java.util.Optional;

public interface IArgsValidator {

	Optional<List<String>> validate(final String[] args);
}
