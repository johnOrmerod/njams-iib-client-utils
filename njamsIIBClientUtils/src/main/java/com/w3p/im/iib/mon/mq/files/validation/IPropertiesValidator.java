package com.w3p.im.iib.mon.mq.files.validation;

import java.util.List;
import java.util.Optional;
import java.util.Properties;

public interface IPropertiesValidator {

	Optional<List<String>> validate(final Properties props);
}
