package com.w3p.im.iib.mon.mq.files.validation;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;
import java.util.Properties;
import java.util.function.Function;

public class PropertiesValidator extends AbstractValidator implements IPropertiesValidator {

	public PropertiesValidator() {
		// TODO Auto-generated constructor stub
	}
	
	// Returns 'msgprop' from 'msgprop.key' or the input value if not 'msgprop'.
	protected Function<Object, String> trimMsgProp = o -> {
		String m = (String)o;
		int dotPos = m.indexOf(".");
		if (dotPos == -1) {
			return m;
		} else {
			return m.substring(0, dotPos); 
		}		
	};

	protected Function<Properties, Optional<List<String>>> validateKeyValuesOfProperties = props -> {
		final List<String> keyList = new ArrayList<>();
		((Properties)props).keySet().forEach(k -> {
			keyList.add(trimMsgProp.apply(k)); //(String) k);
		});

		return validateKeyValues.apply(keyList);
	};

	protected Function<Properties, Optional<List<String>>> validateDataTypeOfPropertyValues = props -> {
		final List<String> propertiesList = new ArrayList<>();
		
		props.forEach((k, v) -> {
			propertiesList.add((String)k);
			propertiesList.add((String)v);
		});

		List<String> errorMsgs = super.validateDataTypeOfValues.apply(propertiesList);
		
		return errorMsgs.isEmpty() ? Optional.empty() : Optional.of(errorMsgs);
	};


	@Override
	public Optional<List<String>> validate(Properties props) {
		final List<String> errorMsgsList =  new ArrayList<>();
		List<Function<Properties, Optional<List<String>>>> validators = Arrays.asList(
				validateKeyValuesOfProperties,
				validateDataTypeOfPropertyValues);

		validators.forEach(v -> {
			// If there is an error message present, stop validation
			if (errorMsgsList.isEmpty()) {
				Optional<List<String>> msgs = v.apply(props);
				if (msgs.isPresent()) {
					msgs.get().forEach(m -> {
						errorMsgsList.add(m);
					});
				}
			}
		});
		
		return errorMsgsList.isEmpty() ? Optional.empty() : Optional.of(errorMsgsList);				
	}
}
