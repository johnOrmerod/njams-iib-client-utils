package com.w3p.im.iib.mon.mq.files.validation;

import static com.w3p.im.iib.mon.mq.files.constants.IConstants.FILES_TO_QUEUE;
import static com.w3p.im.iib.mon.mq.files.constants.IConstants.QUEUE_TO_FILES;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.Properties;

import com.w3p.im.iib.mon.mq.files.data.Request;
import com.w3p.im.iib.mon.mq.files.data.RequestDataIn;
import com.w3p.im.iib.mon.mq.files.data.RequestDataOut;
import com.w3p.im.iib.mon.mq.files.exceptions.ValidationException;

public class Validator {
	

	public Validator() {
	}

	public void validateArgs(String[] args) throws ValidationException {
		// Validate Args
		ArgsValidator av = new ArgsValidator();				
		Optional<List<String>> errorMsgs = av.validate(args);
		if(errorMsgs.isPresent()) {
			throw new ValidationException(String.format("Error in CLI args - details:\n%s", errorMsgs.get()));
		}
	}
	
	public void validateProperties(Properties props, String propsFile) throws ValidationException {
		// Validate Properties
		PropertiesValidator pv = new PropertiesValidator();
		Optional<List<String>> errorMsgs = pv.validate(props);			
		if(errorMsgs.isPresent()) {
			throw new ValidationException(String.format("Error in properties file '%s' - details:\n%s", propsFile, errorMsgs.get()));
		}
	}
	
	public void validateRequest(Request req) throws ValidationException {
		RequestDataIn inData = req.getInData();
		RequestDataOut outData = req.getOutData();
		
		if (inData.getFolder() != null && outData.getConnData().getQmgr() != null) {
			// Files to Queue
			req.setReqType(FILES_TO_QUEUE);
			if(inData.isRequestDataCompleteFilesIn().isPresent() || outData.isRequestDataCompleteQueueOut().isPresent()) {
				Map<String, String> errorMsgs = inData.isRequestDataCompleteFilesIn().isPresent() ? inData.isRequestDataCompleteFilesIn().get() : new HashMap<>();
				Map<String, String> errorMsgsOut = outData.isRequestDataCompleteQueueOut().isPresent() ? outData.isRequestDataCompleteQueueOut().get() : new HashMap<>();
				if (!errorMsgs.isEmpty()) {
					errorMsgs.putAll(errorMsgsOut);
				} else {
					errorMsgs = errorMsgsOut;
				}
				throw new ValidationException("One or more required values in the CLI args or Properties, is missing.", errorMsgs); 
			}
//			if (!(inData.getReadBufSize() > 0)) {
//				throw new ValidationException(String.format("Read Buffer Size must be greater than 0. Its value is '%d", inData.getReadBufSize()));
//			}
//			if (inData.getFolder().isEmpty()) {
//				throw new ValidationException(String.format("Input folder name is missing - it must be specified"));
//			}
		} else if (inData.getConnData().getQmgr() != null && outData.getFolder() != null) {
			// Queue to Files
			req.setReqType(QUEUE_TO_FILES);
			if(inData.isRequestDataCompleteQueueIn().isPresent() || outData.isRequestDataCompleteFilesOut().isPresent()) { 
				Map<String, String> errorMsgs = inData.isRequestDataCompleteQueueIn().isPresent() ? inData.isRequestDataCompleteQueueIn().get() : new HashMap<>();
				Map<String, String> errorMsgsOut = outData.isRequestDataCompleteFilesOut().isPresent() ? outData.isRequestDataCompleteFilesOut().get() : new HashMap<>();
				if (!errorMsgs.isEmpty()) {
					errorMsgs.putAll(errorMsgsOut);
				} else {
					errorMsgs = errorMsgsOut;
				}
				throw new ValidationException("One or more required values in the CLI args or Properties, is missing.", errorMsgs); 
			}
//			if (outData.getFolder().isEmpty()) {
//				throw new ValidationException(String.format("Output folder name is missing - it must be specified"));
//			}
//			if (outData.getFileNamePattern().isEmpty()) {
//				throw new ValidationException(String.format("Output folder name is missing - it must be specified"));
//			}
		} else if (inData.getConnData().getQmgr() != null && outData.getConnData().getQmgr() != null) {
			// Queue to Queue
//			req.setReqType(QUEUE_TO_QUEUE);
//			if(inData.isRequestDataCompleteQueueIn().isPresent() || outData.isRequestDataCompleteQueueOut().isPresent()) {
//				// FIXMEthrow new ValidationException("One or more required values in the CLI args or Properties, is missing."); // TODO - state which one?
//			}
		} else {
			req.setReqType("invalid");
		}
		
	}
}
