package com.w3p.im.iib.mon.mq.files.writers;

import java.io.Closeable;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardOpenOption;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.w3p.im.iib.mon.mq.files.data.RequestDataOut;
import com.w3p.im.iib.mon.mq.files.data.Response;

public class FilesWriter implements IWriter, Closeable {
	private static final Logger log = LoggerFactory.getLogger(FilesWriter.class);
	
	protected Path folderPath;
	protected RequestDataOut reqDataOut;

	public FilesWriter() {
	}
	
	@Override
	public void init(RequestDataOut reqDataOut, Response resp) {
		this.reqDataOut = reqDataOut;
		String pathToFolder = reqDataOut.getFolder();
		try {
			boolean createFolderIfNotExists = true; //TODO Add new option
			folderPath = Paths.get(pathToFolder);			
			if (Files.notExists(folderPath)) {
				if (createFolderIfNotExists) {
					log.info("Folder '{}' does not exist, creating it", pathToFolder);
					Files.createDirectories(folderPath);
				} else {
					String errMsg = String.format("Folder '%s' does not exist. Pease create it or use the option to auto-create it", pathToFolder);
					log.warn(errMsg);
					resp.setMessage("Writing file", errMsg);
				}
			} else {
				// Check folder is empty
				boolean clearFolderIfNotEmpty = true;
				if (folderPath.toFile().listFiles().length > 0) {
					if (clearFolderIfNotEmpty) {
						deleteFilesInTargetFolder(folderPath);
					}else {
						String errMsg = String.format("Folder '%s' is not empty. Pease clear it or use the option to auto-clear it", pathToFolder);
						log.warn(errMsg);
						resp.setMessage("Writing file", errMsg);
					}
				}
			}
		} catch (IOException e) {
			String reason = String.format("Error initialising the folder '%s' to which messages will be writtem. See the log." , pathToFolder); //, e.toString());
			log.error(reason, e);
			resp.setMessage("Writing file", reason);
		}
	}
	
		@Override
		public void write(String fileName, String message, Response resp) {
			Path filePath = Paths.get(folderPath.toString(), fileName);
			resp.setItemsCopied(resp.getItemsCopied() + 1);
			try {
				Files.write(filePath, message.getBytes(), StandardOpenOption.CREATE);
			} catch (IOException e) {
				String reason = String.format("Error writing the file '%s' from the input message. See the log." , fileName); //, e.toString());
				log.error(reason, e);
				resp.setMessage("Writing file", reason);
				resp.setItemsInError(resp.getItemsInError() + 1); //FIXME should be 'messages in error' etc
			} catch (Exception e) {
				// Catch any unhandled exception
				String reason = String.format("Unhandled error writing the output file '%s' frfom the Message. See the log.", fileName);
				log.error(reason, e);
				resp.setMessage("Writing file", reason);
				resp.setItemsInError(resp.getItemsInError() + 1);
			}
		}

	protected void deleteFilesInTargetFolder(Path folderPath) throws IOException {
		// See: https://stackoverflow.com/questions/35988192/java-nio-most-concise-recursive-directory-delete
		final List<Path> pathsToDelete = Files.walk(folderPath).sorted(Comparator.reverseOrder()).collect(Collectors.toList());
		for(Path path : pathsToDelete) {
			// Allows exceptions can be caught
			if (path != folderPath) {
				Files.deleteIfExists(path); // Do not delete the target folder
			}
		}
		log.info("Cleared existing messages in target folder ({})", folderPath.toString());
	}

	@Override
	public void close() throws IOException {
		// TODO Auto-generated method stub		
	}

}
