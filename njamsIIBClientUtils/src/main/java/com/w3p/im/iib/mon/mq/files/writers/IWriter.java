package com.w3p.im.iib.mon.mq.files.writers;

import com.w3p.im.iib.mon.mq.files.data.RequestDataOut;
import com.w3p.im.iib.mon.mq.files.data.Response;

public interface IWriter {

	void init(RequestDataOut reqDataOut, Response resp);

	void write(String fileName, String message, Response resp);

}