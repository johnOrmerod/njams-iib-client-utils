package com.w3p.im.iib.mon.mq.files.writers;

import java.io.Closeable;
import java.util.UUID;

import javax.jms.JMSException;
import javax.jms.JMSProducer;
import javax.jms.JMSRuntimeException;
import javax.jms.TextMessage;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.ibm.msg.client.wmq.WMQConstants;
import com.w3p.im.iib.mon.mq.files.data.RequestDataOut;
import com.w3p.im.iib.mon.mq.files.data.Response;
import com.w3p.im.iib.mon.mq.files.exceptions.MessagingException;
import com.w3p.im.iib.mon.mq.files.jms.JmsConnector;
import com.w3p.im.iib.mon.mq.files.jms.JmsFactoryFactoryWrapper;

public class MessagesWriter extends JmsConnector implements IWriter, Closeable {	
	private static final Logger log = LoggerFactory.getLogger(MessagesWriter.class);
	
	protected JMSProducer producer;
	protected RequestDataOut reqDataOut;


	public MessagesWriter() { 
	}

	@Override
	public void init(RequestDataOut reqDataOut, Response resp) {
		try {
			super.init(reqDataOut.getConnData());
			this.reqDataOut = reqDataOut;
			producer = context.createProducer();			
		} catch (MessagingException e) {
			resp.setMessage("Messages writer", e.getMessage());
		}
	}	

	// For jUnit testing
	public void init(RequestDataOut reqDataOut, Response resp, JmsFactoryFactoryWrapper jffw) {
		try {
			super.init(reqDataOut.getConnData(), jffw);
			this.reqDataOut = reqDataOut;
			producer = context.createProducer();
		} catch (MessagingException e) {
			resp.setMessage("Messages writer", e.getMessage());
		}
	}

	public void write(String fileName, String msgContent, Response resp)  {
		try {
			TextMessage message = createMessage(msgContent);
			producer.send(dest, message);
			resp.setItemsCopied(resp.getItemsCopied() + 1);
		} catch (JMSException e) {
			String reason = String.format("Error setting properties for the JMS Text Message for file '%s'. Reason: '%s'.", fileName, e.toString());
			log.error(reason, e);
			resp.setMessage("Messages writer", reason);
			resp.setItemsInError(resp.getItemsInError() + 1);
		} catch (JMSRuntimeException e) {
			String reason = String.format("Error trying to send the JMS message to destination '%s'. Reason: '%s'.", dest, e.toString());
			log.error(reason, e);
			resp.setMessage("Messages writer", reason);
			resp.setItemsInError(resp.getItemsInError() + 1);
		} catch (Exception e) {
			// Catch any un-handled exception, such as UnmappableCharacterException from 'foreign' XML files
			String reason = String.format("Unhandled error creating the JMS Text Message for file '%s'. Reason: '%s'.", fileName, e.toString());
			log.error(reason, e);
			resp.setMessage("Messages writer", reason);
			resp.setItemsInError(resp.getItemsInError() + 1);
		}
	}
	
	protected TextMessage createMessage(String msgContent) throws JMSRuntimeException, JMSException {
		TextMessage message = context.createTextMessage(); // throws JMSRuntimeException
		
		// UTF-8 This is needed to ensure that special characters. such as 'TM' superscript are handled.
		message.setIntProperty(WMQConstants.JMS_IBM_CHARACTER_SET, 1208);
		message.setText(msgContent);
		
		// Set a correlationId if required
		if (reqDataOut.isGenCorrelId()) {
			message.setJMSCorrelationIDAsBytes(UUID.randomUUID().toString().getBytes());
		}
		
		// set any message properties
		for (String k : reqDataOut.getMsgProperties().keySet()) {
			message.setStringProperty(k,  reqDataOut.getMsgProperties().get(k));
		}			

		return message;
	}
	
	public void close() {
		if (context != null) {super.context.close();}		
	}
	
	

}
