package com.w3p.im.iib.mon.mq.files;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.fail;
import static org.mockito.Matchers.anyString;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import javax.jms.JMSContext;
import javax.jms.JMSException;
import javax.jms.Queue;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Ignore;
import org.junit.Test;

import com.ibm.msg.client.jms.JmsConnectionFactory;
import com.ibm.msg.client.jms.JmsFactoryFactory;
import com.ibm.msg.client.wmq.WMQConstants;
import com.w3p.im.iib.mon.mq.files.MQFilesUtility;
import com.w3p.im.iib.mon.mq.files.constants.IConstants;
import com.w3p.im.iib.mon.mq.files.data.ConnectionData;
import com.w3p.im.iib.mon.mq.files.data.Request;
import com.w3p.im.iib.mon.mq.files.data.RequestDataIn;
import com.w3p.im.iib.mon.mq.files.data.RequestDataOut;
import com.w3p.im.iib.mon.mq.files.jms.JmsFactoryFactoryWrapper;
import com.w3p.im.iib.mon.mq.files.processors.FilesToQueueProcessor;
import com.w3p.im.iib.mon.mq.files.processors.QueueToFilesProcessor;

public class TestMQFilesUtility {

	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
	}

	@AfterClass
	public static void tearDownAfterClass() throws Exception {
	}

	@Before
	public void setUp() throws Exception {
	}

	@After
	public void tearDown() throws Exception {
	}

	@Test
	public void testCanCreateMQFilesUtility() {
		MQFilesUtility mfu = new MQFilesUtility();
		assertNotNull(mfu);
	}

	@Test
	public void testCanValidateAndReportValidRequest() {
		String[] args = new String[] {"-oqm", "IB10QMGR", "-oq", "NJAMS.UTILS.TEST.OUT", "-if", "C:\\TestFiles\\eis"};
		MQFilesUtility mfu = new MQFilesUtility() {
			@Override
			protected FilesToQueueProcessor createFilesToQueueProcessor() {
				FilesToQueueProcessor ftqp = new FilesToQueueProcessor() {
					@Override
					protected boolean doYouWantToContinue() {
						return false;
					}};
				return ftqp;
			}
		};
		
		mfu.run(args);
	}
	
	@Test
	public void testCanHandleAndReportInvalidArg() {
		// "-f" should be "-if"
		String[] args = new String[] {"-oqm", "IB10QMGR", "-oq", "NJAMS.UTILS.TEST.OUT", "-f", "C:\\TestFiles\\eis"};
		try {
			MQFilesUtility mfu = new MQFilesUtility();
			mfu.run(args);
			fail("Validation should have failed");
		} catch (RuntimeException e) {
			assertEquals("Error - force non-zero return code", e.getMessage());
		}
	}
	
	@Test
	public void testCanHandleAndReportMissingRequiredArg() {
		// "-ofnp" (output file-name pattern) is missing - can't create a file unless it's name format is known.
		String[] args = new String[] {"-iqm", "IB10QMGR", "-iq", "NJAMS.UTILS.TEST.IN", "-of", "C:\\TestFiles\\eisOut"};
		try {
			MQFilesUtility mfu = new MQFilesUtility();
			mfu.run(args);
			fail("Validation should have failed");
		} catch (RuntimeException e) {
			assertEquals("Error - force non-zero return code", e.getMessage());
		}
	}
	
	@Ignore("Relies on input folder existing and local MQ being started")
	@Test
	public void testCanRunFilesToQueue() {
		String[] args = new String[] {"-oqm", "IB10QMGR", "-oq", "NJAMS.UTILS.TEST.OUT", "-if", "C:\\TestFiles\\eis"};
		MQFilesUtility mfu = new MQFilesUtility() {
			@Override
			protected FilesToQueueProcessor createFilesToQueueProcessor() {
				FilesToQueueProcessor ftqp = new FilesToQueueProcessor() {
					@Override
					protected boolean doYouWantToContinue() {
						return true;
					}};
				return ftqp;
			}
		};
		
		mfu.run(args);
	}

	@Ignore("Relies on input folder existing and local MQ being started")
	@Test
	public void testCanRunQueueToFiles() {
		String[] args = new String[] {"-iqm", "IB10QMGR", "-iq", "NJAMS.UTILS.TEST.OUT", "-of", "C:\\TestFiles\\eisout", "-ofnp", "fromMQ_%s.xml"};
		MQFilesUtility mfu = new MQFilesUtility() {
			@Override
			protected QueueToFilesProcessor createQueueToFilesProcessor() {
				QueueToFilesProcessor qtfp = new QueueToFilesProcessor() {
					@Override
					protected boolean doYouWantToContinue() {
						return true;
					}};
				return qtfp;
			}
		};
		
		mfu.run(args);
	}
	
	@Test
	public void testCanMockJMS() {
		String[] args = new String[] {"-oqm", "QMLTGW0X", "-oq", "NJAMS.UTILS.TEST.OUT", "-if", "C:\\TestFiles\\eis"};

		// Override the run() method
		MQFilesUtility mfu = new MQFilesUtility() {
			
			@Override
			public void run(String[] args) {
				Request req = null;
				ConnectionData outConn = null;
				req = super.createRequest(args);
				outConn = req.getOutData().getConnData();				
				JmsConnectionFactory connFac = mock(JmsConnectionFactory.class);				
				JmsFactoryFactory ff = mock(JmsFactoryFactory.class);
				
				try {
					when(ff.createConnectionFactory()).thenReturn(connFac);
					
					JmsFactoryFactoryWrapper jffw = mock(JmsFactoryFactoryWrapper.class);
					when(jffw.getFactory(WMQConstants.WMQ_PROVIDER)).thenReturn(ff);
					
					JmsConnectionFactory outConnFac = super.createJmsConnectionFactory(jffw);
					verify(jffw).getFactory(WMQConstants.WMQ_PROVIDER);
					verify(ff).createConnectionFactory();					
					
					super.configureConnFactory(outConnFac, outConn);
					verify(outConnFac, times(4)).setStringProperty(anyString(), anyString());
					verify(outConnFac, times(1)).setIntProperty(WMQConstants.WMQ_CONNECTION_MODE, WMQConstants.WMQ_CM_CLIENT);
					verify(outConnFac, times(1)).setIntProperty(WMQConstants.WMQ_PORT, outConn.getPort());
					verify(outConnFac, times(1)).setBooleanProperty(WMQConstants.USER_AUTHENTICATION_MQCSP, false);
					
					JMSContext context = mock(JMSContext.class);
					when(outConnFac.createContext(JMSContext.AUTO_ACKNOWLEDGE)).thenReturn(context);					
					context = super.createJMSContext(outConnFac);
					verify(outConnFac).createContext(JMSContext.AUTO_ACKNOWLEDGE);
					
					Queue jmsQueue = mock(Queue.class);					
					when(context.createQueue(anyString())).thenReturn(jmsQueue);
					
					super.createDestination(context, outConn);
					verify(context).createQueue(anyString());
				} catch (JMSException e) {
					fail(e.getMessage());
				} catch (Exception e) {
					fail(e.getMessage());
				}				
			}
		};
		
		mfu.run(args);
	}


	private Request createTestRequest() {
		Request req = new Request();
		req.setAction("copy");
		req.setReqType(IConstants.FILES_TO_QUEUE);
		RequestDataIn inData = new RequestDataIn();
		RequestDataOut outData = new RequestDataOut();
		// In
		inData.setFolder("C:\\TestFiles");
		inData.setReadBufSize(4096);
		// Out
		outData.setHost("testHost");
		outData.setPassword(null);
		outData.setPort(3414);
		outData.setQMgr("QMgr");
		outData.setQueue("Queue");
		outData.setServerChan("SVR.CHAN");
		outData.setTimeToWaitSec(1);		
		outData.getMsgProperties().put("game", "football");
		outData.getMsgProperties().put("team", "Spurs");
		
		req.setInData(inData);
		req.setOutData(outData);
		return req;
	}
}
