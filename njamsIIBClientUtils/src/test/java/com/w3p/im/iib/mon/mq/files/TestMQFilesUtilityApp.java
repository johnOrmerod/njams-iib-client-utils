package com.w3p.im.iib.mon.mq.files;

import static org.junit.Assert.assertNotNull;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import com.w3p.im.iib.mon.mq.files.MQFilesUtilityApp;
import com.w3p.im.iib.mon.mq.files.data.Request;

public class TestMQFilesUtilityApp {

	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
	}

	@AfterClass
	public static void tearDownAfterClass() throws Exception {
	}

	@Before
	public void setUp() throws Exception {
	}

	@After
	public void tearDown() throws Exception {
	}

	@Test
	public void testCanCreateApp() {
		MQFilesUtilityApp mfua = new MQFilesUtilityApp();
		assertNotNull(mfua);
	}
}
