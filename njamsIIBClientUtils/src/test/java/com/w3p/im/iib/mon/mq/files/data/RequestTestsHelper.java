package com.w3p.im.iib.mon.mq.files.data;

import com.w3p.im.iib.mon.mq.files.data.ConnectionData;
import com.w3p.im.iib.mon.mq.files.data.Request;
import com.w3p.im.iib.mon.mq.files.data.RequestDataIn;
import com.w3p.im.iib.mon.mq.files.data.RequestDataOut;

public class RequestTestsHelper {

	public RequestTestsHelper() {
	}
	
	public Request createRequestFilesToQueue(String action) {
		ConnectionData cd = createConnectionDataOut();
		return createRequestFilesToQueue(action, cd);
	}
	public Request createRequestFilesToQueue(String action, ConnectionData cd) {		
		Request req = new Request();
		req.setAction(action);
		req.setInData(createRequestDataFilesIn());
		req.setOutData(createRequestDataQueueOut(cd));
		return req;
	}
	
	public Request createRequestQueueToFiles(String action) {
		ConnectionData cd = createConnectionDataIn();
		return createRequestQueueToFiles(action, cd);
	}
	
	public Request createRequestQueueToFiles(String action, ConnectionData cd) {
		Request req = new Request();
		req.setAction(action);
		req.setInData(createRequestDataQueueIn(cd));
		req.setOutData(createRequestDataFilesOut());
		return req;
	}
	
	public Request createRequestQueueToQueue(String action) {
		Request req = new Request();
		req.setAction(action);
		req.setInData(createRequestDataQueueIn());
		req.setOutData(createRequestDataQueueOut());
		return req;
	}

	public RequestDataIn createRequestDataFilesIn() {				
		RequestDataIn rdi = new RequestDataIn();
		
//		rdi.setFileNamePattern("xxx*.xml");
		rdi.setFolder("C:\\inFiles");		
		rdi.setReadBufSize(4096);
					
		return rdi;				
	}
	
	public RequestDataOut createRequestDataQueueOut() {
		ConnectionData cd = createConnectionDataOut();
		return createRequestDataQueueOut(cd);
	}
	
	public RequestDataOut createRequestDataQueueOut(ConnectionData cd) {
		RequestDataOut rdo = new RequestDataOut();
		
		rdo.setTimeToWaitSec(1);
		rdo.setGenCorrelId(false);
		rdo.getMsgProperties().put("team", "Spurs");
		
		rdo.setHost(cd.getHost());
		rdo.setPassword(cd.getUserid());
		rdo.setPort(cd.getPort());
		rdo.setQMgr(cd.getQmgr());
		rdo.setQueue(cd.getQueue());
		rdo.setServerChan(cd.getServerChan());		
		rdo.setUserid(cd.getUserid());
		
		return rdo;				
	}
	
	public RequestDataIn createRequestDataQueueIn() {
		ConnectionData cd = createConnectionDataIn();
		return createRequestDataQueueIn(cd);
	}
	
	public RequestDataIn createRequestDataQueueIn(ConnectionData cd ) {
		RequestDataIn rdi = new RequestDataIn();
		
		rdi.setMsgSelector("team = 'Spurs'");
		rdi.setHost(cd.getHost());
		rdi.setPassword(cd.getUserid());
		rdi.setPort(cd.getPort());
		rdi.setQMgr(cd.getQmgr());
		rdi.setQueue(cd.getQueue());
		rdi.setReadBufSize(4096);
		rdi.setServerChan(cd.getServerChan());
		rdi.setUserid(cd.getUserid());
		
		return rdi;				
	}
	
	public RequestDataOut createRequestDataFilesOut() {
		RequestDataOut rdo = new RequestDataOut();
		
		rdo.setFileNamePattern("xxx%s.xml");
		rdo.setFolder("C:\\outFiles");		
		
		return rdo;				
	}

	public ConnectionData createConnectionDataOut() {
		ConnectionData connData = new ConnectionData();
		connData
		.setHost("localhost")
		.setPort(3414)
		.setServerChan("DEV.APP.SVRCONN")
		.setQmgr("IB10QMGR")
		.setQueue("NJAMS.UTILS.TEST.OUT");
//		.setUserid(njams)
//		.setPassword(njams)
		return connData;
	}
	
	public ConnectionData createConnectionDataIn() {
		ConnectionData connData = new ConnectionData();
		connData.setHost("localhost")
		.setPort(3414)
		.setServerChan("DEV.APP.SVRCONN")
		.setQmgr("QNJAMSQMGR")
		.setQueue("NJAMS.UTILS.TEST.IN");
//		.setUserid(njams)
//		.setPassword(njams)
		return connData;
	}
	
	public RequestDataOut createRequestDataOutForIB10QMGR() {
		ConnectionData connData = createConnectionDataForIB10QMGR();
		RequestDataOut reqDatOut = new RequestDataOut();
		reqDatOut.setHost(connData.getHost());		
		reqDatOut.setPort(connData.getPort());
		reqDatOut.setQMgr(connData.getQmgr());
		reqDatOut.setQueue(connData.getQueue());
		reqDatOut.setServerChan(connData.getServerChan());
		
		return reqDatOut;
	}
	
	public RequestDataIn createRequestDataInForIB10QMGR() {
		ConnectionData connData = createConnectionDataForIB10QMGR();
		RequestDataIn reqDatIn = new RequestDataIn();
		reqDatIn.setHost(connData.getHost());		
		reqDatIn.setPort(connData.getPort());
		reqDatIn.setQMgr(connData.getQmgr());
		reqDatIn.setQueue(connData.getQueue());
		reqDatIn.setServerChan(connData.getServerChan());
		
		return reqDatIn;
	}


	public ConnectionData createConnectionDataForIB10QMGR() {
		ConnectionData connData = new ConnectionData();
		connData
			.setQmgr("IB10QMGR")
			.setQueue("NJAMS.UTILS.TEST.OUT")
			.setHost("localhost")
			.setPort(3414)
			.setServerChan("DEV.APP.SVRCONN");
		return connData;
	}


}
