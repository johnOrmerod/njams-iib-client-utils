package com.w3p.im.iib.mon.mq.files.data;

import static org.hamcrest.core.AnyOf.anyOf;
import static org.hamcrest.core.Is.is;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertThat;
import static org.junit.Assert.assertTrue;

import java.util.Map;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import com.sun.xml.internal.ws.policy.AssertionSet;
import com.w3p.im.iib.mon.mq.files.data.ConnectionData;
import com.w3p.im.iib.mon.mq.files.data.ConnectionDataIn;

public class TestConnectionData {

	@Before
	public void setUp() throws Exception {
	}

	@After
	public void tearDown() throws Exception {
	}

	@Test
	public void testCanCreateConnectionData() {
		ConnectionData connData = createConnectionData();
		
		assertNotNull(connData);
		assertEquals("localhost", connData.getHost());
		assertEquals(3414, connData.getPort());
		assertEquals("DEV.APP.SVRCONN", connData.getServerChan());
		assertEquals("IB10QMGR", connData.getQmgr());
		assertEquals("NJAMS_FILES", connData.getQueue());
		assertEquals("njams", connData.getUserid());
		assertEquals("njams", connData.getPassword());
	}
	
	@Test
	public void testCanValidateConnectionIsComplete() {
		ConnectionDataIn connData = createConnectionData();		
		assertFalse(connData.isConnectionDataComplete().isPresent());
		
		connData = createConnectionData();
		connData.setQmgr(null);
		connData.setPort(0);
		assertTrue(connData.isConnectionDataComplete().isPresent());
		Map<String, String> errorMsgs = connData.isConnectionDataComplete().get();
		assertEquals(2, errorMsgs.size());
		errorMsgs.forEach((k, v) -> {
			assertThat(k, anyOf(
					is("iqm"),
					is("ip")));
			assertThat(v, anyOf(
					is("is missing - it must be specified"),
					is("must be specified and be greater than zero")));
		} );
	}		
	

	private ConnectionDataIn createConnectionData() {
		ConnectionDataIn conn = new ConnectionDataIn();
		conn.setHost("localhost");
		conn.setPort(3414);
		conn.setServerChan("DEV.APP.SVRCONN");
		conn.setQmgr("IB10QMGR");
		conn.setQueue("NJAMS_FILES");
		conn.setUserid("njams");
		conn.setPassword("njams");
		return conn;
	}

}
