package com.w3p.im.iib.mon.mq.files.data;

import static org.junit.Assert.*;

import java.util.Properties;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import com.ibm.msg.client.commonservices.propertystore.PropertiesException;
import com.w3p.im.iib.mon.mq.files.data.MQFilesProperties;

public class TestMQFilesProperties {

	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
	}

	@AfterClass
	public static void tearDownAfterClass() throws Exception {
	}

	@Before
	public void setUp() throws Exception {
	}

	@After
	public void tearDown() throws Exception {
	}

	@Test
	public void testCanCreateMQFilesProperties() {
		MQFilesProperties mfp = new MQFilesProperties();
		assertNotNull(mfp);
	}
	
	@Test
	public void testCanGetProperties() throws PropertiesException {
		MQFilesProperties mfp = new MQFilesProperties(); 
		Properties testProps = mfp.getProperties("/mqFilesTest.properties");
		assertNotNull(testProps);
	}

	@Test
	public void testCanGetPropertiesNotTrimmed() throws PropertiesException {
		MQFilesProperties mfp = new MQFilesProperties() {
			@Override
			protected void trimProperties(Properties props) {
				// Do not trim - do nothing
			}
		};
		Properties testProps = mfp.getProperties("/mqFilesTest.properties");
		assertNotNull(testProps);
		assertEquals("first", testProps.get("first"));
		assertEquals("second", testProps.get("second"));
		assertEquals("third", testProps.get("third"));
		// Now check for un-trimmed values
		assertEquals("twoSpacesAfter  ", testProps.get("twoSpacesAfter"));
	}

	@Test
	public void testTrimProperties() throws Exception {
		MQFilesProperties mfp = new MQFilesProperties();
		Properties testProps = mfp.getProperties("/mqFilesTest.properties");
		// Now check for trimmed values
		assertEquals("twoSpacesAfter", testProps.get("twoSpacesAfter"));		
	}

}
