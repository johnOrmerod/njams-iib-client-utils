package com.w3p.im.iib.mon.mq.files.data;

import static com.w3p.im.iib.mon.mq.files.constants.IConstants.*;
import static org.junit.Assert.*;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import com.w3p.im.iib.mon.mq.files.data.ConnectionData;
import com.w3p.im.iib.mon.mq.files.data.Request;
import com.w3p.im.iib.mon.mq.files.data.RequestDataIn;
import com.w3p.im.iib.mon.mq.files.data.RequestDataOut;

public class TestRequest {
	
	private RequestTestsHelper requestTestsHelper = new RequestTestsHelper();

	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
	}

	@AfterClass
	public static void tearDownAfterClass() throws Exception {
	}

	@Before
	public void setUp() throws Exception {
	}

	@After
	public void tearDown() throws Exception {
	}

	@Test
	public void testCanCreateRequestFilesToQueue() {
		Request req = requestTestsHelper.createRequestFilesToQueue(COPY);
		assertEquals(COPY, req.getAction());
		assertTrue(req.getInData().getClass().isAssignableFrom(RequestDataIn.class));
		assertTrue(req.getOutData().getClass().isAssignableFrom(RequestDataOut.class));
		
		assertFalse(req.getInData().getFileNamePattern().isPresent());
		assertEquals("C:\\inFiles", req.getInData().getFolder());		
		assertEquals(4096, req.getInData().getReadBufSize());
		assertNull(req.getInData().getConnData().getQmgr());
		
		assertEquals("Spurs", req.getOutData().getMsgProperties().get("team"));
		assertEquals(1, req.getOutData().getTimeToWaitSec());
		ConnectionData cd = req.getOutData().getConnData(); 
		assertNotNull(cd);
		assertEquals("localhost", cd.getHost());
		assertEquals(3414, cd.getPort());
		assertEquals("IB10QMGR", cd.getQmgr());
		assertEquals("NJAMS.UTILS.TEST.OUT", cd.getQueue());
		assertEquals("DEV.APP.SVRCONN", cd.getServerChan());
		assertEquals(null, cd.getPassword());
		assertEquals(null, cd.getUserid());
	}
	
	@Test
	public void testCanCreateRequestQueueToFiles() {
		Request req = requestTestsHelper.createRequestQueueToFiles(COPY);
		assertEquals(COPY, req.getAction());
		assertTrue(req.getInData().getClass().isAssignableFrom(RequestDataIn.class));
		assertTrue(req.getOutData().getClass().isAssignableFrom(RequestDataOut.class));
		
		assertEquals("team = 'Spurs'", req.getInData().getMsgSelector().get());
		ConnectionData cd = req.getInData().getConnData(); 
		assertNotNull(cd);
		assertEquals("localhost", cd.getHost());
		assertEquals(3414, cd.getPort());
		assertEquals("QNJAMSQMGR", cd.getQmgr());
		assertEquals("NJAMS.UTILS.TEST.IN", cd.getQueue());
		assertEquals("DEV.APP.SVRCONN", cd.getServerChan());
		assertEquals(null, cd.getPassword());
		assertEquals(null, cd.getUserid());
		
		assertEquals("xxx%s.xml", req.getOutData().getFileNamePattern());
		assertEquals("C:\\outFiles", req.getOutData().getFolder());		
	}
	
	@Test
	public void testCanCreateRequestQueueToQueue() {
		Request req = requestTestsHelper.createRequestQueueToQueue(MOVE);
		assertEquals(MOVE, req.getAction());
		assertTrue(req.getInData().getClass().isAssignableFrom(RequestDataIn.class));
		assertTrue(req.getOutData().getClass().isAssignableFrom(RequestDataOut.class));
		
		assertEquals("team = 'Spurs'", req.getInData().getMsgSelector().get());
		ConnectionData cdi = req.getInData().getConnData(); 
		assertNotNull(cdi);
		assertEquals("localhost", cdi.getHost());
		assertEquals(3414, cdi.getPort());
		assertEquals("QNJAMSQMGR", cdi.getQmgr());
		assertEquals("NJAMS.UTILS.TEST.IN", cdi.getQueue());
		assertEquals("DEV.APP.SVRCONN", cdi.getServerChan());
		assertEquals(null, cdi.getPassword());
		assertEquals(null, cdi.getUserid());
		
		assertEquals("Spurs", req.getOutData().getMsgProperties().get("team"));
		assertEquals(1, req.getOutData().getTimeToWaitSec());
		ConnectionData cdo = req.getOutData().getConnData(); 
		assertNotNull(cdo);
		assertEquals("localhost", cdo.getHost());
		assertEquals(3414, cdo.getPort());
		assertEquals("IB10QMGR", cdo.getQmgr());
		assertEquals("NJAMS.UTILS.TEST.OUT", cdo.getQueue());
		assertEquals("DEV.APP.SVRCONN", cdo.getServerChan());
		assertEquals(null, cdo.getPassword());
		assertEquals(null, cdo.getUserid());		
	}

}
