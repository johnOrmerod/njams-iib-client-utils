package com.w3p.im.iib.mon.mq.files.data;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import com.w3p.im.iib.mon.mq.files.data.ConnectionData;
import com.w3p.im.iib.mon.mq.files.data.RequestDataIn;

public class TestRequestDataIn {
	
	RequestTestsHelper helper = new RequestTestsHelper();

	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
	}

	@AfterClass
	public static void tearDownAfterClass() throws Exception {
	}

	@Before
	public void setUp() throws Exception {
	}

	@After
	public void tearDown() throws Exception {
	}

	@Test
	public void testCanCreateRequestDataIn() {
		RequestDataIn inData = new RequestDataIn();
		assertNotNull(inData);
	}
	
	@Test
	public void testCanCreateValidRequestDataInForFiles() {
		RequestDataIn inData = helper.createRequestDataFilesIn();
		assertFalse(inData.isRequestDataCompleteFilesIn().isPresent());
		
		assertNotNull(inData.getConnData());
		assertNull(inData.getConnData().getQmgr());
		
		assertFalse(inData.getFileNamePattern().isPresent());
		assertEquals("C:\\inFiles", inData.getFolder());
		assertEquals(4096, inData.getReadBufSize());			
	}
	
	@Test
	public void testCanCreateValidRequestDataInForFilesWithFileNamePatternSelection() {
		RequestDataIn inData = helper.createRequestDataFilesIn();
		inData.setFileNamePattern("name*.xml");
		assertFalse(inData.isRequestDataCompleteFilesIn().isPresent());
		
		assertNotNull(inData.getConnData());
		assertNull(inData.getConnData().getQmgr());
		
		assertTrue(inData.getFileNamePattern().isPresent());
		assertEquals("name*.xml", inData.getFileNamePattern().get());
		assertEquals("C:\\inFiles", inData.getFolder());
		assertEquals(4096, inData.getReadBufSize());			
	}
	
	@Test
	public void testCanCreateValidRequestDataInForFilesWithDefaults() {
		RequestDataIn inData = new RequestDataIn();		
		inData.setFileNamePattern("xxx*.xml");
		inData.setFolder("C:\\inFiles");		
		// inData.setReadBufSize(4096);
		assertFalse(inData.isRequestDataCompleteFilesIn().isPresent());
	
	}
	
	@Test
	public void testCanCreateValidRequestDataInForFilesWithOptionalNotPresent() {
		RequestDataIn inData = helper.createRequestDataFilesIn();;	
		inData.setFileNamePattern("");
		assertFalse(inData.isRequestDataCompleteFilesIn().isPresent());
	
	}

	@Test
	public void testCanDetectIncompleteRequestDataInFiles() {
		RequestDataIn inData = helper.createRequestDataFilesIn();
		
		inData.setFolder(null);
		assertTrue(inData.isRequestDataCompleteFilesIn().isPresent());
		assertFalse(inData.isRequestDataCompleteFilesIn().get().isEmpty());
	}
	
	@Test
	public void testCanCreateValidRequestDataInForQueue() {
		RequestDataIn inData = helper.createRequestDataQueueIn();
		assertFalse(inData.isRequestDataCompleteQueueIn().isPresent());
		
		assertTrue(inData.getMsgSelector().isPresent());
		assertEquals("team = 'Spurs'", inData.getMsgSelector().get());
		
		ConnectionData cd = inData.getConnData();
		assertNotNull(cd);
		assertEquals("localhost", cd.getHost());
		assertEquals(null, cd.getPassword());
		assertEquals(3414, cd.getPort());
		assertEquals("DEV.APP.SVRCONN", cd.getServerChan());
		assertEquals("QNJAMSQMGR", cd.getQmgr());
		assertEquals("NJAMS.UTILS.TEST.IN", cd.getQueue());
		assertEquals(null, cd.getPassword());
	}
	
	@Test
	public void testCanCreateValidRequestDataInForQueueWithOptionalNotPresent() {
		RequestDataIn inData = helper.createRequestDataQueueIn();
		inData.setMsgSelector(null);
		assertFalse(inData.isRequestDataCompleteQueueIn().isPresent());
	}
	
	
	@Test
	public void testCanDetectIncompleteRequestDataInQueue() {
		RequestDataIn inData = helper.createRequestDataQueueIn();
		
		inData.getConnData().setQueue(null);
		assertTrue(inData.isRequestDataCompleteQueueIn().isPresent());
		assertFalse(inData.isRequestDataCompleteQueueIn().get().isEmpty());
	}
	
	
	
	/*		
	@Test
	public void testCanCreateRequestDataQueueToFiles() {
		IRequestDataQueueToFiles rdqtf = createRequestDataQueueToFiles();
		assertTrue(rdqtf.isRequestDataCompleteQueueToFiles());
		
		assertNotNull(rdqtf.getConnData());
		ConnectionData cd = rdqtf.getConnData();
		
		assertEquals("xxx*.xml", rdqtf.getFileNamePattern());
		assertEquals("C:\\outFiles", rdqtf.getFolder());
		assertEquals("team = 'Spurs'", rdqtf.getMsgSelector().get());	
		
		assertEquals("localhost", cd.getHost());
		assertEquals(null, cd.getPassword());
		assertEquals(3414, cd.getPort());
		assertEquals("DEV.APP.SVRCONN", cd.getServerChan());
		assertEquals("IB10QMGR", cd.getQmgr());
		assertEquals("NJAMS_FILES", cd.getQueue());
		assertEquals(null, cd.getPassword());					
	}

	@Test
	public void testCanDetectIncompleteRequestDataQueueToFiles() {
		IRequestDataQueueToFiles rdqtf = createRequestDataQueueToFiles();
		
		rdqtf.setFolder(null);
		assertFalse(rdqtf.isRequestDataCompleteQueueToFiles());
		
		rdqtf = createRequestDataQueueToFiles();
		rdqtf.setFileNamePattern(null);
		assertFalse(rdqtf.isRequestDataCompleteQueueToFiles());
		
		// Verify MsgSelector can be null
		rdqtf = createRequestDataQueueToFiles();
		rdqtf.setMsgSelector(null);
		assertFalse(rdqtf.getMsgSelector().isPresent());
		assertTrue(rdqtf.isRequestDataCompleteQueueToFiles());
		
		// Now set QMgr null in Conn Data
		rdqtf = createRequestDataQueueToFiles();
		rdqtf.setQMgr(null);
		assertFalse(rdqtf.isRequestDataCompleteQueueToFiles());
	}
	
	@Test
	public void testCanCreateRequestDataQueueToQueue() {
		IRequestDataQueueToQueue rdqtq = createRequestDataQueueToQueue();
		assertTrue(rdqtq.isRequestDataCompleteQueueToQueue());
		
		assertEquals("team = 'Spurs'", rdqtq.getMsgSelector().get());
		assertTrue(rdqtq.isGenCorrelId());
		assertEquals(1, rdqtq.getTimeToWaitSec());
		
		ConnectionData cdIn = rdqtq.getConnDataIn();
		assertEquals("localhost", cdIn.getHost());
		assertEquals(null, cdIn.getPassword());
		assertEquals(3414, cdIn.getPort());
		assertEquals("DEV.APP.SVRCONN", cdIn.getServerChan());
		assertEquals("IB10QMGR", cdIn.getQmgr());
		assertEquals("NJAMS_FILES", cdIn.getQueue());
		assertEquals(null, cdIn.getPassword());		
		
		ConnectionData cdOut = rdqtq.getConnDataOut();
		assertEquals("localhost", cdOut.getHost());
		assertEquals(null, cdOut.getPassword());
		assertEquals(3414, cdOut.getPort());
		assertEquals("DEV.APP.SVRCONN", cdOut.getServerChan());
		assertEquals("OUTQMGR", cdOut.getQmgr());
		assertEquals("OUT_Q", cdOut.getQueue());
		assertEquals(null, cdOut.getPassword());		
		
	}
	
	@Test
	public void testCanDetectIncompleteRequestDataQueueToQueue() {
		IRequestDataQueueToQueue rdqtq = createRequestDataQueueToQueue();		
		rdqtq.getConnDataIn().setQmgr(null);
		assertFalse(rdqtq.isRequestDataCompleteQueueToQueue());
		
		rdqtq = createRequestDataQueueToQueue();		
		rdqtq.getConnDataOut().setQmgr(null);
		assertFalse(rdqtq.isRequestDataCompleteQueueToQueue());
	}

*/
}
