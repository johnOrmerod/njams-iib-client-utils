package com.w3p.im.iib.mon.mq.files.data;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;

import java.util.Map;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import com.w3p.im.iib.mon.mq.files.data.ConnectionData;
import com.w3p.im.iib.mon.mq.files.data.RequestDataOut;

public class TestRequestDataOut {
	
	RequestTestsHelper helper = new RequestTestsHelper();

	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
	}

	@AfterClass
	public static void tearDownAfterClass() throws Exception {
	}

	@Before
	public void setUp() throws Exception {
	}

	@After
	public void tearDown() throws Exception {
	}

	@Test
	public void testCanCreateRequestDataOut() {
		RequestDataOut outData = new RequestDataOut();
		assertNotNull(outData);
	}
	
	@Test
	public void testCanCreateValidRequestDataOutForFiles() {
		RequestDataOut outData = helper.createRequestDataFilesOut();
		assertFalse(outData.isRequestDataCompleteFilesOut().isPresent());
		
		assertNotNull(outData.getConnData());
		assertNull(outData.getConnData().getQmgr());
		
		assertEquals("xxx%s.xml", outData.getFileNamePattern());
		assertEquals("C:\\outFiles", outData.getFolder());
	}
	
	@Test
	public void testCanCreateValidRequestDataOutForFilesWithOptionalNotPresent() {
		RequestDataOut outData = helper.createRequestDataFilesOut();;	
		outData.setFileNamePattern(null);
		assertTrue(outData.isRequestDataCompleteFilesOut().isPresent());
		Map<String, String> errorMsgs = outData.isRequestDataCompleteFilesOut().get();
		assertEquals("is missing - it must be specified", errorMsgs.get("ofnp"));		
	}

	@Test
	public void testCanDetectIncompleteRequestDataOutFiles() {
		RequestDataOut outData = helper.createRequestDataFilesOut();
		
		outData.setFolder(null);
		assertTrue(outData.isRequestDataCompleteFilesOut().isPresent());
		Map<String, String> errorMsgs = outData.isRequestDataCompleteFilesOut().get();
		assertEquals("is missing - it must be specified", errorMsgs.get("of"));	
	}
	
	@Test
	public void testCanCreateValidRequestDataOutForQueue() {
		RequestDataOut outData = helper.createRequestDataQueueOut();
		assertFalse(outData.isRequestDataCompleteQueueOut().isPresent());
		
		assertEquals("Spurs", outData.getMsgProperties().get("team"));
		
		ConnectionData cd = outData.getConnData();
		assertNotNull(cd);
		assertEquals("localhost", cd.getHost());
		assertEquals(null, cd.getPassword());
		assertEquals(3414, cd.getPort());
		assertEquals("DEV.APP.SVRCONN", cd.getServerChan());
		assertEquals("IB10QMGR", cd.getQmgr());
		assertEquals("NJAMS.UTILS.TEST.OUT", cd.getQueue());
		assertEquals(null, cd.getPassword());
	}
	
	
	@Test
	public void testCanDetectIncompleteRequestDataOutQueue() {
		RequestDataOut outData = helper.createRequestDataQueueOut();
		
		outData.getConnData().setQueue(null);
		assertTrue(outData.isRequestDataCompleteQueueOut().isPresent());
	}
}
