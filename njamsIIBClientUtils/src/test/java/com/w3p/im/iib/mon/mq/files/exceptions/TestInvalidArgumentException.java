package com.w3p.im.iib.mon.mq.files.exceptions;

import static org.junit.Assert.*;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import com.w3p.im.iib.mon.mq.files.exceptions.InvalidArgumentException;

public class TestInvalidArgumentException {

	@Before
	public void setUp() throws Exception {
	}

	@After
	public void tearDown() throws Exception {
	}

	@Test
	public void testCanCreateInvalidArgumentException() {
		InvalidArgumentException iae = new InvalidArgumentException();
		assertNotNull(iae);
		assertNull(iae.getMessage());
	}

	@Test
	public void testCanCreateInvalidArgumentExceptionStringThrowable() {
		InvalidArgumentException iae = new InvalidArgumentException("argx", new Exception("Test failed"));
		assertNotNull(iae);
		assertEquals("Invalid parameter 'argx' - missing a '-'", iae.getMessage());
		assertEquals("Test failed", iae.getCause().getMessage());
	}

	@Test
	public void testCanCreateInvalidArgumentExceptionString() {
		InvalidArgumentException iae = new InvalidArgumentException("argx");
		assertNotNull(iae);
		assertEquals("Invalid parameter 'argx' - missing a '-'", iae.getMessage());
	}

}
