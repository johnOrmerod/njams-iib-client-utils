package com.w3p.im.iib.mon.mq.files.exceptions;

import static org.junit.Assert.*;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import com.w3p.im.iib.mon.mq.files.exceptions.MessagingException;

public class TestMessagingException {

	@Before
	public void setUp() throws Exception {
	}

	@After
	public void tearDown() throws Exception {
	}

	@Test
	public void testCanCreateMessagingException() {
		MessagingException me = new MessagingException();
		assertNotNull(me);
		assertNull(me.getMessage());
	}

	@Test
	public void testCanCreateMessagingExceptionStringThrowable() {
		MessagingException me = new MessagingException("error message", new Exception("MQ error"));
		assertNotNull(me);		
		assertEquals("An MQ/JMS error occured. Message is 'error message'.\n  Cause is 'MQ error'.", me.getMessage());
		assertEquals("MQ error", me.getCause().getMessage());
	}

	@Test
	public void testCanCreateMessagingExceptionString() {
		MessagingException me = new MessagingException("error message");
		assertNotNull(me);
		assertEquals("An MQ/JMS error occured. Message is 'error message'.", me.getMessage());
	}

}
