package com.w3p.im.iib.mon.mq.files.exceptions;

import static org.junit.Assert.*;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import com.w3p.im.iib.mon.mq.files.exceptions.PropertiesException;

public class TestPropertiesException {

	@Before
	public void setUp() throws Exception {
	}

	@After
	public void tearDown() throws Exception {
	}

	@Test
	public void testCanCreatePropertiesException() {
		PropertiesException pe = new PropertiesException();
		assertNotNull(pe);
		assertNull(pe.getMessage());
	}

	@Test
	public void testCanCreatePropertiesExceptionStringThrowable() {
		String reason = "An error occurred when reading the properties file '/resources/mqLoader.properties'. see log.";
		PropertiesException pe = new PropertiesException(reason, new Exception("IO error"));
		assertNotNull(pe);		
		assertEquals("An error occurred when reading the properties file '/resources/mqLoader.properties'. see log.", pe.getMessage());
		assertEquals("IO error", pe.getCause().getMessage());
	}

	@Test
	public void testCanCreatePropertiesExceptionString() {
		PropertiesException pe = new PropertiesException("An IO error occured. Mesage is 'error message'.");
		assertNotNull(pe);
		assertEquals("An IO error occured. Mesage is 'error message'.", pe.getMessage());
	}

}
