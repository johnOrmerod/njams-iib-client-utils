package com.w3p.im.iib.mon.mq.files.exceptions;

import static org.hamcrest.core.AnyOf.anyOf;
import static org.hamcrest.core.Is.is;
import static org.junit.Assert.*;

import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.junit.After;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;

import com.w3p.im.iib.mon.mq.files.exceptions.ValidationException;

public class TestValidationException {

	@Before
	public void setUp() throws Exception {
	}

	@After
	public void tearDown() throws Exception {
	}

	@Test
	public void testCanCreateValidationException() {
		ValidationException ve = new ValidationException();
		assertNotNull(ve);
		assertNull(ve.getMessage());
	}

	@Test
	public void testCanCreateValidationExceptionWithThrowable() {
		String msg = "Non-numeric value encountered, please correct and retry. Reason 'Input string: \"I414\"'";
		ValidationException ve = new ValidationException(msg, new NumberFormatException("'xyx' is not numeric."));
		assertNotNull(ve);		
		assertEquals(msg, ve.getMessage());
		assertTrue(ve.getCause() instanceof NumberFormatException);
		assertEquals("'xyx' is not numeric.", ve.getCause().getMessage());
	}

	@Test
	public void testCanCreateValidationExceptionMessageOnly() {
		ValidationException ve = new ValidationException("Parameter 'xyz' is missing, run terminated.");
		assertNotNull(ve);
		assertEquals("Parameter 'xyz' is missing, run terminated.", ve.getMessage());
	}

	@Test
	public void testCanCreateValidationExceptionWithErrorMessages() {
		Map<String, String> errorMsgs = new HashMap<>();
		errorMsgs.put("nameA", "Parameter 'xyz' is missing.");
		errorMsgs.put("nameB", "Parameter 'abc'  has invalid value.");
		ValidationException ve = new ValidationException("Errors found - run terminated.", errorMsgs);
		assertNotNull(ve);
		assertEquals("Errors found - run terminated.", ve.getMessage());
		assertEquals(2, ve.getErrorMessages().size());		
		errorMsgs.forEach((k, v) -> {
			assertThat(v, anyOf(
				is("Parameter 'xyz' is missing."),
				is("Parameter 'abc'  has invalid value.")));
		});
	}

	@Test
	public void testCanCreateValidationExceptionWithErrorMessagesAndThrowable() {
		Map<String, String> errorMsgs = new HashMap<>();
		errorMsgs.put("nameA", "Parameter 'xyz' is missing.");
		errorMsgs.put("nameB", "Parameter 'abc'  has invalid value.");
		ValidationException ve = new ValidationException("Errors found - run terminated.", errorMsgs, new NumberFormatException("'xyx' is not numeric."));

		assertNotNull(ve);
		assertEquals("Errors found - run terminated.", ve.getMessage());
		assertEquals(2, ve.getErrorMessages().size());		
		assertTrue(ve.getCause() instanceof NumberFormatException);
		assertEquals("'xyx' is not numeric.", ve.getCause().getMessage());
		errorMsgs.forEach((k, v) -> {
			assertThat(v, anyOf(
				is("Parameter 'xyz' is missing."),
				is("Parameter 'abc'  has invalid value.")));
		});
	}
	
}
