package com.w3p.im.iib.mon.mq.files.folder;

import java.io.File;
import java.io.IOException;
import java.nio.file.DirectoryStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;

import org.junit.After;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.ClassRule;
import org.junit.rules.TemporaryFolder;;

public class TestCopyFileToMQ {
	
	private static final Path inputFolderPath = Paths.get("src","test","resources", "inputFolder"); 
	
	@ClassRule
	public static TemporaryFolder tempInputFolder = new TemporaryFolder();
	@ClassRule
	public static TemporaryFolder emptyInputFolder = new TemporaryFolder();
	@ClassRule
	public static TemporaryFolder fileInputFolder = new TemporaryFolder();

	@BeforeClass
	public static void copyTestFiles() throws IOException {
		// Copy test files to a JUnit temp folder		
		DirectoryStream<Path> filesList = Files.newDirectoryStream(inputFolderPath); // List the source files
		for (Path inFilePath : filesList) {
			String fileName = inFilePath.toFile().getName();
			File tempFile = tempInputFolder.newFile(fileName);
			Path tempFilePath = Paths.get(tempFile.getAbsolutePath());
			Files.copy(inFilePath, tempFilePath, StandardCopyOption.REPLACE_EXISTING, StandardCopyOption.COPY_ATTRIBUTES);
		}
		
		// Create a file in fileInputFolder for testing 'folder not folder'
		fileInputFolder.newFile("InputFolder");
	}
	
	@Before
	public void setUp() throws Exception {
	}

	@After
	public void tearDown() throws Exception {
	}

/*	

	
	@Test
	public void testCanCreateMQQueueMgr() throws MQException {
		String inputFolder = tempInputFolder.getRoot().getAbsolutePath();
		Request req = createRequest(inputFolder, false); // Don't delete input files.		
		CopyFilesToMQ cftmq = new CopyFilesToMQ();
		assertNotNull(cftmq);
		ConnectionData conn = cftmq.createConnectionData(req);
		assertNotNull(conn);
		MQQueueManager qMgr = cftmq.connectToMQ(conn); // Creates the qMgr
		assertNotNull(qMgr);
		assertEquals("IB10QMGR", qMgr.getName());		
		qMgr.disconnect();
	}
	
	@Test
	public void testCanHandleCreateMQQueueMgrError() {
		String inputFolder = tempInputFolder.getRoot().getAbsolutePath();
		Request req = createRequest(inputFolder, false);
		req.setqMgr("INVALID");
		CopyFilesToMQ cftmq = new CopyFilesToMQ();
		assertNotNull(cftmq);
		ConnectionData conn = cftmq.createConnectionData(req);
		assertNotNull(conn);
		
		MQQueueManager qMgr = null;
		try {
			qMgr = cftmq.connectToMQ(conn); // Should fail to create the qMgr
			fail("Creating Queue Mgr with invalid name should have failed... it didn't");
		} catch (Exception e) {
			assertNull(qMgr);
			assertTrue(e instanceof MessagingException);
			String reason = e.getMessage();
			assertTrue(reason.contains("MQJE001: Completion Code '2', Reason '2058'"));
		}
	}
	
	@Test
	public void testCanCreateMQQueue() throws MQException, IOException {
		String inputFolder = tempInputFolder.getRoot().getAbsolutePath();
		Request req = createRequest(inputFolder, false);		
		CopyFilesToMQ cftmq = new CopyFilesToMQ();
		assertNotNull(cftmq);
		ConnectionData conn = cftmq.createConnectionData(req);
		assertNotNull(conn);
		MQQueueManager qMgr = cftmq.connectToMQ(conn); // Creates the qMgr
		assertNotNull(qMgr);
		assertEquals("IB10QMGR", qMgr.getName());
		MQQueue queue = cftmq.openQueue(qMgr, conn); // Should fail to create the queue
		assertNotNull(queue);
		assertEquals("NJAMS.UTILS.TEST.OUT", queue.getName().trim());
		queue.close();
		qMgr.disconnect();
	}
	
	
	@Test
	public void testCansSetPropertiesForMQMessage() throws MQException {
		CopyFilesToMQ cftmq = new CopyFilesToMQ();
		assertNotNull(cftmq);
		// Define Map of MQ message properties
		Map<String, String> msgProperties = new HashMap<>();
		msgProperties.put("Game", "Football");
		msgProperties.put("Team", "Spurs");
		MQMessage fileMsg = new MQMessage();		
		cftmq.setMsgProperties(fileMsg, msgProperties);
		
		assertEquals("Football", fileMsg.getStringProperty("Game"));
		assertEquals("Spurs", fileMsg.getStringProperty("Team"));
	}
	
	
	@Test
	public void testCanHandleCreateMQQueueError() throws MQException, IOException {
		String inputFolder = tempInputFolder.getRoot().getAbsolutePath();
		Request req = createRequest(inputFolder, false);
		req.setOututQueue("INVALID_QUEUE");
		CopyFilesToMQ cftmq = new CopyFilesToMQ();
		assertNotNull(cftmq);
		ConnectionData conn = cftmq.createConnectionData(req);
		assertNotNull(conn);
		MQQueueManager qMgr = cftmq.connectToMQ(conn); // Creates the qMgr
		assertNotNull(qMgr);
		assertEquals("IB10QMGR", qMgr.getName());
		MQQueue queue = null;
		try {
			queue = cftmq.openQueue(qMgr, conn); // Should fail to create the queue
			fail("Creating queue with invalid name should have failed... it didn't");		
		} catch (Exception e) {
			assertNull(queue);
			assertTrue(e instanceof MessagingException);
			String reason = e.getMessage();
			assertTrue(reason.contains("MQJE001: Completion Code '2', Reason '2085'"));
		} finally {
			qMgr.disconnect();
		}
	}
*/
	
/*
	@Ingore("reason")
	@Test
	public void testCanMockSomething() throws MQException, IOException {
		String inputFolder = tempInputFolder.getRoot().getAbsolutePath();
		Request req = createRequest(inputFolder, false); // Don't delete input files.		
		CopyFilesToMQ cftmq = new CopyFilesToMQ(); //mock(CopyFilesToMQ.class);
		assertNotNull(cftmq);
		ConnectionData conn = cftmq.createConnectionData(req);
		assertNotNull(conn);
		
		MQQueueManager mqQmgr = mock(MQQueueManager.class);		
		when(mqQmgr.getName()).thenReturn("IB10QMGR");
		MQQueue mqQueue = mock(MQQueue.class);
		when(mqQueue.getName()).thenReturn("NJAMS.UTILS.TEST.OUT    ");
		when(cftmq.openQueue(qMgr, conn))).thenReturn(mqQueue);
		
		
		cftmq.connectToMQ();
		assertNotNull(cftmq);
		MQQueueManager qmgr = cftmq.getqManager();
		assertNotNull(qmgr);
		assertEquals("IB10QMGR", qmgr.getName());
		
		MQQueue queue = cftmq.openQueue(req.getOututQueue());
		assertNotNull(queue);
		assertEquals("NJAMS.UTILS.TEST.OUT", queue.getName().trim());
		queue.close();
		qmgr.disconnect();

	}
*/
}
