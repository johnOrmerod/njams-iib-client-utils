package com.w3p.im.iib.mon.mq.files.jms;

import static org.mockito.Matchers.anyString;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import javax.jms.JMSContext;
import javax.jms.JMSException;
import javax.jms.JMSProducer;
import javax.jms.Queue;

import com.ibm.msg.client.jms.JmsConnectionFactory;
import com.ibm.msg.client.jms.JmsFactoryFactory;
import com.ibm.msg.client.wmq.WMQConstants;
import com.w3p.im.iib.mon.mq.files.jms.JmsFactoryFactoryWrapper;

public class CreateMocksForJMS {

	public CreateMocksForJMS() {
		// TODO Auto-generated constructor stub
	}
	
	public MockedJmsObjects createForJMSConnection() throws JMSException {
		MockedJmsObjects mjo = new MockedJmsObjects();
		JmsConnectionFactory connFac = mock(JmsConnectionFactory.class);				
		JmsFactoryFactory ff = mock(JmsFactoryFactory.class);
		when(ff.createConnectionFactory()).thenReturn(connFac);

		JmsFactoryFactoryWrapper jffw = mock(JmsFactoryFactoryWrapper.class);
		when(jffw.getFactory(WMQConstants.WMQ_PROVIDER)).thenReturn(ff);

		JMSContext context = mock(JMSContext.class);
		when(connFac.createContext(JMSContext.AUTO_ACKNOWLEDGE)).thenReturn(context);

		Queue jmsQueue = mock(Queue.class);					
		when(context.createQueue(anyString())).thenReturn(jmsQueue);
		
		JMSProducer producer = mock(JMSProducer.class);
		when(context.createProducer()).thenReturn(producer);
		
		mjo.setJmsConnectionFactory(connFac);
		mjo.setJmsContext(context);
		mjo.setJmsFactoryFactory(ff);
		mjo.setJmsFactoryFactoryWrapper(jffw);
		mjo.setJmsProducer(producer);
		mjo.setJmsQueue(jmsQueue);
		
		return mjo;
	}


}
