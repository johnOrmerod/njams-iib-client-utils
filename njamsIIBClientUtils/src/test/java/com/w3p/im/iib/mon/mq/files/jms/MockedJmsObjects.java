package com.w3p.im.iib.mon.mq.files.jms;

import javax.jms.JMSContext;
import javax.jms.JMSProducer;
import javax.jms.Queue;

import com.ibm.msg.client.jms.JmsConnectionFactory;
import com.ibm.msg.client.jms.JmsFactoryFactory;
import com.w3p.im.iib.mon.mq.files.jms.JmsFactoryFactoryWrapper;

public class MockedJmsObjects {
	private JmsConnectionFactory jmsConnectionFactory;
	private JmsFactoryFactory jmsFactoryFactory;
	private JMSContext jmsContext;
	private Queue jmsQueue;
	private JMSProducer jmsProducer;
	private JmsFactoryFactoryWrapper jmsFactoryFactoryWrapper;
	 

	public MockedJmsObjects() {
	}

	public JmsConnectionFactory getJmsConnectionFactory() {
		return jmsConnectionFactory;
	}
	public void setJmsConnectionFactory(JmsConnectionFactory jmsConnectionFactory) {
		this.jmsConnectionFactory = jmsConnectionFactory;
	}
	public JmsFactoryFactory getJmsFactoryFactory() {
		return jmsFactoryFactory;
	}
	public void setJmsFactoryFactory(JmsFactoryFactory jmsFactoryFactory) {
		this.jmsFactoryFactory = jmsFactoryFactory;
	}
	public JMSContext getJmsContext() {
		return jmsContext;
	}
	public void setJmsContext(JMSContext jmsContext) {
		this.jmsContext = jmsContext;
	}
	public Queue getJmsQueue() {
		return jmsQueue;
	}
	public void setJmsQueue(Queue jmsQueue) {
		this.jmsQueue = jmsQueue;
	}
	public JMSProducer getJmsProducer() {
		return jmsProducer;
	}
	public void setJmsProducer(JMSProducer jmsProducer) {
		this.jmsProducer = jmsProducer;
	}
	public JmsFactoryFactoryWrapper getJmsFactoryFactoryWrapper() {
		return jmsFactoryFactoryWrapper;
	}
	public void setJmsFactoryFactoryWrapper(JmsFactoryFactoryWrapper jmsFactoryFactoryWrapper) {
		this.jmsFactoryFactoryWrapper = jmsFactoryFactoryWrapper;
	}
}
