package com.w3p.im.iib.mon.mq.files.processors;

import static com.w3p.im.iib.mon.mq.files.constants.IConstants.ACTION;
import static com.w3p.im.iib.mon.mq.files.constants.IConstants.COPY;
import static com.w3p.im.iib.mon.mq.files.constants.IConstants.DFLT_MQ_FILES_PROPERTIES;
import static com.w3p.im.iib.mon.mq.files.constants.IConstants.GEN_CORREL_ID;
import static com.w3p.im.iib.mon.mq.files.constants.IConstants.I_FILE_NAME_PATTERN;
import static com.w3p.im.iib.mon.mq.files.constants.IConstants.I_FOLDER;
import static com.w3p.im.iib.mon.mq.files.constants.IConstants.I_HOST;
import static com.w3p.im.iib.mon.mq.files.constants.IConstants.I_PORT;
import static com.w3p.im.iib.mon.mq.files.constants.IConstants.I_QUEUE;
import static com.w3p.im.iib.mon.mq.files.constants.IConstants.I_QUEUE_MANAGER;
import static com.w3p.im.iib.mon.mq.files.constants.IConstants.I_SERVER_CHANNEL;
import static com.w3p.im.iib.mon.mq.files.constants.IConstants.MOVE;
import static com.w3p.im.iib.mon.mq.files.constants.IConstants.MSG_SELECTOR;
import static com.w3p.im.iib.mon.mq.files.constants.IConstants.O_FILE_NAME_PATTERN;
import static com.w3p.im.iib.mon.mq.files.constants.IConstants.O_FOLDER;
import static com.w3p.im.iib.mon.mq.files.constants.IConstants.O_HOST;
import static com.w3p.im.iib.mon.mq.files.constants.IConstants.O_PORT;
import static com.w3p.im.iib.mon.mq.files.constants.IConstants.O_QUEUE;
import static com.w3p.im.iib.mon.mq.files.constants.IConstants.O_QUEUE_MANAGER;
import static com.w3p.im.iib.mon.mq.files.constants.IConstants.O_SERVER_CHANNEL;
import static com.w3p.im.iib.mon.mq.files.constants.IConstants.PUT_WAIT_INTERVAL;
import static com.w3p.im.iib.mon.mq.files.constants.IConstants.READBUFFER_SIZE;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;

import java.io.FileNotFoundException;
import java.util.Map;
import java.util.Properties;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import com.w3p.im.iib.mon.mq.files.data.Request;
import com.w3p.im.iib.mon.mq.files.data.RequestDataIn;
import com.w3p.im.iib.mon.mq.files.data.RequestDataOut;
import com.w3p.im.iib.mon.mq.files.processors.CreateRequest;

public class TestCreateRequest {

	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
	}

	@AfterClass
	public static void tearDownAfterClass() throws Exception {
	}

	@Before
	public void setUp() throws Exception {
	}

	@After
	public void tearDown() throws Exception {
	}

	@Test
	public void testCanCreateCreateRequest() {
		CreateRequest cr = new CreateRequest();
		assertNotNull(cr);
	}
	
	@Test
	public void testCanCreateParamsFromArgsAndDefaultPropertiesFilesToQueue() {
		// "-a", "copy", "-oqm", "QM_OUT", "-oq", "Q_OUT", "-oh", "euuktetest01", "-op", "3414", "-osc", "DEV.APP.SVRCONN",
		// "-gcid", "true", "-pwi", "5", "-if", "C:\\filesIn", "-ifnp", "name1_;name2;*;xml", "-readBufSize", "4096"
		String[] args = createAllArgsFilesToQueue();
		Properties props = createAllPropertiesFilesToQueue();
		
		CreateRequest cr = new CreateRequest();
		Map<String, String> params = cr.createParamsFromArgsAndProperties(args, props);
		
		assertNotNull(params);
		assertFalse(params.isEmpty());
		assertEquals("copy", params.get(ACTION));
		// Files in
		assertEquals("C:\\filesIn", params.get(I_FOLDER));
		assertEquals("4096", params.get(READBUFFER_SIZE));
		// Queue out
		assertEquals("euuktetest01", params.get(O_HOST));
		assertEquals("QM_OUT", params.get(O_QUEUE_MANAGER));
		assertEquals("Q_OUT", params.get(O_QUEUE));
		assertEquals("3414", params.get(O_PORT));
		assertEquals("DEV.APP.SVRCONN", params.get(O_SERVER_CHANNEL));		
		assertEquals("5", params.get(PUT_WAIT_INTERVAL));		
		assertTrue(Boolean.parseBoolean(params.get(GEN_CORREL_ID)));		
	}

	@Test
	public void testCanCreateParamsFromArgsAndPropertiesQueueToFiles() {
		// "-im", "QM_IN", "-iq", "Q_IN", "-ih", "euuktetest01", "-ip", "3414", "-isc", "DEV.APP.SVRCONN", "-of", "C:\\outFiles"
		String[] args = createSomeArgsQueueToFiles();
		Properties props = createPropertiesQueueToFiles();
		
		CreateRequest cr = new CreateRequest();
		Map<String, String> params = cr.createParamsFromArgsAndProperties(args, props);
		
		assertNotNull(params);
		assertFalse(params.isEmpty());
		// Files out
		assertEquals("C:\\outFiles", params.get(O_FOLDER));
		// Queue in
		assertEquals("euuktetest01", params.get(I_HOST));
		assertEquals("QM_IN", params.get(I_QUEUE_MANAGER));
		assertEquals("Q_IN", params.get(I_QUEUE));
		assertEquals("3414", params.get(I_PORT));
		assertEquals("DEV.APP.SVRCONN", params.get(I_SERVER_CHANNEL));		
		assertNull(params.get(PUT_WAIT_INTERVAL));		
		assertFalse(Boolean.parseBoolean(params.get(GEN_CORREL_ID)));				
		}
	
	@Test
	public void testCanCreateRequestWithDefaultAction() throws FileNotFoundException {
		// "-oqm", "QM_OUT", "-oq", "Q_OUT", "-oh", "euuktetest01", "-op", "3414", "-osc", "DEV.APP.SVRCONN", 
		//		"-gcid", "true", "-pwi", "5", "-if", "C:\\filesIn", "-ifnp", "name1_;name2;*;xml", "-readBufSize", "4096"
		String[] args = createAllArgsWithoutActionFilesToQueue();
				
		CreateRequest cr = new CreateRequest() {
			@Override
			protected Properties getProperties(String mqFilesProperties) throws FileNotFoundException {
				return createNoProperties();
			}			
		};
		
		Request req = cr.create(args);
		assertNotNull(req);
		assertEquals(COPY, req.getAction());
	}
	
	@Test
	public void testCanCreateRequestWithDefaultPropertiesFile() throws FileNotFoundException {
		// "-oqm", "QM_OUT", "-oq", "Q_OUT", "-oh", "euuktetest01", "-op", "3414", "-osc", "DEV.APP.SVRCONN", 
		//		"-gcid", "true", "-pwi", "5", "-if", "C:\\filesIn", "-ifnp", "name1_;name2;*;xml", "-readBufSize", "4096"
		String[] args = createAllArgsFilesToQueue();
				
		CreateRequest cr = new CreateRequest() {
			@Override
			protected Properties getProperties(String mqFilesProperties) throws FileNotFoundException {
				assertEquals(DFLT_MQ_FILES_PROPERTIES, mqFilesProperties);
				return createAllPropertiesFilesToQueue();
			}			
		};
		
		Request req = cr.create(args);
		assertNotNull(req);
		assertEquals(COPY, req.getAction());
	}
	
	@Test
	public void testCanCreateRequestWithPropertiesFileFromArgs() throws FileNotFoundException {
		// "-pf", "filesToQueue.properties", "-oqm", "QM_OUT", "-oq", "Q_OUT", "-oh", "euuktetest01", "-op", "3414", "-osc", "DEV.APP.SVRCONN", 
		//		"-gcid", "true", "-pwi", "5", "-if", "C:\\filesIn", "-ifnp", "name1_;name2;*;xml", "-readBufSize", "4096"
		String[] args = createAllArgsWithPropertiesFileForFilesToQueue();
				
		CreateRequest cr = new CreateRequest() {
			@Override
			protected Properties getProperties(String mqFilesProperties) throws FileNotFoundException {
				assertEquals("filesToQueue.properties", mqFilesProperties);
				return createAllPropertiesFilesToQueue();
			}			
		};
		
		Request req = cr.create(args);
		assertNotNull(req);
		assertEquals(COPY, req.getAction());
	}
	
	@Test
	public void testCanCreateValidRequestFromArgsOnly() throws FileNotFoundException {
		// "-a", "copy", "-oqm", "QM_OUT", "-oq", "Q_OUT", "-oh", "euuktetest01", "-op", "3414", "-osc", "DEV.APP.SVRCONN", 
		//		"-gcid", "true", "-pwi", "5", "-if", "C:\\filesIn", "-ifnp", "name1_;name2;*;xml", "-readBufSize", "4096"
		String[] args = createAllArgsFilesToQueue();
				
		CreateRequest cr = new CreateRequest() {
			@Override
			protected Properties getProperties(String mqFilesProperties) throws FileNotFoundException {
				return createNoProperties();
			}			
		};
		
		Request req = cr.create(args);
		RequestDataIn inData = req.getInData();
		RequestDataOut outData = req.getOutData();
		
		assertNotNull(req);
		assertEquals(COPY, req.getAction());
		// Files in
		assertEquals("C:\\filesIn", inData.getFolder());
		assertEquals("name1_;name2;*;xml", inData.getFileNamePattern().get());
		assertEquals(4096, inData.getReadBufSize());
		// Queue out
		assertEquals("QM_OUT", outData.getConnData().getQmgr());
		assertEquals("Q_OUT", outData.getConnData().getQueue());
		assertEquals("euuktetest01",  outData.getConnData().getHost());
		assertEquals(3414, outData.getConnData().getPort());
		assertEquals("DEV.APP.SVRCONN", outData.getConnData().getServerChan());		
		assertEquals(5, outData.getTimeToWaitSec());		
		assertTrue(outData.isGenCorrelId());		
	}
	
	@Test
	public void testCanCreateValidRequestFromOnlyProperties() throws FileNotFoundException {
		String[] args = new String[] {"-pf", "filesToQueue.properties"}; // Have to find the properties file.
		CreateRequest cr = new CreateRequest();
		assertNotNull(cr);
		Request req = cr.create(args);
		RequestDataIn inData = req.getInData();
		RequestDataOut outData = req.getOutData();
		
		assertNotNull(req);
		assertEquals(COPY, req.getAction());
		// Files in
		assertEquals("C:\\filesIn", inData.getFolder());
		assertEquals("name1_;name2;*;xml", inData.getFileNamePattern().get());
		assertEquals(4096, inData.getReadBufSize());
		assertNull(inData.getConnData().getQmgr());
		// Queue out
		assertEquals("IB10QMGR", outData.getConnData().getQmgr());
		assertEquals("NJAMS.UTILS.TEST.OUT", outData.getConnData().getQueue());
		assertEquals("localhost", outData.getConnData().getHost());
		assertEquals(3414, outData.getConnData().getPort());
		assertEquals("DEV.APP.SVRCONN", outData.getConnData().getServerChan());		
		assertEquals(1, outData.getTimeToWaitSec());		
		assertTrue(outData.isGenCorrelId());
		
		final boolean[] results = new boolean[] {false, false};		
		outData.getMsgProperties().forEach((k, v) -> {
			if (k.equals("sport")) {
				assertEquals("Football", v);
				results[0] = true;
			} else if (k.equals("team")) {
				assertEquals("Spurs", v);
				results[1] = true;
			}
		});		
		assertTrue("sport = Football, not found", results[0]);
		assertTrue("team = Spurs, not found", results[1]);
	}
	


	@Test
	public void testCanCreateValidRequestByOverridingAllProperties() throws FileNotFoundException {
		// "-a", "copy", "-oqm", "QM_OUT", "-oq", "Q_OUT", "-oh", "euuktetest01", "-op", "3414", "-osc", "DEV.APP.SVRCONN", 
		//		"-gcid", "true", "-pwi", "5", "-if", "C:\\filesIn", "-ifnp", "name1_;name2;8;xml", "-readBufSize", "4096"
		String[] args = createAllArgsFilesToQueue();
				
		CreateRequest cr = new CreateRequest() {
			@Override
			protected Properties getProperties(String mqFilesProperties) throws FileNotFoundException {
				return createAlternativePropertiesFilesToQueue();
			}			
		};
		assertNotNull(cr);
		Request req = cr.create(args);
		RequestDataIn inData = req.getInData();
		RequestDataOut outData = req.getOutData();
		
		assertNotNull(req);
		assertEquals(COPY, req.getAction());
		// Files in
		assertEquals("C:\\filesIn", inData.getFolder());
		assertEquals("name1_;name2;*;xml", inData.getFileNamePattern().get());
		assertEquals(4096, inData.getReadBufSize());
		assertNull(inData.getConnData().getQmgr());
		// Files in
		assertEquals("QM_OUT", outData.getConnData().getQmgr());
		assertEquals("Q_OUT", outData.getConnData().getQueue());
		assertEquals("euuktetest01", outData.getConnData().getHost());
		assertEquals(3414, outData.getConnData().getPort());
		assertEquals("DEV.APP.SVRCONN", outData.getConnData().getServerChan());		
		assertEquals(5, outData.getTimeToWaitSec());		
		assertTrue(outData.isGenCorrelId());
		
		final boolean[] results = new boolean[] {false, false};		
		outData.getMsgProperties().forEach((k, v) -> {
			if (k.equals("sport")) {
				assertEquals("Football", v);
				results[0] = true;
			} else if (k.equals("team")) {
				assertEquals("Spurs", v);
				results[1] = true;
			}
		});		
		assertTrue("sport = Football, not found", results[0]);
		assertTrue("team = Spurs, not found", results[1]);	
	}

	@Test
	public void testCanCreateFilesToQueueRequestFromArgsAndFilesToQueueProperties() throws FileNotFoundException {
		// "-oqm", "QM_OUT", "-oq", "Q_OUT", "-oh", "euuktetest01", "-gcid", "true", "-if", "C:\\filesIn" 
		String[] args = createSomeArgsFilesToQueue();			
		CreateRequest cr = new CreateRequest() {
			@Override
			protected Properties getProperties(String mqFilesProperties) {
				return createAllPropertiesFilesToQueue();
			}
		};
		
		assertNotNull(cr);	
		Request req = cr.create(args);
		
		assertNotNull(req);
		assertEquals("copy", req.getAction());
		RequestDataIn inData = req.getInData();
		RequestDataOut outData = req.getOutData();
		assertNotNull(inData);
		assertNotNull(outData);
		// In
		assertEquals("C:\\filesIn", inData.getFolder());
		assertEquals(4096, inData.getReadBufSize());
		// Out
		assertEquals("euuktetest01", outData.getConnData().getHost());
		assertEquals("QM_OUT", outData.getConnData().getQmgr());
		assertEquals("Q_OUT", outData.getConnData().getQueue());
		assertEquals(3414, outData.getConnData().getPort());
		assertEquals("DEV.APP.SVRCONN", outData.getConnData().getServerChan());			
		assertEquals(5, outData.getTimeToWaitSec());			
		assertTrue(outData.isGenCorrelId());	
	}

	@Test
	public void testCreateRequestDataFromParamsFilesToQueue() {
		// "-a", "copy", "-oqm", "QM_OUT", "-oq", "Q_OUT", "-oh", "euuktetest01", "-op", "3414", "-osc", "DEV.APP.SVRCONN",
		// "-gcid", "true", "-pwi", "5", "-if", "C:\\filesIn", "-ifnp", "name1_;name2;*;xml", "-readBufSize", "4096"
		String[] args = createAllArgsFilesToQueue();
		Properties props = createAllPropertiesFilesToQueue();
		
		CreateRequest cr = new CreateRequest();
		Map<String, String> params = cr.createParamsFromArgsAndProperties(args, props);
		
		RequestDataIn reqdataIn = cr.createRequestDatatInFromParams(params);		
		assertNotNull(reqdataIn);
		assertEquals(4096, reqdataIn.getReadBufSize());
		assertEquals("C:\\filesIn", reqdataIn.getFolder());
		
		assertNull(reqdataIn.getConnData().getHost());
		assertNull(reqdataIn.getConnData().getQmgr());
		assertNull(reqdataIn.getConnData().getQueue());
		assertEquals(0, reqdataIn.getConnData().getPort());
		assertNull(reqdataIn.getConnData().getServerChan());		
		
		RequestDataOut reqDataOut = cr.createRequestDataOutFromParams(params);
		assertNotNull(reqDataOut);
		assertEquals("euuktetest01", reqDataOut.getConnData().getHost());
		assertEquals("QM_OUT", reqDataOut.getConnData().getQmgr());
		assertEquals("Q_OUT", reqDataOut.getConnData().getQueue());
		assertEquals(3414, reqDataOut.getConnData().getPort());
		assertEquals("DEV.APP.SVRCONN", reqDataOut.getConnData().getServerChan());		
		assertEquals(5, reqDataOut.getTimeToWaitSec());
				
		assertNull(reqDataOut.getFolder());
		assertTrue(reqDataOut.isGenCorrelId());
	}
	
	@Test
	public void testCanCreateQueueToFilesRequestFromArgsAndProperties() throws FileNotFoundException {
		// "-iqm", "QM_IN", "-iq", "Q_IN", "-ih", "euuktetest01", "-ip", "3414", "-isc", "DEV.APP.SVRCONN",	"-of", "C:\\outFiles" 
		String[] args = createSomeArgsQueueToFiles();			
		CreateRequest cr = new CreateRequest() {
			@Override
			protected Properties getProperties(String mqFilesProperties) {
				return createAllPropertiesQueueToFiles();
			}
		};
		
		assertNotNull(cr);	
		Request req = cr.create(args);
		
		assertNotNull(req);
		assertEquals("copy", req.getAction());
		RequestDataIn inData = req.getInData();
		RequestDataOut outData = req.getOutData();
		assertNotNull(inData);
		assertNotNull(outData);
		
		// In
		assertEquals("euuktetest01", inData.getConnData().getHost());
		assertEquals("QM_IN", inData.getConnData().getQmgr());
		assertEquals("Q_IN", inData.getConnData().getQueue());
		assertEquals(3414, inData.getConnData().getPort());
		assertEquals("DEV.APP.SVRCONN", inData.getConnData().getServerChan());
		assertEquals("sport = 'Football' AND team = 'Spurs'", inData.getMsgSelector().get());
		assertNull(inData.getFolder());
		assertFalse(inData.getFileNamePattern().isPresent());
		// Out
		assertEquals("C:\\outFiles", outData.getFolder());
		assertEquals("name1_;name2;*;xml", outData.getFileNamePattern());
		assertNull(outData.getConnData().getQmgr());		
	}
	
	private Properties createAllPropertiesFilesToQueue() {
		Properties props = new Properties();
		props.setProperty(ACTION, COPY);
		// Input		
		props.setProperty(I_FOLDER, "C:\\filesIn");
		props.setProperty(I_FILE_NAME_PATTERN, "name1_;name2;*;xml");
		props.setProperty(READBUFFER_SIZE, "4096");		
		// Output
		props.setProperty(O_HOST, "euuktetest01");
		props.setProperty(O_QUEUE_MANAGER, "QM_OUT");
		props.setProperty(O_QUEUE, "Q_OUT");
		props.setProperty(O_PORT, "3414");
		props.setProperty(O_SERVER_CHANNEL, "DEV.APP.SVRCONN");
		props.setProperty(GEN_CORREL_ID, "true");
		props.setProperty(PUT_WAIT_INTERVAL, "5");
		props.setProperty("msgprop.game", "Football");
		props.setProperty("msgprop.team", "Spurs");		
		
		return props;
	}
	
	private Properties createAllPropertiesQueueToFiles() {
		Properties props = new Properties();
		props.setProperty(ACTION, COPY);
		// Input
		props.setProperty(I_HOST, "euuktetest01");
		props.setProperty(I_QUEUE_MANAGER, "QM_OUT");
		props.setProperty(I_QUEUE, "Q_OUT");
		props.setProperty(I_PORT, "3414");
		props.setProperty(I_SERVER_CHANNEL, "DEV.APP.SVRCONN");
		props.setProperty(MSG_SELECTOR, "sport = 'Football' AND team = 'Spurs'");
		// Output
		props.setProperty(O_FOLDER, "C:\\filesOut");
		props.setProperty(O_FILE_NAME_PATTERN, "name1_;name2;*;xml");

		
		return props;
	}
	
	private Properties createAlternativePropertiesFilesToQueue() {
		// these are to verify that they can all be replaced from arges (except for msgprop values)
		Properties props = new Properties();
		props.setProperty(ACTION, MOVE);
		// Input		
		props.setProperty(I_FOLDER, "C:\\filesInAsWell");
		props.setProperty(I_FILE_NAME_PATTERN, "name1;*;xml");
		props.setProperty(READBUFFER_SIZE, "2048");		
		// Output
		props.setProperty(O_HOST, "euuktetest99");
		props.setProperty(O_QUEUE_MANAGER, "QMGR_OUT");
		props.setProperty(O_QUEUE, "QUEUE_OUT");
		props.setProperty(O_PORT, "3414");
		props.setProperty(O_SERVER_CHANNEL, "IT.APP.SVRCONN");
		props.setProperty(GEN_CORREL_ID, "false");
		props.setProperty(PUT_WAIT_INTERVAL, "1");
		props.setProperty("msgprop.sport", "Football");
		props.setProperty("msgprop.team", "Spurs");		
		
		return props;
	}
	
	private Properties createNoProperties() {
		Properties props = new Properties();
				
		return props;
	}
	
	private String[] createAllArgsFilesToQueue() {
		String[] args = new String[] {"-a", "copy", "-oqm", "QM_OUT", "-oq", "Q_OUT", "-oh", "euuktetest01", "-op", "3414", "-osc", "DEV.APP.SVRCONN",
				"-gcid", "true", "-pwi", "5", "-if", "C:\\filesIn", "-ifnp", "name1_;name2;*;xml", "-readBufSize", "4096"};
		return args;
	}

	private String[] createAllArgsWithoutActionFilesToQueue() {
		String[] args = new String[] {"-oqm", "QM_OUT", "-oq", "Q_OUT", "-oh", "euuktetest01", "-op", "3414", "-osc", "DEV.APP.SVRCONN",
				"-gcid", "true", "-pwi", "5", "-if", "C:\\filesIn", "-ifnp", "name1_;name2;*;xml", "-readBufSize", "4096"};
		return args;
	}
	
	private String[] createAllArgsWithPropertiesFileForFilesToQueue() {
		String[] args = new String[] {"-pf", "filesToQueue.properties", "-oqm", "QM_OUT", "-oq", "Q_OUT", "-oh", "euuktetest01", "-op", "3414", "-osc", "DEV.APP.SVRCONN",
				"-gcid", "true", "-pwi", "5", "-if", "C:\\filesIn", "-ifnp", "name1_;name2;*;xml", "-readBufSize", "4096"};
		return args;
	}

	private String[] createSomeArgsFilesToQueue() {
		String[] args = new String[] {"-oqm", "QM_OUT", "-oq", "Q_OUT", "-oh", "euuktetest01", "-gcid", "true", "-if", "C:\\filesIn"};
		return args;
	}
	
	private String[] createSomeArgsQueueToFiles() {
		String[] args = new String[] {"-iqm", "QM_IN", "-iq", "Q_IN", "-ih", "euuktetest01", "-ip", "3414", "-isc", "DEV.APP.SVRCONN",
				"-of", "C:\\outFiles"};
		return args;
	}
		
	private Properties createPropertiesQueueToFiles() {
		Properties props = new Properties();
		// Input
		props.setProperty(I_HOST, "hostIn");
		props.setProperty(I_QUEUE_MANAGER, "QMGR_IN");
		props.setProperty(I_QUEUE, "QUEUE_IN");
		props.setProperty(I_FOLDER, "C:\\inputFiles");
		props.setProperty(MSG_SELECTOR, "game = 'Football' AND team = 'Spurs'");
		// Output
		props.setProperty(O_FOLDER, "C:\\filesOut");
		props.setProperty(O_FILE_NAME_PATTERN, "name1_;name2;*;xml");
		
		return props;
	}

}
