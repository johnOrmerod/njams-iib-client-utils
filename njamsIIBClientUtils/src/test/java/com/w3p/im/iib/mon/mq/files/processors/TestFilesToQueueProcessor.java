package com.w3p.im.iib.mon.mq.files.processors;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;
import static org.mockito.Matchers.any;
import static org.mockito.Matchers.anyString;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.nio.file.DirectoryStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;

import javax.jms.JMSContext;
import javax.jms.JMSException;
import javax.jms.Queue;
import javax.jms.TextMessage;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.ClassRule;
import org.junit.Test;
import org.junit.rules.TemporaryFolder;
import org.mockito.Mockito;

import com.w3p.im.iib.mon.mq.files.data.Request;
import com.w3p.im.iib.mon.mq.files.data.RequestTestsHelper;
import com.w3p.im.iib.mon.mq.files.data.Response;
import com.w3p.im.iib.mon.mq.files.jms.CreateMocksForJMS;
import com.w3p.im.iib.mon.mq.files.jms.MockedJmsObjects;
import com.w3p.im.iib.mon.mq.files.processors.CreateRequest;
import com.w3p.im.iib.mon.mq.files.processors.FilesToQueueProcessor;
import com.w3p.im.iib.mon.mq.files.writers.MessagesWriter;

public class TestFilesToQueueProcessor {
	
	private static final Path inputFolderPath = Paths.get("src","test","resources", "inputFolder");
	private RequestTestsHelper helper = new RequestTestsHelper();
	private CreateMocksForJMS mocksForJMS = new CreateMocksForJMS();

	@ClassRule
	public static TemporaryFolder tempInputFolder = new TemporaryFolder();
	@ClassRule
	public static TemporaryFolder emptyInputFolder = new TemporaryFolder();
	@ClassRule
	public static TemporaryFolder fileInputFolder = new TemporaryFolder();

	@BeforeClass
	public static void copyTestFiles() throws IOException {
		// Copy test files to a jUnit temp folder		
		DirectoryStream<Path> filesList = Files.newDirectoryStream(inputFolderPath); // List the source files
		for (Path inFilePath : filesList) {
			String fileName = inFilePath.toFile().getName();
			File tempFile = tempInputFolder.newFile(fileName);
			Path tempFilePath = Paths.get(tempFile.getAbsolutePath());
			Files.copy(inFilePath, tempFilePath, StandardCopyOption.REPLACE_EXISTING, StandardCopyOption.COPY_ATTRIBUTES);
		}
		
		// Create a file in fileInputFolder for testing 'folder not folder'
		fileInputFolder.newFile("InputFolder");
	}
	@AfterClass
	public static void tearDownAfterClass() throws Exception {
	}

	@Before
	public void setUp() throws Exception {
	}

	@After
	public void tearDown() throws Exception {
	}

	@Test
	public void testCanCreateFilesToQueueProcessor() {
		FilesToQueueProcessor ftqw = new FilesToQueueProcessor();
		assertNotNull(ftqw);
	}
	
	@Test
	public void intTestCanRunFilesToQueueProcessor() throws FileNotFoundException {	
		String inputFolder = tempInputFolder.getRoot().getAbsolutePath();
		String[] args = new String[] {"-oqm", "IB10QMGR", "-oq", "NJAMS.UTILS.TEST.OUT", "-if", "C:\\TestFiles\\eis"};
		Request req = new CreateRequest().create(args);
		assertNotNull(req);
		assertNotNull(req.getInData().getFolder());
		assertNotNull(req.getOutData().getConnData());
		// Change to a jUnit test folder
		req.getInData().setFolder(inputFolder);		
		
		FilesToQueueProcessor ftqp = new FilesToQueueProcessor() {
			@Override
			protected boolean doYouWantToContinue() {
				return true;
			}
		};

		Response resp = ftqp.process(req, new MessagesWriter());
		assertNotNull(resp);
		assertTrue(resp.isSuccess());
		assertFalse(resp.getAllMessages().isPresent());
		assertEquals(3, resp.getItemsToBeCopied());
		assertEquals(3, resp.getItemsCopied());
		assertEquals(0, resp.getItemsDeleted());
		assertEquals(0, resp.getItemsInError());
	}
	
	@Test
	public void testCanRunFilesToQueueProcessor() throws FileNotFoundException, JMSException {
		String inputFolder = tempInputFolder.getRoot().getAbsolutePath();
		String[] args = new String[] {"-oqm", "IB10QMGR", "-oq", "NJAMS.UTILS.TEST.OUT", "-if", "C:\\TestFiles\\eis"};
		Request req = new CreateRequest().create(args); // Using the default Properties file
		assertNotNull(req);
		assertNotNull(req.getInData().getFolder());
		assertNotNull(req.getOutData().getConnData());
		// Change to a jUnit test folder
		req.getInData().setFolder(inputFolder);		
		
		FilesToQueueProcessor ftqp = new FilesToQueueProcessor() {
			@Override
			protected boolean doYouWantToContinue() {
				return true;
			}
		};
		
		// Mocking JMS connection
		MockedJmsObjects mjo = mocksForJMS.createForJMSConnection();
		JMSContext context = mjo.getJmsContext();
		Queue jmsQueue = mjo.getJmsQueue();
		when(jmsQueue.toString()).thenReturn("queue:///NJAMS.UTILS.TEST.OUT");
		
		TextMessage message = mock(TextMessage.class);
		when(context.createTextMessage()).thenReturn(message);						
		MessagesWriter mw = Mockito.mock(MessagesWriter.class);
		
		Response resp = ftqp.process(req, mw);
		assertNotNull(resp);
		assertTrue(resp.isSuccess());
		assertFalse(resp.getAllMessages().isPresent());		
		verify(mw, times(3)).write(anyString(), anyString(), any(Response.class));
		assertEquals(3, resp.getItemsToBeCopied());
	}

}
