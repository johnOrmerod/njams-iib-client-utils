package com.w3p.im.iib.mon.mq.files.processors;

import static org.junit.Assert.assertNotNull;

import java.io.IOException;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import com.w3p.im.iib.mon.mq.files.data.Request;
import com.w3p.im.iib.mon.mq.files.processors.CreateRequest;
import com.w3p.im.iib.mon.mq.files.processors.QueueToFilesProcessor;
import com.w3p.im.iib.mon.mq.files.writers.FilesWriter;

public class TestQueueToFilesProcessor {

	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
	}

	@AfterClass
	public static void tearDownAfterClass() throws Exception {
	}

	@Before
	public void setUp() throws Exception {
	}

	@After
	public void tearDown() throws Exception {
	}

	@Test
	public void testCanCreateQueueToFilesWriter() {
		QueueToFilesProcessor qtfw = new QueueToFilesProcessor();
		assertNotNull(qtfw);
	}
	
	@Test
	public void testCanRunQueueToFilesWriterInCopyMode() throws IOException {		
		String[] args = new String[] {"-iqm", "IB10QMGR", "-iq", "NJAMS.UTILS.TEST.OUT", "-of", "C:\\TestFiles\\eisFromQueue", "-ofnp", "tesing_%s.xml"};
		Request req = new CreateRequest().create(args);
		assertNotNull(req);
		assertNotNull(req.getInData().getConnData());
		assertNotNull(req.getOutData().getFolder());
		
		QueueToFilesProcessor qtfw = new QueueToFilesProcessor() {
			@Override
			protected boolean doYouWantToContinue() {
				return true;
			}
		};
		FilesWriter filesWriter = new FilesWriter();
		qtfw.process(req, filesWriter);
	}
	
	@Test
	public void testCanRunQueueToFilesWriterinMoveMode() throws IOException {		
		String[] args = new String[] {"-a", "move", "-iqm", "IB10QMGR", "-iq", "NJAMS.UTILS.TEST.OUT", "-of", "C:\\TestFiles\\eisFromQueue", "-ofnp", "tesing_%s.xml"};
		Request req = new CreateRequest().create(args);
		assertNotNull(req);
		assertNotNull(req.getInData().getConnData());
		assertNotNull(req.getOutData().getFolder());
		
		QueueToFilesProcessor qtfw = new QueueToFilesProcessor() {
			@Override
			protected boolean doYouWantToContinue() {
				return true;
			}
		};
		FilesWriter filesWriter = new FilesWriter();
		qtfw.process(req, filesWriter);
	}

}
