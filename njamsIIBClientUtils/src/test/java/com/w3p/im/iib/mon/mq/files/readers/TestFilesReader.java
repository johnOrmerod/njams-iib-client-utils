package com.w3p.im.iib.mon.mq.files.readers;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;
import static org.mockito.Mockito.mock;
import static org.mockito.Matchers.anyString;
import static org.mockito.Mockito.doThrow;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.io.File;
import java.io.IOException;
import java.nio.file.DirectoryStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;
import java.util.ArrayList;
import java.util.List;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.ClassRule;
import org.junit.Test;
import org.junit.rules.TemporaryFolder;

import com.w3p.im.iib.mon.mq.files.data.ConnectionData;
import com.w3p.im.iib.mon.mq.files.data.Request;
import com.w3p.im.iib.mon.mq.files.data.RequestTestsHelper;
import com.w3p.im.iib.mon.mq.files.data.Response;
import com.w3p.im.iib.mon.mq.files.readers.FilesReader;
import com.w3p.im.iib.mon.mq.files.writers.IWriter;
import com.w3p.im.iib.mon.mq.files.writers.MessagesWriter;

public class TestFilesReader {
	
	private static final Path inputFolderPath = Paths.get("src","test","resources", "inputFolder");
	private RequestTestsHelper helper = new RequestTestsHelper();

	@ClassRule
	public static TemporaryFolder tempInputFolder = new TemporaryFolder();
	@ClassRule
	public static TemporaryFolder emptyInputFolder = new TemporaryFolder();
	@ClassRule
	public static TemporaryFolder fileInputFolder = new TemporaryFolder();

	@BeforeClass
	public static void copyTestFiles() throws IOException {
		// Copy test files to a jUnit temp folder		
		DirectoryStream<Path> filesList = Files.newDirectoryStream(inputFolderPath); // List the source files
		for (Path inFilePath : filesList) {
			String fileName = inFilePath.toFile().getName();
			File tempFile = tempInputFolder.newFile(fileName);
			Path tempFilePath = Paths.get(tempFile.getAbsolutePath());
			Files.copy(inFilePath, tempFilePath, StandardCopyOption.REPLACE_EXISTING, StandardCopyOption.COPY_ATTRIBUTES);
		}
		
		// Create a file in fileInputFolder for testing 'folder not folder'
		fileInputFolder.newFile("InputFolder");
	}

	@AfterClass
	public static void tearDownAfterClass() throws Exception {
	}

	@Before
	public void setUp() throws Exception {
	}

	@After
	public void tearDown() throws Exception {
	}

	@Test
	public void testCanCreateFilesReader() {
		FilesReader fr = new FilesReader();
		assertNotNull(fr);
	}

	@Test
	public void intTestCanReadFilesFromFolderAndWriteToQueue() {
		Response resp = new Response();
		IWriter mw = new MessagesWriter();
		ConnectionData cd = helper.createConnectionDataForIB10QMGR(); // Real MQ
		Request req = helper.createRequestFilesToQueue("copy", cd);
		String inputFolder = tempInputFolder.getRoot().getAbsolutePath();
		req.getInData().setFolder(inputFolder);
		FilesReader fr = new FilesReader();
		fr.readAll(req, resp, mw);
		
		assertTrue(resp.isSuccess());
		assertEquals(3, resp.getItemsToBeCopied());
		assertEquals(3, resp.getItemsCopied());
		assertEquals(0, resp.getItemsDeleted());
		assertEquals(0, resp.getItemsInError());
		assertFalse(resp.getAllMessages().isPresent());
	}

	@Test
	public void testCanReadFilesFromFolder() {
		Response resp = new Response();
		IWriter mw = mock(MessagesWriter.class);
		ConnectionData cd = helper.createConnectionDataForIB10QMGR(); // Real MQ
		Request req = helper.createRequestFilesToQueue("copy", cd);
		String inputFolder = tempInputFolder.getRoot().getAbsolutePath();
		req.getInData().setFolder(inputFolder);
		FilesReader fr = new FilesReader();
		fr.readAll(req, resp, mw);
		
		assertTrue(resp.isSuccess());
		assertEquals(3, resp.getItemsToBeCopied());
		assertEquals(0, resp.getItemsCopied());
		assertEquals(0, resp.getItemsDeleted());
		assertEquals(0, resp.getItemsInError());
		assertFalse(resp.getAllMessages().isPresent());		
		
	}

	@Test
	public void testCanGetFileContentsAsString() throws IOException {
		Response resp = new Response();
		// Get a list of files for testing
		List<Path> filesForTesting = getListOfFilesForTesting();
		
		Path testFile_0 = filesForTesting.get(0);
		
		FilesReader fr = new FilesReader();
		String fileContents = fr.getFileContentsAsString(testFile_0, 4096, resp);
		assertNotNull(fileContents);
		assertTrue(fileContents.startsWith("<?xml"));		
	}

	@Test
	public void testCanHandleFileDoesNotExistWhenGettingFileContentsAsString() throws IOException {
		Response resp = new Response();		
		Path errorPath = Paths.get("missing");
		
		FilesReader fr = new FilesReader();
		String fileContents = fr.getFileContentsAsString(errorPath, 4096, resp);
		assertNull(fileContents);
		assertFalse(resp.isSuccess());
		assertTrue(resp.getAllMessages().isPresent());
		assertTrue(resp.getAllMessages().get().startsWith("Reading file: Error reading contents of inputfile 'missing' before writing to the MQ queue."));
	}
	
	private List<Path> getListOfFilesForTesting() throws IOException {
		List<Path> filesForTesting = new ArrayList<>();
		String inputFolder = tempInputFolder.getRoot().getAbsolutePath();
		Path inputFolderPath = Paths.get(inputFolder);
		try (DirectoryStream<Path> stream = Files.newDirectoryStream(inputFolderPath)) {
			for (Path file : stream) {
				filesForTesting.add(file);
			}
		}
		return filesForTesting;
	}
}
