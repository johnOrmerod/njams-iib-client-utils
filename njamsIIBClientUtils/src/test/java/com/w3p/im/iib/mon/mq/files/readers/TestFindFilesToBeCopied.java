package com.w3p.im.iib.mon.mq.files.readers;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import java.io.File;
import java.io.IOException;
import java.nio.file.DirectoryStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.ClassRule;
import org.junit.Test;
import org.junit.rules.TemporaryFolder;

import com.w3p.im.iib.mon.mq.files.data.Request;
import com.w3p.im.iib.mon.mq.files.data.RequestDataIn;
import com.w3p.im.iib.mon.mq.files.data.RequestTestsHelper;
import com.w3p.im.iib.mon.mq.files.data.Response;
import com.w3p.im.iib.mon.mq.files.readers.FilesToBeCopied;
import com.w3p.im.iib.mon.mq.files.readers.FindFilesToBeCopied;

public class TestFindFilesToBeCopied {
	
	private static final Path inputFolderPath = Paths.get("src","test","resources", "inputFolder");
	private RequestTestsHelper helper = new RequestTestsHelper();

	@ClassRule
	public static TemporaryFolder tempInputFolder = new TemporaryFolder();
	@ClassRule
	public static TemporaryFolder emptyInputFolder = new TemporaryFolder();
	@ClassRule
	public static TemporaryFolder fileInputFolder = new TemporaryFolder();

	@BeforeClass
	public static void copyTestFiles() throws IOException {
		// Copy test files to a jUnit temp folder		
		DirectoryStream<Path> filesList = Files.newDirectoryStream(inputFolderPath); // List the source files
		for (Path inFilePath : filesList) {
			String fileName = inFilePath.toFile().getName();
			File tempFile = tempInputFolder.newFile(fileName);
			Path tempFilePath = Paths.get(tempFile.getAbsolutePath());
			Files.copy(inFilePath, tempFilePath, StandardCopyOption.REPLACE_EXISTING, StandardCopyOption.COPY_ATTRIBUTES);
		}
		
		// Create a file in fileInputFolder for testing 'folder not folder'
		fileInputFolder.newFile("InputFolder");
	}

	@AfterClass
	public static void tearDownAfterClass() throws Exception {
	}

	@Before
	public void setUp() throws Exception {
	}

	@After
	public void tearDown() throws Exception {
	}

	@Test
	public void testCanCreateFindFilesToBeCopied() {
		FindFilesToBeCopied finder = new FindFilesToBeCopied();
		assertNotNull(finder);
	}

	@Test
	public void testCanFindFilesToBeCopied() {
		Response resp = new Response();
		Request req = helper.createRequestFilesToQueue("copy");
		String inputFolder = tempInputFolder.getRoot().getAbsolutePath();
		RequestDataIn requestDataIn = req.getInData().setFolder(inputFolder);
		FindFilesToBeCopied finder = new FindFilesToBeCopied();
		
		FilesToBeCopied ftbc = finder.find(requestDataIn, resp);
		
		assertNotNull(ftbc);
		assertTrue(resp.isSuccess());
		assertFalse(resp.getAllMessages().isPresent());
		assertEquals(3, ftbc.getFiles().size());
	}
	
	@Test
	public void testCanFindFilesToBeCopiedByNamePattern() {
		Response resp = new Response();
		Request req = helper.createRequestFilesToQueue("copy");
		String inputFolder = tempInputFolder.getRoot().getAbsolutePath();
		RequestDataIn requestDataIn = req.getInData().setFolder(inputFolder);
		requestDataIn.setFileNamePattern("*-symbol.xml");
		FindFilesToBeCopied finder = new FindFilesToBeCopied();
		
		FilesToBeCopied ftbc = finder.find(requestDataIn, resp);
		
		assertNotNull(ftbc);
		assertTrue(resp.isSuccess());
		assertFalse(resp.getAllMessages().isPresent());
		assertEquals(1, ftbc.getFiles().size());
	}
	
	@Test
	public void testCanHandleFilesToBeCopiedAreExcludedByNamePattern() {
		Response resp = new Response();
		Request req = helper.createRequestFilesToQueue("copy");
		String inputFolder = tempInputFolder.getRoot().getAbsolutePath();
		RequestDataIn requestDataIn = req.getInData().setFolder(inputFolder);
		requestDataIn.setFileNamePattern("*.pdf");
		FindFilesToBeCopied finder = new FindFilesToBeCopied();
		
		FilesToBeCopied ftbc = finder.find(requestDataIn, resp);
		
		assertNotNull(ftbc);
		assertTrue(resp.isSuccess());
		assertFalse(resp.getAllMessages().isPresent());
		assertEquals(0, ftbc.getFiles().size());
	}
	
	@Test
	public void testCanHandleNoInputFilesToBeCopied() {	
		Response resp = new Response();
		Request req = helper.createRequestFilesToQueue("copy");
		String inputFolder = emptyInputFolder.getRoot().getAbsolutePath();		
		RequestDataIn requestDataIn = req.getInData().setFolder(inputFolder);
		FindFilesToBeCopied finder = new FindFilesToBeCopied();
				
		FilesToBeCopied ftbc = finder.find(requestDataIn, resp);	
		
		assertFalse(resp.isSuccess());
		assertTrue(resp.getAllMessages().isPresent());
		assertEquals(0, ftbc.getFiles().size());
		assertTrue(resp.getAllMessages().get().startsWith("Empty folder:"));
	}
	
	@Test
	public void testCanHandleInputFolderNotExist() {
		Response resp = new Response();
		Request req = helper.createRequestFilesToQueue("copy");
		Path inputFolderPath = Paths.get("src","test","resources", "notFound"); 
		String inputFolder = inputFolderPath.toFile().getAbsolutePath();
		RequestDataIn requestDataIn = req.getInData().setFolder(inputFolder);
		FindFilesToBeCopied finder = new FindFilesToBeCopied();
		
		FilesToBeCopied ftbc = finder.find(requestDataIn, resp);
		
		assertFalse(resp.isSuccess());
		assertTrue(resp.getAllMessages().isPresent());
		assertTrue(resp.getAllMessages().get().startsWith("Input folder does not exist"));
		assertEquals(0, ftbc.getFiles().size());
	}
	
	@Test
	public void testCanHandleInputFolderNotFolder() {
		Response resp = new Response();
		Request req = helper.createRequestFilesToQueue("copy");
		String inputRoot = fileInputFolder.getRoot().getAbsolutePath();
		String inputFolder = new File(inputRoot, "InputFolder").getAbsolutePath();
		RequestDataIn requestDataIn = req.getInData().setFolder(inputFolder);
		FindFilesToBeCopied finder = new FindFilesToBeCopied();
				
		FilesToBeCopied ftbc = finder.find(requestDataIn, resp);
		
		assertFalse(resp.isSuccess());
		assertTrue(resp.getAllMessages().isPresent());
		assertTrue(resp.getAllMessages().get().startsWith("Input folder is not a directory"));
		assertEquals(0, ftbc.getFiles().size());
	}

	@Test
	public void testCanHandleThrownIOException() {
		System.out.println("Can't find an easy way to mock 'Files.newDirectoryStream(inputFolder)' and throw an IOException"); 
	}
}
