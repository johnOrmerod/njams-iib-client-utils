package com.w3p.im.iib.mon.mq.files.readers;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;
import static org.mockito.Matchers.anyString;
import static org.mockito.Mockito.doThrow;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.io.IOException;

import javax.jms.JMSContext;
import javax.jms.JMSException;
import javax.jms.JMSRuntimeException;
import javax.jms.Queue;
import javax.jms.TextMessage;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Ignore;
import org.junit.Test;

import com.ibm.msg.client.jms.JmsConnectionFactory;
import com.ibm.msg.client.jms.JmsFactoryFactory;
import com.ibm.msg.client.wmq.WMQConstants;
import com.w3p.im.iib.mon.mq.files.data.ConnectionData;
import com.w3p.im.iib.mon.mq.files.data.Request;
import com.w3p.im.iib.mon.mq.files.data.RequestDataIn;
import com.w3p.im.iib.mon.mq.files.data.RequestTestsHelper;
import com.w3p.im.iib.mon.mq.files.data.Response;
import com.w3p.im.iib.mon.mq.files.exceptions.MessagingException;
import com.w3p.im.iib.mon.mq.files.jms.CreateMocksForJMS;
import com.w3p.im.iib.mon.mq.files.jms.JmsFactoryFactoryWrapper;
import com.w3p.im.iib.mon.mq.files.jms.MockedJmsObjects;
import com.w3p.im.iib.mon.mq.files.readers.MessagesReader;
import com.w3p.im.iib.mon.mq.files.writers.FilesWriter;
import com.w3p.im.iib.mon.mq.files.writers.IWriter;
import com.w3p.im.iib.mon.mq.files.writers.MessagesWriter;

public class TestMessagesReader {
	
	private RequestTestsHelper helper = new RequestTestsHelper();
	private CreateMocksForJMS mocksForJMS = new CreateMocksForJMS();

	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
	}

	@AfterClass
	public static void tearDownAfterClass() throws Exception {
	}

	@Before
	public void setUp() throws Exception {
	}

	@After
	public void tearDown() throws Exception {
	}

	
	@Test
	public void testCanCreateMessagesReader() {
		MessagesReader mr = new MessagesReader();
		assertNotNull(mr);
	}
	
//	@Ignore("Integration Test - accesses MQ")
	@Test
	public void intTestCanInitMessagesReader() {   // For integ test
		Response resp = new Response();
		IWriter fw = new FilesWriter();
		ConnectionData cd = helper.createConnectionDataForIB10QMGR(); // Real MQ
		cd.setHost("localhost");
		cd.setQmgr("IB10QMGR");
		cd.setQueue("NJAMS.UTILS.TEST.IN");
		cd.setPort(3414);
		cd.setServerChan("DEV.APP.SVRCONN");
		Request req = helper.createRequestQueueToFiles("copy", cd);
		req.getInData().setMsgSelector(null);
		req.getOutData().setFileNamePattern("xxx$s.json");
		
		MessagesReader mr = new MessagesReader();

//		RequestDataIn reqDataIn = helper.createRequestDataInForIB10QMGR();
//		mr.init(req, resp, fw);
		mr.readAll(req, resp, fw);
		//TODO check response
		assertNotNull(mr); 
		assertNotNull(mr.getDest());
		assertEquals("queue:///NJAMS.UTILS.TEST.IN", mr.getDest().toString());		
	}
	
	@Test
	public void testMockCanInitMessagesReader() throws JMSException, IOException {
		
		MockedJmsObjects mjo = mocksForJMS.createForJMSConnection();
		JmsFactoryFactoryWrapper jffw = mjo.getJmsFactoryFactoryWrapper();
		JmsFactoryFactory jff = mjo.getJmsFactoryFactory();
		JmsConnectionFactory connFac = mjo.getJmsConnectionFactory();
		JMSContext context = mjo.getJmsContext();
		Queue jmsQueue = mjo.getJmsQueue();
		when(jmsQueue.toString()).thenReturn("queue:///NJAMS.UTILS.TEST.OUT");
		
		Response resp = new Response();
		ConnectionData cd = helper.createConnectionDataForIB10QMGR();
		Request req = helper.createRequestQueueToFiles("copy", cd);
		RequestDataIn reqDataIn = helper.createRequestDataInForIB10QMGR();
		
		IWriter filesWriter = mock(FilesWriter.class);
		
		try(MessagesReader mr = new MessagesReader()) { 
			mr.init(req, resp, filesWriter, jffw); 
			//TODO check response
			assertNotNull(mr);
//			assertNotNull(mr.producer);
			assertNotNull(mr.getDest());
			assertEquals("queue:///NJAMS.UTILS.TEST.OUT", mr.getDest().toString());	
		}
		

		
		verify(jffw).getFactory(WMQConstants.WMQ_PROVIDER);
		verify(jff).createConnectionFactory();
		
		verify(connFac, times(4)).setStringProperty(anyString(), anyString());
		verify(connFac, times(1)).setIntProperty(WMQConstants.WMQ_CONNECTION_MODE, WMQConstants.WMQ_CM_CLIENT);
		verify(connFac, times(1)).setIntProperty(WMQConstants.WMQ_PORT, reqDataIn.getConnData().getPort());
		verify(connFac, times(1)).setBooleanProperty(WMQConstants.USER_AUTHENTICATION_MQCSP, false);
		
//		verify(context).createProducer();
		verify(context).createQueue("queue:///NJAMS.UTILS.TEST.OUT");		
		verify(context).close(); // Check that the auto-close worked.			
	}
/*	
	@Test
	public void testMockCanHandleJMSExceptionWhenCreatingFactoryFactory() throws JMSException {
		
		MockedJmsObjects mjo = mocksForJMS.createForJMSConnection();
		JmsFactoryFactoryWrapper jffw = mjo.getJmsFactoryFactoryWrapper();
		
		when(jffw.getFactory(WMQConstants.WMQ_PROVIDER)).thenThrow(new JMSException("Error creating the JmsFactoryFactory"));		

		Response resp = new Response();
		RequestDataIn reqDataIn = helper.createRequestDataInForIB10QMGR();
		
		try(MessagesReader mr = new MessagesReader()) { 
			mr.init(reqDataIn, resp, jffw);
			//TODO check response
			assertNotNull(mr);
		} catch (MessagingException e) {			
			String errMsg = "An MQ/JMS error occured. Message is 'Error occurred when creating the JMS Connection Factory. See log.'.\n" + 
					"  Cause is 'Error creating the JmsFactoryFactory'.";
			assertEquals(errMsg, e.getMessage());
			assertTrue(e.getCause().getClass().isAssignableFrom(JMSException.class));
			assertEquals("Error creating the JmsFactoryFactory", e.getCause().getMessage());
		}
		// Follow the code path
		verify(jffw).getFactory(WMQConstants.WMQ_PROVIDER);
	}
	
	@Test
	public void testMockCanHandleJMSExceptionWhenCreatingConnectionFactory() throws JMSException {
		
		MockedJmsObjects mjo = mocksForJMS.createForJMSConnection();
		JmsFactoryFactoryWrapper jffw = mjo.getJmsFactoryFactoryWrapper();
		JmsFactoryFactory jff = mjo.getJmsFactoryFactory();
		
		when(jff.createConnectionFactory()).thenThrow(new JMSException("Error creating the JmsConnectionFactory"));		
		
		Response resp = new Response();
		RequestDataIn reqDataIn = helper.createRequestDataInForIB10QMGR();
		
		try(MessagesReader mr = new MessagesReader()) { 
			mr.init(reqDataIn, resp, jffw);
			//TODO check response
			assertNotNull(mr);
		} catch (MessagingException e) {			
			String errMsg = "An MQ/JMS error occured. Message is 'Error occurred when creating the JMS Connection Factory. See log.'.\n" + 
					"  Cause is 'Error creating the JmsConnectionFactory'.";
			assertEquals(errMsg, e.getMessage());
			assertTrue(e.getCause().getClass().isAssignableFrom(JMSException.class));
			assertEquals("Error creating the JmsConnectionFactory", e.getCause().getMessage());
		}
		// Follow the code path
		verify(jffw).getFactory(WMQConstants.WMQ_PROVIDER);
	}

	@Test
	public void testMockCanHandleJMSExceptionWhenConfiguringConnectionFactory() throws JMSException {
		
		MockedJmsObjects mjo = mocksForJMS.createForJMSConnection();
		JmsFactoryFactoryWrapper jffw = mjo.getJmsFactoryFactoryWrapper();
		JmsConnectionFactory connFac = mjo.getJmsConnectionFactory();
		
		Response resp = new Response();
		RequestDataIn reqDataIn = helper.createRequestDataInForIB10QMGR();
		ConnectionData connData = reqDataIn.getConnData();
		// To handle a 'when' on a method that returns 'null' (e.g a setter), see: https://stackoverflow.com/questions/29537574/mockito-error-is-not-applicable-for-the-arguments-void
		doThrow(new JMSException("Error occurred when setting an Integer property for the Connection Factory")).
			when(connFac).setIntProperty(WMQConstants.WMQ_PORT, connData.getPort());
		
		try(MessagesReader mr = new MessagesReader()) { 
			mr.init(reqDataIn, resp, jffw);
			//TODO check response
			assertNotNull(mr);
		} catch (MessagingException e) {			
			String errMsg = "An MQ/JMS error occured. Message is 'Error occurred when configuring the JMS Connection Factory. See log.'.\n" + 
					"  Cause is 'Error occurred when setting an Integer property for the Connection Factory'.";
			assertEquals(errMsg, e.getMessage());
			assertTrue(e.getCause().getClass().isAssignableFrom(JMSException.class));
			assertEquals("Error occurred when setting an Integer property for the Connection Factory", e.getCause().getMessage());
		}
		// Follow the code path
		verify(jffw).getFactory(WMQConstants.WMQ_PROVIDER);
	}
	
	@Test
	public void testMockCanHandleJMSExceptionWhenCreatingJMSContext() throws JMSException {
		
		MockedJmsObjects mjo = mocksForJMS.createForJMSConnection();
		JmsFactoryFactoryWrapper jffw = mjo.getJmsFactoryFactoryWrapper();
		JmsConnectionFactory connFac = mjo.getJmsConnectionFactory();
		
		when(connFac.createContext(JMSContext.AUTO_ACKNOWLEDGE)).
			thenThrow(new JMSRuntimeException("Internal error occurred when creating the JMS Context"));		

		Response resp = new Response();
		RequestDataIn reqDataIn = helper.createRequestDataInForIB10QMGR();
		ConnectionData connData = reqDataIn.getConnData();
		
		try(MessagesReader mr = new MessagesReader()) { 
			mr.init(reqDataIn, resp, jffw);
			//TODO check response
			assertNotNull(mr);
		} catch (MessagingException e) {			
			String errMsg = "An MQ/JMS error occured. Message is 'Error occurred when creating the JMS Context. See log.'.\n" + 
					"  Cause is 'Internal error occurred when creating the JMS Context'.";
			assertEquals(errMsg, e.getMessage());
			assertTrue(e.getCause().getClass().isAssignableFrom(JMSRuntimeException.class));
			assertEquals("Internal error occurred when creating the JMS Context", e.getCause().getMessage());
		}
		// Follow the code path
		verify(jffw).getFactory(WMQConstants.WMQ_PROVIDER);
		verify(connFac).setStringProperty(WMQConstants.WMQ_HOST_NAME, connData.getHost());
		verify(connFac).createContext(JMSContext.AUTO_ACKNOWLEDGE);
	}
	
	@Test
	public void testMockCanHandleJMSExceptionWhenCreatingJMSDestination() throws JMSException {
		
		MockedJmsObjects mjo = mocksForJMS.createForJMSConnection();
		JmsFactoryFactoryWrapper jffw = mjo.getJmsFactoryFactoryWrapper();
		JmsConnectionFactory connFac = mjo.getJmsConnectionFactory();
		JMSContext context = mjo.getJmsContext();
		Queue jmsQueue = mjo.getJmsQueue();
		when(jmsQueue.toString()).thenReturn("queue:///NJAMS.UTILS.TEST.OUT");
		
		Response resp = new Response();
		RequestDataIn reqDataIn = helper.createRequestDataInForIB10QMGR();
		ConnectionData connData = reqDataIn.getConnData();		
		
		when(context.createQueue(String.format("queue:///%s", connData.getQueue()))).
			thenThrow(new JMSRuntimeException("Internal error occurred when creating the Queue object"));		

		
		
		try(MessagesReader mr = new MessagesReader()) { 
			mr.init(reqDataIn, resp, jffw);
			assertNotNull(mr);
			//TODO check response
		} catch (MessagingException e) {			
			String errMsg = "An MQ/JMS error occured. Message is 'Error occurred when creating the JMS Destination 'NJAMS.UTILS.TEST.OUT'. See log.'.\n" + 
					"  Cause is 'Internal error occurred when creating the Queue object'.";
			assertEquals(errMsg, e.getMessage());
			assertTrue(e.getCause().getClass().isAssignableFrom(JMSRuntimeException.class));
			assertEquals("Internal error occurred when creating the Queue object", e.getCause().getMessage());
		}
		// Follow the code path
		verify(jffw).getFactory(WMQConstants.WMQ_PROVIDER);
		verify(connFac).setStringProperty(WMQConstants.WMQ_HOST_NAME, connData.getHost());
		verify(connFac).createContext(JMSContext.AUTO_ACKNOWLEDGE);
		verify(context).createQueue(String.format("queue:///%s", connData.getQueue()));
		verify(context).close(); // Check that the auto-close worked.	
	}
	
	@Test
	public void testMockCanHandleJMSExceptionWhenCreatingTextMessage() throws JMSException {
		
		MockedJmsObjects mjo = mocksForJMS.createForJMSConnection();
		JmsFactoryFactoryWrapper jffw = mjo.getJmsFactoryFactoryWrapper();
		JmsConnectionFactory connFac = mjo.getJmsConnectionFactory();
		JMSContext context = mjo.getJmsContext();
		Queue jmsQueue = mjo.getJmsQueue();
		when(jmsQueue.toString()).thenReturn("queue:///NJAMS.UTILS.TEST.OUT");
		
		Response resp = new Response();
		RequestDataIn reqDataIn = helper.createRequestDataInForIB10QMGR();
		ConnectionData connData = reqDataIn.getConnData();		
		
		when(context.createTextMessage()).
			thenThrow(new JMSRuntimeException("Internal error occurred when creating the message"));		
		
		try(MessagesReader mr = new MessagesReader()) { 
			mr.init(reqDataIn, resp, jffw);
			assertNotNull(mr);
			assertTrue(resp.isSuccess());
			mr.write("fileName", "<?xml >", resp);
		}
		
		assertFalse(resp.isSuccess());
		assertEquals(1, resp.getFilesInError());
		assertTrue(resp.getAllMessages().isPresent());
		System.out.println(resp.getAllMessages().get());
		// Seems to be an issue with a trailing space, comparing values.
		assertTrue(resp.getAllMessages().get().startsWith("Message writer: Error trying to send the JMS message to destination 'queue:///NJAMS.UTILS.TEST.OUT'." +
				" Reason: 'javax.jms.JMSRuntimeException: Internal error occurred when creating the message'."));
		
		// Follow the code path
		verify(jffw).getFactory(WMQConstants.WMQ_PROVIDER);
		verify(connFac).setStringProperty(WMQConstants.WMQ_HOST_NAME, connData.getHost());
		verify(connFac).createContext(JMSContext.AUTO_ACKNOWLEDGE);
		verify(context).createQueue(String.format("queue:///%s", connData.getQueue()));
		verify(context).createTextMessage();		
		verify(context).close(); // Check that the auto-close worked.	
	}
	
	
	@Test
	public void testMockCanHandleJMSExceptionWhenSettingMessageProperties() throws JMSException {
		
		MockedJmsObjects mjo = mocksForJMS.createForJMSConnection();
		JmsFactoryFactoryWrapper jffw = mjo.getJmsFactoryFactoryWrapper();
		JmsConnectionFactory connFac = mjo.getJmsConnectionFactory();
		JMSContext context = mjo.getJmsContext();
		Queue jmsQueue = mjo.getJmsQueue();
		when(jmsQueue.toString()).thenReturn("queue:///NJAMS.UTILS.TEST.OUT");
		
		Response resp = new Response();
		RequestDataIn reqDataIn = helper.createRequestDataInForIB10QMGR();
		reqDataIn.setGenCorrelId(true); // Make sure this code is used		
		ConnectionData connData = reqDataIn.getConnData();
		
		TextMessage message = mock(TextMessage.class);
		
		when(context.createTextMessage()).thenReturn(message);
		
		doThrow(new JMSException("Error occurred when setting the correlationId for the message")).
			when(message).setJMSCorrelationIDAsBytes(anyString().getBytes());
		
		try(MessagesReader mr = new MessagesReader()) { 
			mr.init(reqDataIn, resp, jffw);
			assertNotNull(mr);
			assertTrue(resp.isSuccess());
			mr.write("fileName", "<?xml >", resp);
		}
		
		assertFalse(resp.isSuccess());
		assertEquals(1, resp.getFilesInError());
		assertTrue(resp.getAllMessages().isPresent());
		System.out.println(resp.getAllMessages().get());
		// Error setting properties for the JMS Text Message. Reason: '%s'.
		// Seems to be an issue with a trailing space, comparing values.
		assertTrue(resp.getAllMessages().get().startsWith("Message writer: Error setting properties for the JMS Text Message for file 'fileName'."
				+ " Reason: 'javax.jms.JMSException: Error occurred when setting the correlationId for the message'."));
		
		// Follow the code path
		verify(jffw).getFactory(WMQConstants.WMQ_PROVIDER);
		verify(connFac).setStringProperty(WMQConstants.WMQ_HOST_NAME, connData.getHost());
		verify(connFac).createContext(JMSContext.AUTO_ACKNOWLEDGE);
		verify(context).createQueue(String.format("queue:///%s", connData.getQueue()));
		verify(context).createTextMessage();
		verify(message).setText(anyString());
		verify(message).setJMSCorrelationIDAsBytes(anyString().getBytes());
		verify(context).close(); // Check that the auto-close worked.	
	}
	
	@Test
	public void testMockCanHandleUnhandledExceptionWhenSettingMessageProperties() throws JMSException {
		
		MockedJmsObjects mjo = mocksForJMS.createForJMSConnection();
		JmsFactoryFactoryWrapper jffw = mjo.getJmsFactoryFactoryWrapper();
		JmsConnectionFactory connFac = mjo.getJmsConnectionFactory();
		JMSContext context = mjo.getJmsContext();
		Queue jmsQueue = mjo.getJmsQueue();
		when(jmsQueue.toString()).thenReturn("queue:///NJAMS.UTILS.TEST.OUT");
		
		Response resp = new Response();
		RequestDataIn reqDataIn = helper.createRequestDataInForIB10QMGR();
		reqDataIn.setGenCorrelId(true); // Make sure this code is used		
		ConnectionData connData = reqDataIn.getConnData();
		
		TextMessage message = mock(TextMessage.class);
		
		when(context.createTextMessage()).thenReturn(message);
		
		doThrow(new RuntimeException("Something stupid happened!")).
			when(message).setText(anyString());
		
		try(MessagesReader mr = new MessagesReader()) { 
			mr.init(reqDataIn, resp, jffw);
			assertNotNull(mr);
			assertTrue(resp.isSuccess());
			mr.write("fileName", "<?xml >", resp);
		}
		
		assertFalse(resp.isSuccess());
		assertEquals(1, resp.getFilesInError());
		assertTrue(resp.getAllMessages().isPresent());
		System.out.println(resp.getAllMessages().get());
		// Error setting properties for the JMS Text Message. Reason: '%s'.
		// Seems to be an issue with a trailing space, comparing values.
		assertTrue(resp.getAllMessages().get().startsWith("Message writer: Unhandled error creating the JMS Text Message for file 'fileName'."
				+ " Reason: 'java.lang.RuntimeException: Something stupid happened!'."));
		
		// Follow the code path
		verify(jffw).getFactory(WMQConstants.WMQ_PROVIDER);
		verify(connFac).setStringProperty(WMQConstants.WMQ_HOST_NAME, connData.getHost());
		verify(connFac).createContext(JMSContext.AUTO_ACKNOWLEDGE);
		verify(context).createQueue(String.format("queue:///%s", connData.getQueue()));
		verify(context).createTextMessage();		
		verify(message).setText(anyString());		
		verify(context).close(); // Check that the auto-close worked.	
	}
	
*/	

//	@Test
//	public void testCanCreateMessage() {
//		Response resp = new Response();
//		String msgContent = "This is a test message";
//		RequestDataIn reqDataIn = helper.createRequestDataInForIB10QMGR();
//		reqDataIn.setGenCorrelId(true);
//		reqDataIn.getMsgProperties().put("team", "Spurs");
//		try (MessagesReader mr = new MessagesReader()) {
//			mr.init(reqDataIn, resp);	
//			//TODO check response
//			TextMessage msg = mr.createMessage(msgContent);			
//			assertNotNull(msg.getJMSCorrelationID());
//			assertEquals("Spurs", msg.getStringProperty("team"));			
//		} catch (JMSException e) {
//			fail(String.format("Unplanned JMSExcption occured: %s", e.toString()));
//		}
//	}

	@Ignore("Integration Test - accesses MQ")
	@Test
	public void intTestCanWriteMessage() {		
//		Response resp = new Response();
//		resp.setFilesToBeCopied(1);
//		RequestDataIn reqDataIn = helper.createRequestDataInForIB10QMGR();
//		reqDataIn.setGenCorrelId(true);
//		reqDataIn.getMsgProperties().put("team", "Spurs");
//		
//		try (MessagesReader mr = new MessagesReader()) {
//			mr.init(reqDataIn, resp);
//			String fileName = "testFile";
//			String msgContent = "This is a test message";
//			mr.write(fileName, msgContent, resp);
//			//TODO check response
//			assertTrue(resp.isSuccess());
//			assertEquals(1,resp.getFilesToBeCopied());
//			assertEquals(1,resp.getFilesCopied());
//			assertEquals(0,resp.getFilesInError());
//			assertEquals(0,resp.getFilesDeleted());
//			assertFalse(resp.getAllMessages().isPresent());
//		}
	}

	@Test
	public void testMockCanWriteMessage() throws JMSException {		
//		Response resp = new Response();
//		resp.setFilesToBeCopied(1);
//		RequestDataIn reqDataIn = helper.createRequestDataInForIB10QMGR();
//		reqDataIn.setGenCorrelId(true);
//		reqDataIn.getMsgProperties().put("team", "Spurs");
//		
//		// Mocking JMS connection
//		MockedJmsObjects mjo = mocksForJMS.createForJMSConnection();
//		JmsFactoryFactoryWrapper jffw = mjo.getJmsFactoryFactoryWrapper();
//		JMSContext context = mjo.getJmsContext();
//		Queue jmsQueue = mjo.getJmsQueue();
//		when(jmsQueue.toString()).thenReturn("queue:///NJAMS.UTILS.TEST.OUT");
//		
//		TextMessage message = mock(TextMessage.class);
//		when(context.createTextMessage()).thenReturn(message);
//		
//		IWriter filesWriter = mock(FilesWriter.class);
//
//		String fileName = "testFile";
//		String msgContent = "This is a test message";
//		try (MessagesReader mr = new MessagesReader()) {
////			mr.init(reqDataIn, resp, filesWriter, jffw);
//			//TODO check response
//			mr.readAll(req, resp, filesWriter);
//		}
//		assertTrue(resp.isSuccess());
//		assertEquals(1,resp.getFilesToBeCopied());
//		assertEquals(1,resp.getFilesCopied());
//		assertEquals(0,resp.getFilesInError());
//		assertEquals(0,resp.getFilesDeleted());
//		assertFalse(resp.getAllMessages().isPresent());
//		// Verify paths taken
//		verify(context).createTextMessage();
//		verify(message).setIntProperty(WMQConstants.JMS_IBM_CHARACTER_SET, 1208);
//		verify(message).setText(msgContent);
//		verify(message).setJMSCorrelationIDAsBytes(anyString().getBytes());
//		verify(message).setStringProperty("team", "Spurs");
//		verify(context).close(); // Check that the auto-close worked.
	}

}
