package com.w3p.im.iib.mon.mq.files.reporting;

import static org.junit.Assert.*;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import com.w3p.im.iib.mon.mq.files.data.ConnectionData;
import com.w3p.im.iib.mon.mq.files.data.Request;
import com.w3p.im.iib.mon.mq.files.data.RequestDataIn;
import com.w3p.im.iib.mon.mq.files.data.RequestDataOut;
import com.w3p.im.iib.mon.mq.files.data.Response;
import com.w3p.im.iib.mon.mq.files.reporting.FilesToQueueReporter;
import com.w3p.im.iib.mon.mq.files.reporting.QueueToFilesReporter;
import com.w3p.im.iib.mon.mq.files.reporting.Reporter;

import static org.mockito.Mockito.*;

import java.util.HashMap;
import java.util.Optional;

public class TestReporter {

	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
	}

	@AfterClass
	public static void tearDownAfterClass() throws Exception {
	}

	@Before
	public void setUp() throws Exception {
	}

	@After
	public void tearDown() throws Exception {
	}

	@Test
	public void testCanCreateFilesToQueueReporter() {
		Reporter rep = new FilesToQueueReporter();
		assertNotNull(rep); 		
	}

	@Test
	public void testCanReportFilesToQueueRequest() {
		Request req = mock(Request.class);
		when(req.getAction()).thenReturn("copy");
		
		ConnectionData cd = mock(ConnectionData.class);
		when(cd.getQmgr()).thenReturn("QMGR");
		
		RequestDataIn inData = mock(RequestDataIn.class);
		when(inData.getReadBufSize()).thenReturn(4096);
		when(inData.getFolder()).thenReturn("C:\\sourceFolder");
		when(inData.getFileNamePattern()).thenReturn(Optional.empty());
		
		RequestDataOut outData = mock(RequestDataOut.class);
		when(outData.getConnData()).thenReturn(cd);
		when(outData.getTimeToWaitSec()).thenReturn(5);
		when(outData.getMsgProperties()).thenReturn(new HashMap<String, String>());
		
		when(req.getInData()).thenReturn(inData);		
		when(req.getOutData()).thenReturn(outData);
		
		Reporter rep = new FilesToQueueReporter();		
		rep.reportRequest(req);
		
		verify(req).getInData();
		verify(req).getOutData();
		verify(req).getAction();
		verify(inData).getFolder();
		verify(inData).getFileNamePattern();
		verify(outData, times(6)).getConnData(); // userid = null
		verify(outData).getTimeToWaitSec();
		verify(outData, times(1)).getMsgProperties(); // no msg properties 		
	}
	
	@Test
	public void testCanReportFilesToQueueRequestWithOptionalsPresent() {
		Request req = mock(Request.class);
		when(req.getAction()).thenReturn("copy");
		ConnectionData cd = mock(ConnectionData.class);
		when(cd.getQmgr()).thenReturn("QMGR");
		RequestDataIn inData = mock(RequestDataIn.class);
		when(inData.getReadBufSize()).thenReturn(4096);
		when(inData.getFolder()).thenReturn("C:\\targetFolder");
		// Include File-name pattern
		when(inData.getFileNamePattern()).thenReturn(Optional.of("name1_name2_*.xml"));
		RequestDataOut outData = mock(RequestDataOut.class);
		when(outData.getConnData()).thenReturn(cd);
		when(outData.getTimeToWaitSec()).thenReturn(5);
		when(outData.getMsgProperties()).thenReturn(new HashMap<String, String>());
		
		when(req.getInData()).thenReturn(inData);		
		when(req.getOutData()).thenReturn(outData);
		
		Reporter rep = new FilesToQueueReporter();		
		rep.reportRequest(req);
		
		verify(req).getInData();
		verify(req).getOutData();
		verify(req).getAction();
		verify(inData).getFolder();
		verify(inData, times(2)).getFileNamePattern();
		verify(outData, times(6)).getConnData(); // userid = null
		verify(outData).getTimeToWaitSec();
		verify(outData, times(1)).getMsgProperties(); // no msg properties 		
	}
	
	@Test
	public void testCanReportFilesToQueueResults() {
		Response resp = mock(Response.class);
		when(resp.getItemsToBeCopied()).thenReturn(10);
		when(resp.getItemsCopied()).thenReturn(8);
		when(resp.getItemsInError()).thenReturn(2);
		when(resp.getItemsDeleted()).thenReturn(8);
		when(resp.isSuccess()).thenReturn(true);
		
		FilesToQueueReporter rep = new FilesToQueueReporter();
		rep.reportResults(resp);;
		
		verify(resp).getItemsToBeCopied();
		verify(resp).getItemsCopied();
		verify(resp).getItemsInError();
		verify(resp).getItemsDeleted();
		verify(resp, times(2)).isSuccess();
	}
	
	@Test
	public void testCanCreateQueueToFilesReporter() {
		Reporter rep = new QueueToFilesReporter();
		assertNotNull(rep); 		
	}
	
	@Test
	public void testCanReportQueueToFilesRequestWithOptionalsPresent() {
		Request req = mock(Request.class);
		when(req.getAction()).thenReturn("copy");
		
		ConnectionData cd = mock(ConnectionData.class);
		when(cd.getQmgr()).thenReturn("QMGR");
		
		RequestDataIn inData = mock(RequestDataIn.class);
		when(inData.getConnData()).thenReturn(cd);
		when(inData.getMsgSelector()).thenReturn(Optional.of("team = 'Spurs'"));
		
		RequestDataOut outData = mock(RequestDataOut.class);
		when(outData.getFolder()).thenReturn("C:\\targetFolder");
		when(outData.getFileNamePattern()).thenReturn("name$s.xml");		
		
		when(req.getInData()).thenReturn(inData);		
		when(req.getOutData()).thenReturn(outData);
		
		Reporter rep = new QueueToFilesReporter();		
		rep.reportRequest(req);
		
		verify(req).getInData();
		verify(req).getOutData();
		verify(req).getAction();
		verify(cd).getQmgr();
		verify(inData, times(6)).getConnData(); // userid = null
		verify(inData, times(2)).getMsgSelector();
		verify(outData).getFolder();
		verify(outData).getFileNamePattern();
	}
	
	@Test
	public void testCanReportQueueToFilesResults() {
		Response resp = mock(Response.class);
		when(resp.getItemsToBeCopied()).thenReturn(10);
		when(resp.getItemsCopied()).thenReturn(8);
		when(resp.getItemsInError()).thenReturn(2);
		when(resp.getItemsDeleted()).thenReturn(8);
		when(resp.isSuccess()).thenReturn(true);
		
		QueueToFilesReporter rep = new QueueToFilesReporter();
		rep.reportResults(resp);;
		
		verify(resp).getItemsToBeCopied();
		verify(resp).getItemsCopied();
		verify(resp).getItemsInError();
		verify(resp).getItemsDeleted();
		verify(resp, times(2)).isSuccess();
	}
}
