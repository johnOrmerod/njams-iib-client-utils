package com.w3p.im.iib.mon.mq.files.validation;

import static com.w3p.im.iib.mon.mq.files.constants.IConstants.ACTION;
import static com.w3p.im.iib.mon.mq.files.constants.IConstants.COPY;
import static com.w3p.im.iib.mon.mq.files.constants.IConstants.GEN_CORREL_ID;
import static com.w3p.im.iib.mon.mq.files.constants.IConstants.I_FILE_NAME_PATTERN;
import static com.w3p.im.iib.mon.mq.files.constants.IConstants.I_FOLDER;
import static com.w3p.im.iib.mon.mq.files.constants.IConstants.I_HOST;
import static com.w3p.im.iib.mon.mq.files.constants.IConstants.I_PORT;
import static com.w3p.im.iib.mon.mq.files.constants.IConstants.I_QUEUE;
import static com.w3p.im.iib.mon.mq.files.constants.IConstants.I_QUEUE_MANAGER;
import static com.w3p.im.iib.mon.mq.files.constants.IConstants.I_SERVER_CHANNEL;
import static com.w3p.im.iib.mon.mq.files.constants.IConstants.MSG_SELECTOR;
import static com.w3p.im.iib.mon.mq.files.constants.IConstants.O_FILE_NAME_PATTERN;
import static com.w3p.im.iib.mon.mq.files.constants.IConstants.O_FOLDER;
import static com.w3p.im.iib.mon.mq.files.constants.IConstants.O_HOST;
import static com.w3p.im.iib.mon.mq.files.constants.IConstants.O_PORT;
import static com.w3p.im.iib.mon.mq.files.constants.IConstants.O_QUEUE;
import static com.w3p.im.iib.mon.mq.files.constants.IConstants.O_QUEUE_MANAGER;
import static com.w3p.im.iib.mon.mq.files.constants.IConstants.O_SERVER_CHANNEL;
import static com.w3p.im.iib.mon.mq.files.constants.IConstants.PUT_WAIT_INTERVAL;
import static com.w3p.im.iib.mon.mq.files.constants.IConstants.READBUFFER_SIZE;
import static org.hamcrest.core.AnyOf.anyOf;
import static org.hamcrest.core.Is.is;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertThat;
import static org.junit.Assert.assertTrue;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.Properties;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import com.w3p.im.iib.mon.mq.files.validation.ArgsValidator;

public class TestArgsValidator {

	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
	}

	@AfterClass
	public static void tearDownAfterClass() throws Exception {
	}

	@Before
	public void setUp() throws Exception {
	}

	@After
	public void tearDown() throws Exception {
	}

	@Test
	public void testCanCreateArgsValidator() {
		ArgsValidator av = new ArgsValidator();
		assertNotNull(av);
	}

	@Test
	public void testCanValidateArgsLength() {
		// Test the Function
		String[] evenArgLength = new String[] {"-zero", "one", "-two", "three"};
	
		ArgsValidator av = new ArgsValidator();		
		Optional<List<String>> errorMsgs = av.checkThereAreEvenNumberOfArgs.apply(evenArgLength);
		
		assertFalse(errorMsgs.isPresent());		
	}
	
	@Test
	public void testCanDetectInvalidLengthArgs() {
		// Test the Function
		String[] oddArgLength = new String[] {"-zero", "one", "-two", "three", "-four"};
		
		ArgsValidator av = new ArgsValidator();		
		Optional<List<String>> errorMsgs = av.checkThereAreEvenNumberOfArgs.apply(oddArgLength);
		
		assertTrue(errorMsgs.isPresent());
		assertEquals("There is an arg missing - should be an even number of key-value pairs.", errorMsgs.get().get(0));
	}
	
	@Test
	public void testCanValidateKeyValuePairs() {
		// Test the Function
		// Assumes validation has passed for having an even number of args
		String[] validArgs = new String[] {"-zero", "one", "-two", "three"};
		
		ArgsValidator av = new ArgsValidator();
		Optional<List<String>> errorMsgs = av.verifyAllArgsAreKeyValuePairs.apply(validArgs);
		assertFalse(errorMsgs.isPresent());
	}
	
	@Test
	public void testCanDetectInvalidKeyInKeyValuePairs() {
		// Test the Function
		// Assumes validation has passed for having an even number of args
		String[] invalidArgs = new String[] {"-zero", "one", "two", "three"};
		
		ArgsValidator av = new ArgsValidator();
		Optional<List<String>> errorMsgs = av.verifyAllArgsAreKeyValuePairs.apply(invalidArgs);
		
		assertTrue(errorMsgs.isPresent());
		assertEquals("Invalid key-value pair found: 'two, three'. Arg 'two' must start with a '-'.", errorMsgs.get().get(0)); 
	}
	
	@Test
	public void testCanDetectInvalidValueInKeyValuePairs() {
		// Test the Function
		// Assumes validation has passed for having an even number of args
		String[] invalidArgs = new String[] {"-zero", "one", "-two", "-three"};
		
		ArgsValidator av = new ArgsValidator();
		Optional<List<String>> errorMsgs = av.verifyAllArgsAreKeyValuePairs.apply(invalidArgs);
		
		assertTrue(errorMsgs.isPresent());
		assertEquals("Invalid key-value pair found: '-two, -three'. Arg '-three' must not start with a '-'.", errorMsgs.get().get(0)); 
	}
	
	@Test
	public void testCanDetectSeveralInvalidKeyValuePairs() {
		// Test the Function
		// Assumes validation has passed for having an even number of args
		String[] invalidArgs = new String[] {"-zero", "one", 
				"-two", "-three", 
				"four", "five", 
				"six", "-seven"};
		
		ArgsValidator av = new ArgsValidator();
		Optional<List<String>> errorMsgs = av.verifyAllArgsAreKeyValuePairs.apply(invalidArgs);
		
		assertTrue(errorMsgs.isPresent());
		assertEquals(4, errorMsgs.get().size());
		errorMsgs.get().forEach(msg -> {
			assertThat(msg, anyOf(
				is("Invalid key-value pair found: '-two, -three'. Arg '-three' must not start with a '-'."),
				is("Invalid key-value pair found: 'four, five'. Arg 'four' must start with a '-'."),
				is("Invalid key-value pair found: 'six, -seven'. Arg 'six' must start with a '-'."),
				is("Invalid key-value pair found: 'six, -seven'. Arg '-seven' must not start with a '-'.")));
		}); 
	}
	
	@Test
	public void testCanReportResultOfValidatingValidfArgKeys () {
		// Test the Function
		// Assumes validation has passed for having an even number of args and valid 'k,v' pairs
		String[] validArgs = new String[] {"-a", "copy", "-if", "C:\\filesIn"};
		
		ArgsValidator av = new ArgsValidator();

		Optional<List<String>> errorMsgs = av.validateKeyValuesOfArgs.apply(validArgs);
		assertFalse(errorMsgs.isPresent());
	}
	
	@Test
	public void testCanReportResultOfValidatingInvalidfArgKeys () {
		// Test the Function
		// Assumes validation has passed for having an even number of args and valid 'k,v' pairs
		String[] invalidArgs = new String[] {"-ac", "copy", "-iff", "C:\\filesIn"};
		
		ArgsValidator av = new ArgsValidator();

		Optional<List<String>> errorMsgs = av.validateKeyValuesOfArgs.apply(invalidArgs);
//		errorMsgs.get().forEach(msg -> {System.out.println(msg);});
		assertTrue(errorMsgs.isPresent());
		assertEquals(2, errorMsgs.get().size());
		errorMsgs.get().forEach(msg -> {
			assertThat(msg, anyOf(
				is("Key 'ac' is not a valid value. Please correct."),
				is("Key 'iff' is not a valid value. Please correct.")));
		});		
	}
	
	@Test
	public void testCanValidateValueType() {
		// Test the Function
		ArgsValidator av = new ArgsValidator();
		Map<String, String> keyValueMap = new HashMap<>();		
		keyValueMap.put("-op", "1114");
		assertTrue(av.doesValueMatchClass.test(keyValueMap));
		
		keyValueMap.clear();
		keyValueMap.put("-gci", "true");
		assertTrue(av.doesValueMatchClass.test(keyValueMap));
				
		keyValueMap.clear();
		keyValueMap.put("-op", "11I4");
		assertFalse(av.doesValueMatchClass.test(keyValueMap));
		
		keyValueMap.clear();
		keyValueMap.put("-gcid", "treu");
		assertFalse(av.doesValueMatchClass.test(keyValueMap));
	}
	
	@Test
	public void testCanReportResultOfValidatingValuesOfKeyArgs () {
		// Test the Function		
		String[] validArgs = new String[] {"-a", "copy", "-gcid", "true", "-op", "1114"};
		String[] invalidArgs = new String[] {"-a", "cpy","-gcid", "treu", "-op", "11I4"};
		
		ArgsValidator av = new ArgsValidator();
		
		Optional<List<String>> errorMsgs = av.validateDataTypeOfArgValues.apply(validArgs); 
		assertFalse(errorMsgs.isPresent());
		
		errorMsgs = av.validateDataTypeOfArgValues.apply(invalidArgs); 
		assertTrue(errorMsgs.isPresent());
		assertEquals(3, errorMsgs.get().size());
		errorMsgs.get().forEach(msg -> {
			assertThat(msg, anyOf(
				is("Value 'cpy' is not valid for 'a'. Please correct."),
				is("Value 'treu' is not valid for 'gcid'. Please correct."),
				is("Value '11I4' is not valid for 'op'. Please correct.")));
		});
	}
	
	@Test
	public void testArgsValidatorWithValidAndInvalidArgs() {
		// Simple test the ArgsValidator class
		String[] validArgs = new String[]{"-a", "copy", "-if", "C:\\filesIn"};
		String[] oddArgLength = new String[]{"-a", "copy", "-if", "C:\\filesIn", "-iqm"};
		String[] missingKeyArgs = new String[]{"-a", "copy", "if", "C:\\filesIn"};
		String[] invalidArgs = new String[]{"-a", "copy", "-iff", "C:\\filesIn"};
		
		ArgsValidator av = new ArgsValidator();		
		Optional<List<String>> errorMsgs = av.validate(validArgs);
		// Valid args
		assertFalse(errorMsgs.isPresent());
		
		// Odd length
		errorMsgs = av.validate(oddArgLength);
		assertTrue(errorMsgs.isPresent());
		assertTrue(errorMsgs.get().size() == 1);
		assertEquals("There is an arg missing - should be an even number of key-value pairs.", errorMsgs.get().get(0));
		
		// Missing '-' key
		errorMsgs = av.validate(missingKeyArgs);
		assertTrue(errorMsgs.isPresent());
		assertTrue(errorMsgs.get().size() == 1);
		assertEquals("Invalid key-value pair found: 'if, C:\\filesIn'. Arg 'if' must start with a '-'.", errorMsgs.get().get(0));
		
		// Invalid agr key (-iff)
		errorMsgs = av.validate(invalidArgs);
		assertTrue(errorMsgs.isPresent());
		assertTrue(errorMsgs.get().size() == 1);	
		assertEquals("Key 'iff' is not a valid value. Please correct.", errorMsgs.get().get(0));
	}
	
	@Test
	public void testCanValidateValidArgsFilesToQueue() {
		ArgsValidator av = new ArgsValidator();
		
		// Valid Files to Queue
		String[] filesToQueueArgs = createAllArgsFilesToQueue();		
		Optional<List<String>> errorMsgs = av.validate(filesToQueueArgs);
		assertFalse(errorMsgs.isPresent());
	}
	
	@Test
	public void testCanValidateValidArgsQueueToFiles() {
		ArgsValidator av = new ArgsValidator();
		
		// Valid Files to Queue
		String[] queueToFilesArgs = createAllArgsQueueToFiles();		
		Optional<List<String>> errorMsgs = av.validate(queueToFilesArgs);
		assertFalse(errorMsgs.isPresent());
	}

	private String[] createAllArgsFilesToQueue() {
		Properties props = new Properties();
		props.setProperty(ACTION, COPY);
		// Input		
		props.setProperty(I_FOLDER, "C:\\filesIn");
		props.setProperty(I_FILE_NAME_PATTERN, "name1_;name2;*;xml");
		props.setProperty(READBUFFER_SIZE, "4096");		
		// Output
		props.setProperty(O_HOST, "euuktetest01");
		props.setProperty(O_QUEUE_MANAGER, "QM_OUT");
		props.setProperty(O_QUEUE, "Q_OUT");
		props.setProperty(O_PORT, "3414");
		props.setProperty(O_SERVER_CHANNEL, "DEV.APP.SVRCONN");
		props.setProperty(GEN_CORREL_ID, "true");
		props.setProperty(PUT_WAIT_INTERVAL, "5");
		
		final String[] args = new String[props.size()*2];
		final int[] i = new int[]{0};
		props.forEach((k,v) -> {			
			args[i[0]] = "-"+(String)k;
			i[0]++;
			args[i[0]] = (String)v;
			i[0]++;
		});
		
		return args;
	}
	
	private String[] createAllArgsQueueToFiles() {
		Properties props = new Properties();
		props.setProperty(ACTION, COPY);
		// Input
		props.setProperty(I_HOST, "euuktetest01");
		props.setProperty(I_QUEUE_MANAGER, "QM_OUT");
		props.setProperty(I_QUEUE, "Q_OUT");
		props.setProperty(I_PORT, "3414");
		props.setProperty(I_SERVER_CHANNEL, "DEV.APP.SVRCONN");
		props.setProperty(MSG_SELECTOR, "sport = 'Football' AND team = 'Spurs'");
		// Output
		props.setProperty(O_FOLDER, "C:\\filesOut");
		props.setProperty(O_FILE_NAME_PATTERN, "name1_;name2;*;xml");
		
		final String[] args = new String[props.size()*2];
		final int[] i = new int[]{0};
		props.forEach((k,v) -> {			
			args[i[0]] = "-"+(String)k;
			i[0]++;
			args[i[0]] = (String)v;
			i[0]++;
		});
		
		return args;
	}
}
