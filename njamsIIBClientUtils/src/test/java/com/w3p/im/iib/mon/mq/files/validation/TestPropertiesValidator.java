package com.w3p.im.iib.mon.mq.files.validation;

import static com.w3p.im.iib.mon.mq.files.constants.IConstants.ACTION;
import static com.w3p.im.iib.mon.mq.files.constants.IConstants.COPY;
import static com.w3p.im.iib.mon.mq.files.constants.IConstants.GEN_CORREL_ID;
import static com.w3p.im.iib.mon.mq.files.constants.IConstants.I_FILE_NAME_PATTERN;
import static com.w3p.im.iib.mon.mq.files.constants.IConstants.I_FOLDER;
import static com.w3p.im.iib.mon.mq.files.constants.IConstants.I_HOST;
import static com.w3p.im.iib.mon.mq.files.constants.IConstants.I_PORT;
import static com.w3p.im.iib.mon.mq.files.constants.IConstants.I_QUEUE;
import static com.w3p.im.iib.mon.mq.files.constants.IConstants.I_QUEUE_MANAGER;
import static com.w3p.im.iib.mon.mq.files.constants.IConstants.I_SERVER_CHANNEL;
import static com.w3p.im.iib.mon.mq.files.constants.IConstants.MSG_SELECTOR;
import static com.w3p.im.iib.mon.mq.files.constants.IConstants.O_FILE_NAME_PATTERN;
import static com.w3p.im.iib.mon.mq.files.constants.IConstants.O_FOLDER;
import static com.w3p.im.iib.mon.mq.files.constants.IConstants.O_HOST;
import static com.w3p.im.iib.mon.mq.files.constants.IConstants.O_PORT;
import static com.w3p.im.iib.mon.mq.files.constants.IConstants.O_QUEUE;
import static com.w3p.im.iib.mon.mq.files.constants.IConstants.O_QUEUE_MANAGER;
import static com.w3p.im.iib.mon.mq.files.constants.IConstants.O_SERVER_CHANNEL;
import static com.w3p.im.iib.mon.mq.files.constants.IConstants.PUT_WAIT_INTERVAL;
import static com.w3p.im.iib.mon.mq.files.constants.IConstants.READBUFFER_SIZE;
import static org.hamcrest.core.AnyOf.anyOf;
import static org.hamcrest.core.Is.is;
import static org.junit.Assert.*;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.Properties;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import com.w3p.im.iib.mon.mq.files.validation.AbstractValidator;
import com.w3p.im.iib.mon.mq.files.validation.PropertiesValidator;

public class TestPropertiesValidator {

	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
	}

	@AfterClass
	public static void tearDownAfterClass() throws Exception {
	}

	@Before
	public void setUp() throws Exception {
	}

	@After
	public void tearDown() throws Exception {
	}

	
	@Test
	public void testCanCreatePropertiesValidator() {
		AbstractValidator pv = new PropertiesValidator();
		assertNotNull(pv);
	}
	
	@Test
	public void testCanTrimMessagePropertyKey() {
		PropertiesValidator pv = new PropertiesValidator();
		String result = pv.trimMsgProp.apply("msgprop.key");
		assertEquals("msgprop", result);
		
		// Should return all other property keys unchanged
		result = pv.trimMsgProp.apply("property");
		assertEquals("property", result);
	}
	
	@Test
	public void testCanValidateValueType() {
		AbstractValidator pv = new PropertiesValidator();
		Map<String, String> keyValueMap = new HashMap<>();		
		keyValueMap.put(O_PORT, "1114");
		assertTrue(pv.doesValueMatchClass.test(keyValueMap));
		
		keyValueMap.clear();
		keyValueMap.put(GEN_CORREL_ID, "true");
		assertTrue(pv.doesValueMatchClass.test(keyValueMap));
				
		keyValueMap.clear();
		keyValueMap.put(O_PORT, "I114");
		assertFalse(pv.doesValueMatchClass.test(keyValueMap));
		
		keyValueMap.clear();
		keyValueMap.put(GEN_CORREL_ID, "truw");
		assertFalse(pv.doesValueMatchClass.test(keyValueMap));
	}

	@Test
	public void testCanDetectInvalidPropertyKeys() {
		PropertiesValidator pv = new PropertiesValidator();		
		Properties props = createPropertiesWithInvalidKeys();		
	
		Optional<List<String>> errorMsgs = pv.validate(props);
		assertTrue(errorMsgs.isPresent());
		assertTrue(errorMsgs.get().size() == 2);
		errorMsgs.get().forEach(msg -> {
			assertThat(msg, anyOf(
					is("Key 'xxx' is not a valid value. Please correct."),
					is("Key 'msgprops' is not a valid value. Please correct.")));
		});
	}

	@Test
	public void testCanDetectInvalidPropertyValues() {
		PropertiesValidator pv = new PropertiesValidator();		
		Properties filesToQueueProps = createPropertiesWithInvalidValues();
						
		Optional<List<String>> errorMsgs = pv.validate(filesToQueueProps);
		assertTrue(errorMsgs.isPresent());		
		assertTrue(errorMsgs.get().size() == 5);
		errorMsgs.get().forEach(msg -> {
			assertThat(msg, anyOf(
					is("Value 'cpy' is not valid for 'a'. Please correct."),
					is("Value '4O96' is not valid for 'readBufSize'. Please correct."),
					is("Value 'treu' is not valid for 'gcid'. Please correct."),
					is("Value 'S' is not valid for 'pwi'. Please correct."),
					is("Value '1I14' is not valid for 'op'. Please correct.")));
		});
	}

	@Test
	public void testCanReportResultOfValidatingValidfPropertyValues() {
		// Test the Function		
		Properties validProps = new Properties();
		validProps.setProperty("a", "copy");
		validProps.setProperty("gcid", "true");
		validProps.setProperty("op", "1114");
		
		PropertiesValidator pv = new PropertiesValidator();
		
		Optional<List<String>> errorMsgs = pv.validateDataTypeOfPropertyValues.apply(validProps); 
		assertFalse(errorMsgs.isPresent());
	}

	@Test
	public void testCanReportResultOfValidatingInvalidPropertyValues () {
		// Test the Function		

		Properties invalidProps = new Properties();
		invalidProps.setProperty("a", "cpy");
		invalidProps.setProperty("gcid", "treu");
		invalidProps.setProperty("op", "1I14");
		
		PropertiesValidator pv = new PropertiesValidator();
		
		Optional<List<String>> errorMsgs = pv.validateDataTypeOfPropertyValues.apply(invalidProps); 
		assertTrue(errorMsgs.isPresent());
		assertEquals(3, errorMsgs.get().size());
		errorMsgs.get().forEach(msg -> {
			assertThat(msg, anyOf(
					is("Value 'cpy' is not valid for 'a'. Please correct."),
					is("Value 'treu' is not valid for 'gcid'. Please correct."),
					is("Value '1I14' is not valid for 'op'. Please correct.")));
		});
	}
	
	@Test
	public void testCanValidateValidPropertiesFilesToQueue() {
		PropertiesValidator pv = new PropertiesValidator();
		
		// Valid Files to Queue
		Properties filesToQueueProps = createAllPropertiesFilesToQueue();		
		Optional<List<String>> errorMsgs = pv.validate(filesToQueueProps);
		assertFalse(errorMsgs.isPresent());
	}
	
	@Test
	public void testCanValidateValidPropertiesQueueToFile() {
		PropertiesValidator pv = new PropertiesValidator();
		
		// Valid Queue to Files
		Properties queueToFilesProps = createAllPropertiesQueueToFiles();
		Optional<List<String>> errorMsgs = pv.validate(queueToFilesProps);
		assertFalse(errorMsgs.isPresent());
	}

	private Properties createPropertiesWithInvalidKeys() {
		Properties props = createAllPropertiesFilesToQueue();
 
		props.setProperty("msgprops.team", "Spurs");
		props.setProperty("xxx", "invalid");
		return props;
	}
	
	private Properties createPropertiesWithInvalidValues() {
		Properties props = createAllPropertiesFilesToQueue();
		
		//  Use assert to ensure we change an existing property 
		assertNotNull(props.setProperty(ACTION, "cpy"));		
		assertNotNull(props.setProperty(READBUFFER_SIZE, "4O96"));
		assertNotNull(props.setProperty(O_PORT, "1I14"));
		assertNotNull(props.setProperty(GEN_CORREL_ID, "treu"));
		assertNotNull(props.setProperty(PUT_WAIT_INTERVAL, "S"));
		return props;
	}
	
	private Properties createAllPropertiesFilesToQueue() {
		Properties props = new Properties();
		props.setProperty(ACTION, COPY);
		// Input		
		props.setProperty(I_FOLDER, "C:\\filesIn");
		props.setProperty(I_FILE_NAME_PATTERN, "name1_;name2;*;xml");
		props.setProperty(READBUFFER_SIZE, "4096");		
		// Output
		props.setProperty(O_HOST, "euuktetest01");
		props.setProperty(O_QUEUE_MANAGER, "QM_OUT");
		props.setProperty(O_QUEUE, "Q_OUT");
		props.setProperty(O_PORT, "3414");
		props.setProperty(O_SERVER_CHANNEL, "DEV.APP.SVRCONN");
		props.setProperty(GEN_CORREL_ID, "true");
		props.setProperty(PUT_WAIT_INTERVAL, "5");
		props.setProperty("msgprop.game", "Football");
		props.setProperty("msgprop.team", "Spurs");		
		
		return props;
	}
	
	private Properties createAllPropertiesQueueToFiles() {
		Properties props = new Properties();
		props.setProperty(ACTION, COPY);
		// Input
		props.setProperty(I_HOST, "euuktetest01");
		props.setProperty(I_QUEUE_MANAGER, "QM_OUT");
		props.setProperty(I_QUEUE, "Q_OUT");
		props.setProperty(I_PORT, "3414");
		props.setProperty(I_SERVER_CHANNEL, "DEV.APP.SVRCONN");
		props.setProperty(MSG_SELECTOR, "sport = 'Football' AND team = 'Spurs'");
		// Output
		props.setProperty(O_FOLDER, "C:\\filesOut");
		props.setProperty(O_FILE_NAME_PATTERN, "name1_;name2;*;xml");
		
		return props;
	}

}
