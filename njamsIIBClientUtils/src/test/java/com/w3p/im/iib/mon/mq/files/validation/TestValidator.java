package com.w3p.im.iib.mon.mq.files.validation;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import com.w3p.im.iib.mon.mq.files.data.Request;
import com.w3p.im.iib.mon.mq.files.data.RequestDataOut;
import com.w3p.im.iib.mon.mq.files.data.RequestTestsHelper;
import com.w3p.im.iib.mon.mq.files.exceptions.ValidationException;
import com.w3p.im.iib.mon.mq.files.validation.Validator;

public class TestValidator {
	
	private final RequestTestsHelper helper = new RequestTestsHelper();

	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
	}

	@AfterClass
	public static void tearDownAfterClass() throws Exception {
	}

	@Before
	public void setUp() throws Exception {
	}

	@After
	public void tearDown() throws Exception {
	}

	@Test
	public void testCanCreateValidator() {
		Validator validator = new Validator();
		assertNotNull(validator);
	}
	
	@Test
	public void testValidRequestFilesToQueue() {
		try {
			Request req = helper.createRequestFilesToQueue("copy");
			new Validator().validateRequest(req);
		} catch (Exception e) {
			fail(String.format("Error - valid RequestData has thrown an exception: '%s'.", e.toString()));
		}
	}
	
	@Test
	public void testValidRequestQueueToFiles() {
		try {
			Request req = helper.createRequestQueueToFiles("move");
			new Validator().validateRequest(req);
		} catch (Exception e) {
			fail(String.format("Error - valid RequestData has thrown an exception: '%s'.", e.toString()));
		}
	}

	@Test
	public void testValidRequestQueueToQueue() {
		try {
			Request req = helper.createRequestQueueToQueue("move");
			new Validator().validateRequest(req);
		} catch (Exception e) {
			fail(String.format("Error - valid RequestData has thrown an exception: '%s'.", e.toString()));
		}
	}

	@Test
	public void testRequestWithMissingValue() {
		try {
			Request req = helper.createRequestFilesToQueue("copy");
			RequestDataOut outData = req.getOutData();

			// Out
			outData.setHost(null);
			new Validator().validateRequest(req);
			fail("Error - invalid RequestData has not thrown an exception");
		} catch (Exception e) {
			assertTrue(e instanceof ValidationException);
			assertEquals("One or more required values in the CLI args or Properties, is missing.", e.getMessage());
		}
	}
		
		
}
