package com.w3p.im.iib.mon.mq.files.writers;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;
import static org.mockito.Matchers.anyString;
import static org.mockito.Mockito.doThrow;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import javax.jms.JMSContext;
import javax.jms.JMSException;
import javax.jms.JMSProducer;
import javax.jms.JMSRuntimeException;
import javax.jms.Queue;
import javax.jms.TextMessage;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Ignore;
import org.junit.Test;

import com.ibm.msg.client.jms.JmsConnectionFactory;
import com.ibm.msg.client.jms.JmsFactoryFactory;
import com.ibm.msg.client.wmq.WMQConstants;
import com.w3p.im.iib.mon.mq.files.data.ConnectionData;
import com.w3p.im.iib.mon.mq.files.data.RequestDataOut;
import com.w3p.im.iib.mon.mq.files.data.RequestTestsHelper;
import com.w3p.im.iib.mon.mq.files.data.Response;
import com.w3p.im.iib.mon.mq.files.exceptions.MessagingException;
import com.w3p.im.iib.mon.mq.files.jms.CreateMocksForJMS;
import com.w3p.im.iib.mon.mq.files.jms.JmsFactoryFactoryWrapper;
import com.w3p.im.iib.mon.mq.files.jms.MockedJmsObjects;
import com.w3p.im.iib.mon.mq.files.writers.MessagesWriter;

public class TestMessagesWriter {
	
	private RequestTestsHelper helper = new RequestTestsHelper();
	private CreateMocksForJMS mocksForJMS = new CreateMocksForJMS();

	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
	}

	@AfterClass
	public static void tearDownAfterClass() throws Exception {
	}

	@Before
	public void setUp() throws Exception {
	}

	@After
	public void tearDown() throws Exception {
	}

	
	@Test
	public void testCanCreateMessagesWriter() {
		MessagesWriter mw = new MessagesWriter();
		assertNotNull(mw);
	}
	
	@Ignore("Integration Test - accesses MQ")
	@Test
	public void intTestCanInitMessagesWriter() {   // For integ test
		MessagesWriter mw = new MessagesWriter();
		Response resp = new Response();
		RequestDataOut reqDataOut = helper.createRequestDataOutForIB10QMGR();
		mw.init(reqDataOut, resp);
		//TODO check response
		assertNotNull(mw); 
		assertNotNull(mw.producer);
		assertNotNull(mw.getDest());
		assertEquals("queue:///NJAMS.UTILS.TEST.OUT", mw.getDest().toString());		
	}
	
	@Test
	public void testMockCanInitMessagesWriter() throws JMSException {
		
		MockedJmsObjects mjo = mocksForJMS.createForJMSConnection();
		JmsFactoryFactoryWrapper jffw = mjo.getJmsFactoryFactoryWrapper();
		JmsFactoryFactory jff = mjo.getJmsFactoryFactory();
		JmsConnectionFactory connFac = mjo.getJmsConnectionFactory();
		JMSContext context = mjo.getJmsContext();
		Queue jmsQueue = mjo.getJmsQueue();
		when(jmsQueue.toString()).thenReturn("queue:///NJAMS.UTILS.TEST.OUT");
		
		Response resp = new Response();
		RequestDataOut reqDataOut = helper.createRequestDataOutForIB10QMGR();
		
		try(MessagesWriter mw = new MessagesWriter()) { 
			mw.init(reqDataOut, resp, jffw); 
			//TODO check response
			assertNotNull(mw);
			assertNotNull(mw.producer);
			assertNotNull(mw.getDest());
			assertEquals("queue:///NJAMS.UTILS.TEST.OUT", mw.getDest().toString());	
		}
		

		
		verify(jffw).getFactory(WMQConstants.WMQ_PROVIDER);
		verify(jff).createConnectionFactory();
		
		verify(connFac, times(4)).setStringProperty(anyString(), anyString());
		verify(connFac, times(1)).setIntProperty(WMQConstants.WMQ_CONNECTION_MODE, WMQConstants.WMQ_CM_CLIENT);
		verify(connFac, times(1)).setIntProperty(WMQConstants.WMQ_PORT, reqDataOut.getConnData().getPort());
		verify(connFac, times(1)).setBooleanProperty(WMQConstants.USER_AUTHENTICATION_MQCSP, false);
		
		verify(context).createProducer();
		verify(context).createQueue("queue:///NJAMS.UTILS.TEST.OUT");		
		verify(context).close(); // Check that the auto-close worked.			
	}
	
	@Test
	public void testMockCanHandleJMSExceptionWhenCreatingFactoryFactory() throws JMSException {
		
		MockedJmsObjects mjo = mocksForJMS.createForJMSConnection();
		JmsFactoryFactoryWrapper jffw = mjo.getJmsFactoryFactoryWrapper();
		
		when(jffw.getFactory(WMQConstants.WMQ_PROVIDER)).thenThrow(new JMSException("Error creating the JmsFactoryFactory"));		

		Response resp = new Response();
		RequestDataOut reqDataOut = helper.createRequestDataOutForIB10QMGR();
		
		try(MessagesWriter mw = new MessagesWriter()) { 
			mw.init(reqDataOut, resp, jffw);
			//TODO check response
			assertNotNull(mw);
		} catch (MessagingException e) {			
			String errMsg = "An MQ/JMS error occured. Message is 'Error occurred when creating the JMS Connection Factory. See log.'.\n" + 
					"  Cause is 'Error creating the JmsFactoryFactory'.";
			assertEquals(errMsg, e.getMessage());
			assertTrue(e.getCause().getClass().isAssignableFrom(JMSException.class));
			assertEquals("Error creating the JmsFactoryFactory", e.getCause().getMessage());
		}
		// Follow the code path
		verify(jffw).getFactory(WMQConstants.WMQ_PROVIDER);
	}
	
	@Test
	public void testMockCanHandleJMSExceptionWhenCreatingConnectionFactory() throws JMSException {
		
		MockedJmsObjects mjo = mocksForJMS.createForJMSConnection();
		JmsFactoryFactoryWrapper jffw = mjo.getJmsFactoryFactoryWrapper();
		JmsFactoryFactory jff = mjo.getJmsFactoryFactory();
		
		when(jff.createConnectionFactory()).thenThrow(new JMSException("Error creating the JmsConnectionFactory"));		
		
		Response resp = new Response();
		RequestDataOut reqDataOut = helper.createRequestDataOutForIB10QMGR();
		
		try(MessagesWriter mw = new MessagesWriter()) { 
			mw.init(reqDataOut, resp, jffw);
			//TODO check response
			assertNotNull(mw);
		} catch (MessagingException e) {			
			String errMsg = "An MQ/JMS error occured. Message is 'Error occurred when creating the JMS Connection Factory. See log.'.\n" + 
					"  Cause is 'Error creating the JmsConnectionFactory'.";
			assertEquals(errMsg, e.getMessage());
			assertTrue(e.getCause().getClass().isAssignableFrom(JMSException.class));
			assertEquals("Error creating the JmsConnectionFactory", e.getCause().getMessage());
		}
		// Follow the code path
		verify(jffw).getFactory(WMQConstants.WMQ_PROVIDER);
	}

	@Test
	public void testMockCanHandleJMSExceptionWhenConfiguringConnectionFactory() throws JMSException {
		
		MockedJmsObjects mjo = mocksForJMS.createForJMSConnection();
		JmsFactoryFactoryWrapper jffw = mjo.getJmsFactoryFactoryWrapper();
		JmsConnectionFactory connFac = mjo.getJmsConnectionFactory();
		
		Response resp = new Response();
		RequestDataOut reqDataOut = helper.createRequestDataOutForIB10QMGR();
		ConnectionData connData = reqDataOut.getConnData();
		// To handle a 'when' on a method that returns 'null' (e.g a setter), see: https://stackoverflow.com/questions/29537574/mockito-error-is-not-applicable-for-the-arguments-void
		doThrow(new JMSException("Error occurred when setting an Integer property for the Connection Factory")).
			when(connFac).setIntProperty(WMQConstants.WMQ_PORT, connData.getPort());
		
		try(MessagesWriter mw = new MessagesWriter()) { 
			mw.init(reqDataOut, resp, jffw);
			//TODO check response
			assertNotNull(mw);
		} catch (MessagingException e) {			
			String errMsg = "An MQ/JMS error occured. Message is 'Error occurred when configuring the JMS Connection Factory. See log.'.\n" + 
					"  Cause is 'Error occurred when setting an Integer property for the Connection Factory'.";
			assertEquals(errMsg, e.getMessage());
			assertTrue(e.getCause().getClass().isAssignableFrom(JMSException.class));
			assertEquals("Error occurred when setting an Integer property for the Connection Factory", e.getCause().getMessage());
		}
		// Follow the code path
		verify(jffw).getFactory(WMQConstants.WMQ_PROVIDER);
	}
	
	@Test
	public void testMockCanHandleJMSExceptionWhenCreatingJMSContext() throws JMSException {
		
		MockedJmsObjects mjo = mocksForJMS.createForJMSConnection();
		JmsFactoryFactoryWrapper jffw = mjo.getJmsFactoryFactoryWrapper();
		JmsConnectionFactory connFac = mjo.getJmsConnectionFactory();
		
		when(connFac.createContext(JMSContext.AUTO_ACKNOWLEDGE)).
			thenThrow(new JMSRuntimeException("Internal error occurred when creating the JMS Context"));		

		Response resp = new Response();
		RequestDataOut reqDataOut = helper.createRequestDataOutForIB10QMGR();
		ConnectionData connData = reqDataOut.getConnData();
		
		try(MessagesWriter mw = new MessagesWriter()) { 
			mw.init(reqDataOut, resp, jffw);
			//TODO check response
			assertNotNull(mw);
		} catch (MessagingException e) {			
			String errMsg = "An MQ/JMS error occured. Message is 'Error occurred when creating the JMS Context. See log.'.\n" + 
					"  Cause is 'Internal error occurred when creating the JMS Context'.";
			assertEquals(errMsg, e.getMessage());
			assertTrue(e.getCause().getClass().isAssignableFrom(JMSRuntimeException.class));
			assertEquals("Internal error occurred when creating the JMS Context", e.getCause().getMessage());
		}
		// Follow the code path
		verify(jffw).getFactory(WMQConstants.WMQ_PROVIDER);
		verify(connFac).setStringProperty(WMQConstants.WMQ_HOST_NAME, connData.getHost());
		verify(connFac).createContext(JMSContext.AUTO_ACKNOWLEDGE);
	}
	
	@Test
	public void testMockCanHandleJMSExceptionWhenCreatingJMSDestination() throws JMSException {
		
		MockedJmsObjects mjo = mocksForJMS.createForJMSConnection();
		JmsFactoryFactoryWrapper jffw = mjo.getJmsFactoryFactoryWrapper();
		JmsConnectionFactory connFac = mjo.getJmsConnectionFactory();
		JMSContext context = mjo.getJmsContext();
		Queue jmsQueue = mjo.getJmsQueue();
		when(jmsQueue.toString()).thenReturn("queue:///NJAMS.UTILS.TEST.OUT");
		
		Response resp = new Response();
		RequestDataOut reqDataOut = helper.createRequestDataOutForIB10QMGR();
		ConnectionData connData = reqDataOut.getConnData();		
		
		when(context.createQueue(String.format("queue:///%s", connData.getQueue()))).
			thenThrow(new JMSRuntimeException("Internal error occurred when creating the Queue object"));		

		
		
		try(MessagesWriter mw = new MessagesWriter()) { 
			mw.init(reqDataOut, resp, jffw);
			assertNotNull(mw);
			//TODO check response
		} catch (MessagingException e) {			
			String errMsg = "An MQ/JMS error occured. Message is 'Error occurred when creating the JMS Destination 'NJAMS.UTILS.TEST.OUT'. See log.'.\n" + 
					"  Cause is 'Internal error occurred when creating the Queue object'.";
			assertEquals(errMsg, e.getMessage());
			assertTrue(e.getCause().getClass().isAssignableFrom(JMSRuntimeException.class));
			assertEquals("Internal error occurred when creating the Queue object", e.getCause().getMessage());
		}
		// Follow the code path
		verify(jffw).getFactory(WMQConstants.WMQ_PROVIDER);
		verify(connFac).setStringProperty(WMQConstants.WMQ_HOST_NAME, connData.getHost());
		verify(connFac).createContext(JMSContext.AUTO_ACKNOWLEDGE);
		verify(context).createQueue(String.format("queue:///%s", connData.getQueue()));
		verify(context).close(); // Check that the auto-close worked.	
	}
	
	@Test
	public void testMockCanHandleJMSExceptionWhenCreatingTextMessage() throws JMSException {
		
		MockedJmsObjects mjo = mocksForJMS.createForJMSConnection();
		JmsFactoryFactoryWrapper jffw = mjo.getJmsFactoryFactoryWrapper();
		JmsConnectionFactory connFac = mjo.getJmsConnectionFactory();
		JMSContext context = mjo.getJmsContext();
		Queue jmsQueue = mjo.getJmsQueue();
		when(jmsQueue.toString()).thenReturn("queue:///NJAMS.UTILS.TEST.OUT");
		
		Response resp = new Response();
		RequestDataOut reqDataOut = helper.createRequestDataOutForIB10QMGR();
		ConnectionData connData = reqDataOut.getConnData();		
		
		when(context.createTextMessage()).
			thenThrow(new JMSRuntimeException("Internal error occurred when creating the message"));		
		
		try(MessagesWriter mw = new MessagesWriter()) { 
			mw.init(reqDataOut, resp, jffw);
			assertNotNull(mw);
			assertTrue(resp.isSuccess());
			mw.write("fileName", "<?xml >", resp);
		}
		
		assertFalse(resp.isSuccess());
		assertEquals(1, resp.getItemsInError());
		assertTrue(resp.getAllMessages().isPresent());
//		System.out.println(resp.getAllMessages().get());
		// Seems to be an issue with a trailing space, comparing values.
		assertTrue(resp.getAllMessages().get().startsWith("Messages writer: Error trying to send the JMS message to destination 'queue:///NJAMS.UTILS.TEST.OUT'." +
				" Reason: 'javax.jms.JMSRuntimeException: Internal error occurred when creating the message'."));
		
		// Follow the code path
		verify(jffw).getFactory(WMQConstants.WMQ_PROVIDER);
		verify(connFac).setStringProperty(WMQConstants.WMQ_HOST_NAME, connData.getHost());
		verify(connFac).createContext(JMSContext.AUTO_ACKNOWLEDGE);
		verify(context).createQueue(String.format("queue:///%s", connData.getQueue()));
		verify(context).createTextMessage();		
		verify(context).close(); // Check that the auto-close worked.	
	}
	
	
	@Test
	public void testMockCanHandleJMSExceptionWhenSettingMessageProperties() throws JMSException {
		
		MockedJmsObjects mjo = mocksForJMS.createForJMSConnection();
		JmsFactoryFactoryWrapper jffw = mjo.getJmsFactoryFactoryWrapper();
		JmsConnectionFactory connFac = mjo.getJmsConnectionFactory();
		JMSContext context = mjo.getJmsContext();
		Queue jmsQueue = mjo.getJmsQueue();
		when(jmsQueue.toString()).thenReturn("queue:///NJAMS.UTILS.TEST.OUT");
		
		Response resp = new Response();
		RequestDataOut reqDataOut = helper.createRequestDataOutForIB10QMGR();
		reqDataOut.setGenCorrelId(true); // Make sure this code is used		
		ConnectionData connData = reqDataOut.getConnData();
		
		TextMessage message = mock(TextMessage.class);
		
		when(context.createTextMessage()).thenReturn(message);
		
		doThrow(new JMSException("Error occurred when setting the correlationId for the message")).
			when(message).setJMSCorrelationIDAsBytes(anyString().getBytes());
		
		try(MessagesWriter mw = new MessagesWriter()) { 
			mw.init(reqDataOut, resp, jffw);
			assertNotNull(mw);
			assertTrue(resp.isSuccess());
			mw.write("fileName", "<?xml >", resp);
		}
		
		assertFalse(resp.isSuccess());
		assertEquals(1, resp.getItemsInError());
		assertTrue(resp.getAllMessages().isPresent());
//		System.out.println(resp.getAllMessages().get());
		// Error setting properties for the JMS Text Message. Reason: '%s'.
		// Seems to be an issue with a trailing space, comparing values.
		assertTrue(resp.getAllMessages().get().startsWith("Messages writer: Error setting properties for the JMS Text Message for file 'fileName'."
				+ " Reason: 'javax.jms.JMSException: Error occurred when setting the correlationId for the message'."));
		
		// Follow the code path
		verify(jffw).getFactory(WMQConstants.WMQ_PROVIDER);
		verify(connFac).setStringProperty(WMQConstants.WMQ_HOST_NAME, connData.getHost());
		verify(connFac).createContext(JMSContext.AUTO_ACKNOWLEDGE);
		verify(context).createQueue(String.format("queue:///%s", connData.getQueue()));
		verify(context).createTextMessage();
		verify(message).setText(anyString());
		verify(message).setJMSCorrelationIDAsBytes(anyString().getBytes());
		verify(context).close(); // Check that the auto-close worked.	
	}
	
	@Test
	public void testMockCanHandleUnhandledExceptionWhenSettingMessageProperties() throws JMSException {
		
		MockedJmsObjects mjo = mocksForJMS.createForJMSConnection();
		JmsFactoryFactoryWrapper jffw = mjo.getJmsFactoryFactoryWrapper();
		JmsConnectionFactory connFac = mjo.getJmsConnectionFactory();
		JMSContext context = mjo.getJmsContext();
		Queue jmsQueue = mjo.getJmsQueue();
		when(jmsQueue.toString()).thenReturn("queue:///NJAMS.UTILS.TEST.OUT");
		
		Response resp = new Response();
		RequestDataOut reqDataOut = helper.createRequestDataOutForIB10QMGR();
		reqDataOut.setGenCorrelId(true); // Make sure this code is used		
		ConnectionData connData = reqDataOut.getConnData();
		
		TextMessage message = mock(TextMessage.class);
		
		when(context.createTextMessage()).thenReturn(message);
		
		doThrow(new RuntimeException("Something stupid happened!")).
			when(message).setText(anyString());
		
		try(MessagesWriter mw = new MessagesWriter()) { 
			mw.init(reqDataOut, resp, jffw);
			assertNotNull(mw);
			assertTrue(resp.isSuccess());
			mw.write("fileName", "<?xml >", resp);
		}
		
		assertFalse(resp.isSuccess());
		assertEquals(1, resp.getItemsInError());
		assertTrue(resp.getAllMessages().isPresent());
//		System.out.println(resp.getAllMessages().get());
		// Error setting properties for the JMS Text Message. Reason: '%s'.
		// Seems to be an issue with a trailing space, comparing values.
		assertTrue(resp.getAllMessages().get().startsWith("Messages writer: Unhandled error creating the JMS Text Message for file 'fileName'."
				+ " Reason: 'java.lang.RuntimeException: Something stupid happened!'."));
		
		// Follow the code path
		verify(jffw).getFactory(WMQConstants.WMQ_PROVIDER);
		verify(connFac).setStringProperty(WMQConstants.WMQ_HOST_NAME, connData.getHost());
		verify(connFac).createContext(JMSContext.AUTO_ACKNOWLEDGE);
		verify(context).createQueue(String.format("queue:///%s", connData.getQueue()));
		verify(context).createTextMessage();		
		verify(message).setText(anyString());		
		verify(context).close(); // Check that the auto-close worked.	
	}
	
	

	@Test
	public void testCanCreateMessage() {
		Response resp = new Response();
		String msgContent = "This is a test message";
		RequestDataOut reqDataOut = helper.createRequestDataOutForIB10QMGR();
		reqDataOut.setGenCorrelId(true);
		reqDataOut.getMsgProperties().put("team", "Spurs");
		try (MessagesWriter mw = new MessagesWriter()) {
			mw.init(reqDataOut, resp);	
			//TODO check response
			TextMessage msg = mw.createMessage(msgContent);			
			assertNotNull(msg.getJMSCorrelationID());
			assertEquals("Spurs", msg.getStringProperty("team"));			
		} catch (JMSException e) {
			fail(String.format("Unplanned JMSExcption occured: %s", e.toString()));
		}
	}

	@Ignore("Integration Test - accesses MQ")
	@Test
	public void intTestCanWriteMessage() {		
		Response resp = new Response();
		resp.setItemsToBeCopied(1);
		RequestDataOut reqDataOut = helper.createRequestDataOutForIB10QMGR();
		reqDataOut.setGenCorrelId(true);
		reqDataOut.getMsgProperties().put("team", "Spurs");
		
		try (MessagesWriter mw = new MessagesWriter()) {
			mw.init(reqDataOut, resp);
			String fileName = "testFile";
			String msgContent = "This is a test message";
			mw.write(fileName, msgContent, resp);
			//TODO check response
			assertTrue(resp.isSuccess());
			assertEquals(1,resp.getItemsToBeCopied());
			assertEquals(1,resp.getItemsCopied());
			assertEquals(0,resp.getItemsInError());
			assertEquals(0,resp.getItemsDeleted());
			assertFalse(resp.getAllMessages().isPresent());
		}
	}

	@Test
	public void testMockCanWriteMessage() throws JMSException {		
		Response resp = new Response();
		resp.setItemsToBeCopied(1);
		RequestDataOut reqDataOut = helper.createRequestDataOutForIB10QMGR();
		reqDataOut.setGenCorrelId(true);
		reqDataOut.getMsgProperties().put("team", "Spurs");
		
		// Mocking JMS connection
		MockedJmsObjects mjo = mocksForJMS.createForJMSConnection();
		JmsFactoryFactoryWrapper jffw = mjo.getJmsFactoryFactoryWrapper();
		JMSContext context = mjo.getJmsContext();
		Queue jmsQueue = mjo.getJmsQueue();
		when(jmsQueue.toString()).thenReturn("queue:///NJAMS.UTILS.TEST.OUT");
		
		TextMessage message = mock(TextMessage.class);
		when(context.createTextMessage()).thenReturn(message);

		String fileName = "testFile";
		String msgContent = "This is a test message";
		try (MessagesWriter mw = new MessagesWriter()) {
			mw.init(reqDataOut, resp, jffw);
			//TODO check response
			mw.write(fileName, msgContent, resp);
		}
		assertTrue(resp.isSuccess());
		assertEquals(1,resp.getItemsToBeCopied());
		assertEquals(1,resp.getItemsCopied());
		assertEquals(0,resp.getItemsInError());
		assertEquals(0,resp.getItemsDeleted());
		assertFalse(resp.getAllMessages().isPresent());
		// Verify paths taken
		verify(context).createTextMessage();
		verify(message).setIntProperty(WMQConstants.JMS_IBM_CHARACTER_SET, 1208);
		verify(message).setText(msgContent);
		verify(message).setJMSCorrelationIDAsBytes(anyString().getBytes());
		verify(message).setStringProperty("team", "Spurs");
		verify(context).close(); // Check that the auto-close worked.
	}

	
	
	private MockedJmsObjects createMocksForJMSConnection() throws JMSException {
			MockedJmsObjects mjo = new MockedJmsObjects();
			JmsConnectionFactory connFac = mock(JmsConnectionFactory.class);				
			JmsFactoryFactory ff = mock(JmsFactoryFactory.class);
			when(ff.createConnectionFactory()).thenReturn(connFac);
	
			JmsFactoryFactoryWrapper jffw = mock(JmsFactoryFactoryWrapper.class);
			when(jffw.getFactory(WMQConstants.WMQ_PROVIDER)).thenReturn(ff);
	
			JMSContext context = mock(JMSContext.class);
			when(connFac.createContext(JMSContext.AUTO_ACKNOWLEDGE)).thenReturn(context);
	
			Queue jmsQueue = mock(Queue.class);					
			when(context.createQueue(anyString())).thenReturn(jmsQueue);
			
			JMSProducer producer = mock(JMSProducer.class);
			when(context.createProducer()).thenReturn(producer);
			
			mjo.setJmsConnectionFactory(connFac);
			mjo.setJmsContext(context);
			mjo.setJmsFactoryFactory(ff);
			mjo.setJmsFactoryFactoryWrapper(jffw);
			mjo.setJmsProducer(producer);
			mjo.setJmsQueue(jmsQueue);
			
			return mjo;
		}

}
